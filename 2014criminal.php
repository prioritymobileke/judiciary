<?php
session_start();
require("connect1.php");
require("config.php");

if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
header("Location:index.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Usalama Dashboard</title>
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
 <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([

         ['Month', 'Cases Filed','Cases Decided'],
         <?php 
           $query1 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =01 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row1 = mysqli_fetch_array($query1);
           $count1 = $row1['count'];
          
           $query2 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =01 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row2 = mysqli_fetch_array($query2);
          $count2 = $row2['count'];

         $query3 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =02 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row3 = mysqli_fetch_array($query3);
           $count3 = $row3['count'];
          
           $query4 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =02 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row4 = mysqli_fetch_array($query4);
          $count4 = $row4['count'];

          $query5 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =03 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row5= mysqli_fetch_array($query5);
           $count5 = $row5['count'];
          
           $query6 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =03 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row6 = mysqli_fetch_array($query6);
          $count6 = $row6['count'];
           $query7 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =04 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row7= mysqli_fetch_array($query7);
           $count7 = $row7['count'];
          
           $query8 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =04 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row8 = mysqli_fetch_array($query8);
          $count8 = $row8['count'];
           $query9 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =05 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row9= mysqli_fetch_array($query9);
           $count9 = $row9['count'];
          
           $query10 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =05 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row10 = mysqli_fetch_array($query10);
          $count10 = $row10['count'];
           $query11 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =06 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row11= mysqli_fetch_array($query11);
           $count11 = $row11['count'];
          
           $query12 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =06 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row12 = mysqli_fetch_array($query12);
          $count12 = $row12['count'];
           $query13 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =07 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row13= mysqli_fetch_array($query13);
           $count13 = $row13['count'];
          
           $query14 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =07 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row14 = mysqli_fetch_array($query14);
          $count14 = $row14['count'];
           $query15 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =08 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row15= mysqli_fetch_array($query15);
           $count15 = $row15['count'];
          
           $query16= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =08 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row16 = mysqli_fetch_array($query16);
          $count16 = $row16['count'];
           $query17 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =09 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row17= mysqli_fetch_array($query17);
           $count17 = $row17['count'];
          
           $query18 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =09 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row18 = mysqli_fetch_array($query18);
          $count18 = $row18['count'];
           $query19 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =10 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row19= mysqli_fetch_array($query19);
           $count19 = $row19['count'];
          
           $query20= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =10 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row20 = mysqli_fetch_array($query20);
          $count20= $row20['count'];
           $query21= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =11 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row21= mysqli_fetch_array($query21);
           $count21 = $row21['count'];
          
           $query22 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =11 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row22 = mysqli_fetch_array($query22);
          $count22 = $row22['count'];
           $query23 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =12 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row23= mysqli_fetch_array($query23);
           $count23= $row23['count'];
          
           $query24 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =12 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row24 = mysqli_fetch_array($query24);
          $count24 = $row24['count'];



          ?>
           
           ['Jan',<?php echo $count1; ?>,<?php echo $count2; ?>],
           ['Feb',<?php echo $count3; ?>,<?php echo $count4; ?>],
           ['Mar',<?php echo $count5; ?>,<?php echo $count6; ?>],
           ['Apr',<?php echo $count7; ?>,<?php echo $count8; ?>],
           ['May',<?php echo $count9; ?>,<?php echo $count10; ?>],
           ['Jun',<?php echo $count11; ?>,<?php echo $count12; ?>],
           ['Jul',<?php echo $count13; ?>,<?php echo $count14; ?>],
           ['Aug',<?php echo $count15; ?>,<?php echo $count16; ?>],
           ['Sep',<?php echo $count17; ?>,<?php echo $count18; ?>],
           ['Oct',<?php echo $count19; ?>,<?php echo $count20; ?>],
           ['Nov',<?php echo $count21; ?>,<?php echo $count22; ?>],
           ['Dec',<?php echo $count23; ?>,<?php echo $count24; ?>],
    

         
      ]);

        var options = {
          title: 'Murder Cases 2014'
        };
      var chart = new google.visualization.ColumnChart(document.getElementById("linechart"));
      chart.draw(data, options);
  }
  </script>
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([

         ['Month', 'Cases Filed','Cases Decided'],
         <?php 
           $query1 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =01 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row1 = mysqli_fetch_array($query1);
           $count1 = $row1['count'];
          
           $query2 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =01 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row2 = mysqli_fetch_array($query2);
          $count2 = $row2['count'];

         $query3 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =02 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row3 = mysqli_fetch_array($query3);
           $count3 = $row3['count'];
          
           $query4 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =02 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row4 = mysqli_fetch_array($query4);
          $count4 = $row4['count'];

          $query5 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =03 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals");

           $row5= mysqli_fetch_array($query5);
           $count5 = $row5['count'];
          
           $query6 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =03 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row6 = mysqli_fetch_array($query6);
          $count6 = $row6['count'];
           $query7 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =04 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row7= mysqli_fetch_array($query7);
           $count7 = $row7['count'];
          
           $query8 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =04 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row8 = mysqli_fetch_array($query8);
          $count8 = $row8['count'];
           $query9 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =05 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row9= mysqli_fetch_array($query9);
           $count9 = $row9['count'];
          
           $query10 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =05 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row10 = mysqli_fetch_array($query10);
          $count10 = $row10['count'];
           $query11 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =06 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row11= mysqli_fetch_array($query11);
           $count11 = $row11['count'];
          
           $query12 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =06 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row12 = mysqli_fetch_array($query12);
          $count12 = $row12['count'];
           $query13 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =07 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row13= mysqli_fetch_array($query13);
           $count13 = $row13['count'];
          
           $query14 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =07 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row14 = mysqli_fetch_array($query14);
          $count14 = $row14['count'];
           $query15 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =08 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row15= mysqli_fetch_array($query15);
           $count15 = $row15['count'];
          
           $query16= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =08 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row16 = mysqli_fetch_array($query16);
          $count16 = $row16['count'];
           $query17 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =09 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row17= mysqli_fetch_array($query17);
           $count17 = $row17['count'];
          
           $query18 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =09 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row18 = mysqli_fetch_array($query18);
          $count18 = $row18['count'];
           $query19 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =10 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row19= mysqli_fetch_array($query19);
           $count19 = $row19['count'];
          
           $query20= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =10 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row20 = mysqli_fetch_array($query20);
          $count20= $row20['count'];
           $query21= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =11 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row21= mysqli_fetch_array($query21);
           $count21 = $row21['count'];
          
           $query22 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =11 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row22 = mysqli_fetch_array($query22);
          $count22 = $row22['count'];
           $query23 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =12 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals'");

           $row23= mysqli_fetch_array($query23);
           $count23= $row23['count'];
          
           $query24 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =12 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Appeals' AND CaseStatus ='case closed'");

          $row24 = mysqli_fetch_array($query24);
          $count24 = $row24['count'];



          ?>
           
           ['Jan',<?php echo $count1; ?>,<?php echo $count2; ?>],
           ['Feb',<?php echo $count3; ?>,<?php echo $count4; ?>],
           ['Mar',<?php echo $count5; ?>,<?php echo $count6; ?>],
           ['Apr',<?php echo $count7; ?>,<?php echo $count8; ?>],
           ['May',<?php echo $count9; ?>,<?php echo $count10; ?>],
           ['Jun',<?php echo $count11; ?>,<?php echo $count12; ?>],
           ['Jul',<?php echo $count13; ?>,<?php echo $count14; ?>],
           ['Aug',<?php echo $count15; ?>,<?php echo $count16; ?>],
           ['Sep',<?php echo $count17; ?>,<?php echo $count18; ?>],
           ['Oct',<?php echo $count19; ?>,<?php echo $count20; ?>],
           ['Nov',<?php echo $count21; ?>,<?php echo $count22; ?>],
           ['Dec',<?php echo $count23; ?>,<?php echo $count24; ?>],
    

         
      ]);

        var options = {
          title: 'Capital appeals Cases 2014'
        };
      var chart = new google.visualization.ColumnChart(document.getElementById("line1chart"));
      chart.draw(data, options);
  }
  </script>
 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([

         ['Month', 'Cases Filed','Cases Decided'],
         <?php 
           $query1 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =01 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row1 = mysqli_fetch_array($query1);
           $count1 = $row1['count'];
          
           $query2 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =01 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row2 = mysqli_fetch_array($query2);
          $count2 = $row2['count'];

         $query3 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =02 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row3 = mysqli_fetch_array($query3);
           $count3 = $row3['count'];
          
           $query4 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =02 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row4 = mysqli_fetch_array($query4);
          $count4 = $row4['count'];

          $query5 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =03 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row5= mysqli_fetch_array($query5);
           $count5 = $row5['count'];
          
           $query6 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =03 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row6 = mysqli_fetch_array($query6);
          $count6 = $row6['count'];
           $query7 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =04 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row7= mysqli_fetch_array($query7);
           $count7 = $row7['count'];
          
           $query8 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =04 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row8 = mysqli_fetch_array($query8);
          $count8 = $row8['count'];
           $query9 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =05 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row9= mysqli_fetch_array($query9);
           $count9 = $row9['count'];
          
           $query10 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =05 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row10 = mysqli_fetch_array($query10);
          $count10 = $row10['count'];
           $query11 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =06 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row11= mysqli_fetch_array($query11);
           $count11 = $row11['count'];
          
           $query12 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =06 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row12 = mysqli_fetch_array($query12);
          $count12 = $row12['count'];
           $query13 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =07 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row13= mysqli_fetch_array($query13);
           $count13 = $row13['count'];
          
           $query14 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =07 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row14 = mysqli_fetch_array($query14);
          $count14 = $row14['count'];
           $query15 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =08 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row15= mysqli_fetch_array($query15);
           $count15 = $row15['count'];
          
           $query16= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =08 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row16 = mysqli_fetch_array($query16);
          $count16 = $row16['count'];
           $query17 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =09 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row17= mysqli_fetch_array($query17);
           $count17 = $row17['count'];
          
           $query18 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =09 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row18 = mysqli_fetch_array($query18);
          $count18 = $row18['count'];
           $query19 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =10 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row19= mysqli_fetch_array($query19);
           $count19 = $row19['count'];
          
           $query20= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =10 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row20 = mysqli_fetch_array($query20);
          $count20= $row20['count'];
           $query21= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =11 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row21= mysqli_fetch_array($query21);
           $count21 = $row21['count'];
          
           $query22 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =11 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row22 = mysqli_fetch_array($query22);
          $count22 = $row22['count'];
           $query23 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =12 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row23= mysqli_fetch_array($query23);
           $count23= $row23['count'];
          
           $query24 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =12 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row24 = mysqli_fetch_array($query24);
          $count24 = $row24['count'];



          ?>
           
           ['Jan',<?php echo $count1; ?>,<?php echo $count2; ?>],
           ['Feb',<?php echo $count3; ?>,<?php echo $count4; ?>],
           ['Mar',<?php echo $count5; ?>,<?php echo $count6; ?>],
           ['Apr',<?php echo $count7; ?>,<?php echo $count8; ?>],
           ['May',<?php echo $count9; ?>,<?php echo $count10; ?>],
           ['Jun',<?php echo $count11; ?>,<?php echo $count12; ?>],
           ['Jul',<?php echo $count13; ?>,<?php echo $count14; ?>],
           ['Aug',<?php echo $count15; ?>,<?php echo $count16; ?>],
           ['Sep',<?php echo $count17; ?>,<?php echo $count18; ?>],
           ['Oct',<?php echo $count19; ?>,<?php echo $count20; ?>],
           ['Nov',<?php echo $count21; ?>,<?php echo $count22; ?>],
           ['Dec',<?php echo $count23; ?>,<?php echo $count24; ?>],
    

         
      ]);

        var options = {
          title: 'Appeals Cases 2014'
        };
      var chart = new google.visualization.ColumnChart(document.getElementById("line2chart"));
      chart.draw(data, options);
  }
  </script>
 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([

         ['Month', 'Cases Filed','Cases Decided'],
         <?php 
           $query1 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =01 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row1 = mysqli_fetch_array($query1);
           $count1 = $row1['count'];
          
           $query2 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =01 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row2 = mysqli_fetch_array($query2);
          $count2 = $row2['count'];

         $query3 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =02 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row3 = mysqli_fetch_array($query3);
           $count3 = $row3['count'];
          
           $query4 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =02 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row4 = mysqli_fetch_array($query4);
          $count4 = $row4['count'];

          $query5 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =03 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row5= mysqli_fetch_array($query5);
           $count5 = $row5['count'];
          
           $query6 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =03 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row6 = mysqli_fetch_array($query6);
          $count6 = $row6['count'];
           $query7 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =04 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row7= mysqli_fetch_array($query7);
           $count7 = $row7['count'];
          
           $query8 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =04 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row8 = mysqli_fetch_array($query8);
          $count8 = $row8['count'];
           $query9 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =05 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row9= mysqli_fetch_array($query9);
           $count9 = $row9['count'];
          
           $query10 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =05 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row10 = mysqli_fetch_array($query10);
          $count10 = $row10['count'];
           $query11 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =06 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row11= mysqli_fetch_array($query11);
           $count11 = $row11['count'];
          
           $query12 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =06 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row12 = mysqli_fetch_array($query12);
          $count12 = $row12['count'];
           $query13 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =07 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row13= mysqli_fetch_array($query13);
           $count13 = $row13['count'];
          
           $query14 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =07 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row14 = mysqli_fetch_array($query14);
          $count14 = $row14['count'];
           $query15 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =08 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row15= mysqli_fetch_array($query15);
           $count15 = $row15['count'];
          
           $query16= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =08 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row16 = mysqli_fetch_array($query16);
          $count16 = $row16['count'];
           $query17 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =09 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row17= mysqli_fetch_array($query17);
           $count17 = $row17['count'];
          
           $query18 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =09 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row18 = mysqli_fetch_array($query18);
          $count18 = $row18['count'];
           $query19 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =10 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row19= mysqli_fetch_array($query19);
           $count19 = $row19['count'];
          
           $query20= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =10 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row20 = mysqli_fetch_array($query20);
          $count20= $row20['count'];
           $query21= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =11 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row21= mysqli_fetch_array($query21);
           $count21 = $row21['count'];
          
           $query22 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =11 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row22 = mysqli_fetch_array($query22);
          $count22 = $row22['count'];
           $query23 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =12 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row23= mysqli_fetch_array($query23);
           $count23= $row23['count'];
          
           $query24 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =12 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row24 = mysqli_fetch_array($query24);
          $count24 = $row24['count'];



          ?>
           
           ['Jan',<?php echo $count1; ?>,<?php echo $count2; ?>],
           ['Feb',<?php echo $count3; ?>,<?php echo $count4; ?>],
           ['Mar',<?php echo $count5; ?>,<?php echo $count6; ?>],
           ['Apr',<?php echo $count7; ?>,<?php echo $count8; ?>],
           ['May',<?php echo $count9; ?>,<?php echo $count10; ?>],
           ['Jun',<?php echo $count11; ?>,<?php echo $count12; ?>],
           ['Jul',<?php echo $count13; ?>,<?php echo $count14; ?>],
           ['Aug',<?php echo $count15; ?>,<?php echo $count16; ?>],
           ['Sep',<?php echo $count17; ?>,<?php echo $count18; ?>],
           ['Oct',<?php echo $count19; ?>,<?php echo $count20; ?>],
           ['Nov',<?php echo $count21; ?>,<?php echo $count22; ?>],
           ['Dec',<?php echo $count23; ?>,<?php echo $count24; ?>],
    

         
      ]);

        var options = {
          title: 'Ordinary Cr.Appeals Cases 2014'
        };
      var chart = new google.visualization.ColumnChart(document.getElementById("line3chart"));
      chart.draw(data, options);
  }
  </script>
 <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([

         ['Month', 'Cases Filed','Cases Decided'],
         <?php 
           $query1 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =01 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row1 = mysqli_fetch_array($query1);
           $count1 = $row1['count'];
          
           $query2 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =01 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row2 = mysqli_fetch_array($query2);
          $count2 = $row2['count'];

         $query3 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =02 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row3 = mysqli_fetch_array($query3);
           $count3 = $row3['count'];
          
           $query4 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =02 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row4 = mysqli_fetch_array($query4);
          $count4 = $row4['count'];

          $query5 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =03 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row5= mysqli_fetch_array($query5);
           $count5 = $row5['count'];
          
           $query6 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =03 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row6 = mysqli_fetch_array($query6);
          $count6 = $row6['count'];
           $query7 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =04 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row7= mysqli_fetch_array($query7);
           $count7 = $row7['count'];
          
           $query8 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =04 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row8 = mysqli_fetch_array($query8);
          $count8 = $row8['count'];
           $query9 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =05 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row9= mysqli_fetch_array($query9);
           $count9 = $row9['count'];
          
           $query10 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =05 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row10 = mysqli_fetch_array($query10);
          $count10 = $row10['count'];
           $query11 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =06 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row11= mysqli_fetch_array($query11);
           $count11 = $row11['count'];
          
           $query12 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =06 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row12 = mysqli_fetch_array($query12);
          $count12 = $row12['count'];
           $query13 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =07 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row13= mysqli_fetch_array($query13);
           $count13 = $row13['count'];
          
           $query14 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =07 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row14 = mysqli_fetch_array($query14);
          $count14 = $row14['count'];
           $query15 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =08 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row15= mysqli_fetch_array($query15);
           $count15 = $row15['count'];
          
           $query16= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =08 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row16 = mysqli_fetch_array($query16);
          $count16 = $row16['count'];
           $query17 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =09 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row17= mysqli_fetch_array($query17);
           $count17 = $row17['count'];
          
           $query18 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =09 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row18 = mysqli_fetch_array($query18);
          $count18 = $row18['count'];
           $query19 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =10 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row19= mysqli_fetch_array($query19);
           $count19 = $row19['count'];
          
           $query20= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =10 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row20 = mysqli_fetch_array($query20);
          $count20= $row20['count'];
           $query21= mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =11 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row21= mysqli_fetch_array($query21);
           $count21 = $row21['count'];
          
           $query22 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =11 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row22 = mysqli_fetch_array($query22);
          $count22 = $row22['count'];
           $query23 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM DateOfPlea) =12 AND EXTRACT(YEAR FROM DateOfPlea)=2014 
           AND Division ='Criminal' AND CaseType ='Murder'");

           $row23= mysqli_fetch_array($query23);
           $count23= $row23['count'];
          
           $query24 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases where
           EXTRACT(MONTH FROM lastStatusDate) =12 AND EXTRACT(YEAR FROM lastStatusDate)=2014 
           AND Division ='Criminal' AND CaseType ='Murder' AND CaseStatus ='case closed'");

          $row24 = mysqli_fetch_array($query24);
          $count24 = $row24['count'];



          ?>
           
           ['Jan',<?php echo $count1; ?>,<?php echo $count2; ?>],
           ['Feb',<?php echo $count3; ?>,<?php echo $count4; ?>],
           ['Mar',<?php echo $count5; ?>,<?php echo $count6; ?>],
           ['Apr',<?php echo $count7; ?>,<?php echo $count8; ?>],
           ['May',<?php echo $count9; ?>,<?php echo $count10; ?>],
           ['Jun',<?php echo $count11; ?>,<?php echo $count12; ?>],
           ['Jul',<?php echo $count13; ?>,<?php echo $count14; ?>],
           ['Aug',<?php echo $count15; ?>,<?php echo $count16; ?>],
           ['Sep',<?php echo $count17; ?>,<?php echo $count18; ?>],
           ['Oct',<?php echo $count19; ?>,<?php echo $count20; ?>],
           ['Nov',<?php echo $count21; ?>,<?php echo $count22; ?>],
           ['Dec',<?php echo $count23; ?>,<?php echo $count24; ?>],
    

         
      ]);

        var options = {
          title: 'Revisions Cases 2014'
        };
      var chart = new google.visualization.ColumnChart(document.getElementById("line4chart"));
      chart.draw(data, options);
  }
  </script>
 
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
</head>

<body>

<div class="mainwrapper">
    
    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">
                <li class="odd">


                </li>
                <li>

                </li>
                <li class="odd">

                </li>
                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
    
    <div class="leftpanel">
        
    <?php include"left_menu.php";?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Dashboard</li>

        </ul>
        
      <br><br>
        
        <div class="">
            <div class="">
                <div class="">
                    <div id="dashboard-left" class="span12">
	                 
           
			 
			 <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title">Murder Cases 2014</h3>
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    
                    <td id="linechart" style="width: 900px; height: 400px; align:center"></td>
                  </tr></table>
                  
                
                </div>
                </div><!-- /.box-body-->
              </div><!-- /.box -->
              <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title">Capital Appeals Cases 2014</h3>
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    
                    <td id="line2chart" style="width: 900px; height: 400px; align:center"></td>
                  </tr></table>
                  
                
                </div>
                </div><!-- /.box-body-->
              </div>
               <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title">Ordinary Cr.Appeals Cases 2014</h3>
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    
                    <td id="line3chart" style="width: 900px; height: 400px; align:center"></td>
                  </tr></table>
                  
                
                </div>
                </div><!-- /.box-body-->
              </div>
               <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title">Revisions Cases 2014</h3>
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    
                    <td id="line4chart" style="width: 900px; height: 400px; align:center"></td>
                  </tr></table>
                  
                
                </div>
                </div><!-- /.box-body-->
              </div>
            </div><!-- /.col -->           

                        
                    </div><!--span8-->
                    

                </div><!--row-fluid-->
                
                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->


 	
<script type="text/javascript" src="js/jQuery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.1.1.minssssss.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="js/modernizrssss.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.uniformsss.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>

<script type="text/javascript" src="js/custom.js"></script>

	


    <!-- Page script -->
	   
</body>
</html>
