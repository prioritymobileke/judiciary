<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))
{
header("Location:index.php");
}
include"functions.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
			
			jQuery('.numbersOnly').keyup(function () {
			if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
			   this.value = this.value.replace(/[^0-9\.]/g, '');
			}
		      });
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
			


	jQuery("#add_doctor").click(function(e){
      jQuery('#add_doctor').attr('disabled',true);
	  jQuery('#add_doctor').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {			
			id_number:jQuery("#doctor_id").val(),
			specialization:jQuery("#specialization").val(),
			email:jQuery("#doctor_email").val(),
			name_of_party:jQuery("#doctor_name").val(),
			phone_number:jQuery("#doctor_phone").val(),
			postal_address:jQuery("#doctor_address").val(),
			doctor:"yes"
		},
		  function(data,status){
			
			jQuery("#doctor_id").val("")
			jQuery("#doctor_name").val(""),
			jQuery("#doctor_phone").val("")
			jQuery("#specialization").val("")
			jQuery("#doctor_email").val("")
			jQuery("#doctor_address").val("")
			jQuery('#add_doctor').attr('disabled',false);
			jQuery('#add_doctor').val('SUBMITED');
	        jQuery('#add_doctor').val('SUBMIT');
			
			
			
		  });
		
	});		

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.php"><img src="images/logo1.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

         <?php include"reg_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>doctor Registration</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">
       <h4 class="widgettitle" align="center"></h4>
    <form action="regCriminalCases.php" method="post"/>
	<input  type="hidden" name="caseNo" id="caseNo" value="<?php echo case_no();?>" />
	
	<div style="width:350px;margin:10px; float:left;">	
	<fieldset><legend>Doctor </legend>
	<p class="form_input">Name<br><input  type="text" id="doctor_name" /></p>
	<p class="form_input">Practice Number<br><input  type="text" id="doctor_id" /></p>
    <p class="form_input">Specialization<br><input  type="text" id="specialization" /></p>	
	<p class="form_input">Phone Number<br><input  type="text" id="doctor_phone" /></p>
	<p class="form_input">Address<br><input  type="text" id="doctor_address" /></p>
	<p class="form_input">Email<br><input  type="email" id="doctor_email" /></p>
	<p class="form_input"><input  type="button" id="add_doctor" value="SUBMIT" /></p>	
	</fieldset>
	</div>
	
	</form>
     <div style="width:350px;margin:10px;float:left;">
	<?php
	if(isset($_POST['register_case'])){
		
	   if($_POST['caseType']!="Criminal revision" && $_POST['caseType']!="Murder"):	
		
		$caseNo=mysql_real_escape_string($_POST['caseNo']);
        $caseDetails=mysql_real_escape_string($_POST['case_particulars']);
		$caseType=mysql_real_escape_string($_POST['caseType']);
         if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`Division`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','Criminal','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}	
			
		elseif($_POST['caseType']=="Murder"):
		
			$caseNo=mysql_real_escape_string($_POST['caseNo']);
			$caseDetails=mysql_real_escape_string($_POST['case_particulars']);
			$caseType=mysql_real_escape_string($_POST['caseType']);
			$DateOfPlea=mysql_real_escape_string($_POST['DateOfPlea']);
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`Division`,`CaseDetails`,`DateOfPlea`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','Criminal','{$caseDetails}','{$DateOfPlea}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
		
		elseif($_POST['caseType']=="Criminal revision"):
		
			$caseNo=mysql_real_escape_string($_POST['caseNo']);
            $caseDetails=mysql_real_escape_string($_POST['case_particulars']);
		    $caseType=mysql_real_escape_string($_POST['caseType']);
			$CommunityServinceOrder=mysql_real_escape_string($_POST['CommunityServinceOrder']);
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`Division`,`CaseDetails`,`CommunityServinceOrder`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','Criminal','{$caseDetails}','{$CommunityServinceOrder}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
	   endif;
		
	?>
	<script>document.location="regCriminalCases.php?caseType=<?php echo $_REQUEST['caseType'];?>";</script>
  <?php	
	}
	?>
	</div>           
               

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
