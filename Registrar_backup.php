<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))
{
header("Location:index.php");
}
?>

<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">
    
    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.php"><img src="images/logo1.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>

<li class="active"><a href="Registrar.php"><i class="iconfa-home"></i></span> ALL CASES</a></li>


 
                    </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->

    <div class="rightpanel">

        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>All Cases</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">

              <h4 class="widgettitle">Case Details Table|<a href="reports/scripts/new_cases.php" style="color:#fff">  Export <img src="images/images/excel.png" align="center" width="20"></a></h4>
				
                <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0">Accused</th>
                         <th class="head0">Gender</th>
                        <th class="head0">Age</th>
                        <th class="head0">Offence</th>
                        <th class="head0">Offence Location</th>
                        <th class="head0">ObNo</th>
                        <th class="head0">Ob entry Date</th>
                        <th class="head0">Police Station</th>                      
                        
                        
                        <th class="head0"></th>




                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");


                    $query="select ob.OffenceType,ob.CourtDate,ob.EntryDate,parties.ObNo,ob.Station,GROUP_CONCAT(parties.PName SEPARATOR ' , ' ) as PName,parties.Role,parties.Dob,parties.Gender,ob.LocationOccurred from parties inner join ob on ob.ObNo=parties.ObNo
                     where parties.ObNo  not in (select ObNo from cases) and parties.Role='Accused' and ob.CourtDate is not null and ob.CourtDate!='' and parties.PName !='' and ob.IncidentType <> 'Traffic' group by ObNo";

                    $result=mysql_query($query);
                    
                    /* $ob_details=mysql_query("select ObNo,OffenceType,CourtDate,EntryDate,Station,LocationOccurred from ob 
                    where ObNo  not in (select ObNo from cases) and ob.CourtDate is not null and ob.CourtDate!='' and ob.IncidentType <> 'Traffic' "); 
                    
                    $row1=mysql_fetch_array($ob_details);
                    
                    $party_accused=mysql_query("select ObNo,GROUP_CONCAT(PName SEPARATOR ' , ' ) as PName,Role,Dob,Gender from parties 
                    where Role='Accused' and ObNo=' ".$row1['ObNo']." ' ");
                    
                    $row2=mysql_fetch_array($party_accused);
                    
                    $party_complainant=mysql_query("select ObNo,GROUP_CONCAT(PName SEPARATOR ' , ' ) as PName,Role,Dob,Gender from parties 
                    where Role='Complainant' and ObNo=' ".$row2['ObNo']." ' ");
                    
                    $row3=mysql_fetch_array($party_complainant); */
                    
                                    

                    while($row=mysql_fetch_array($result))
                    {
                        $obno=$row['ObNo'];
                        $entrydate=$row['EntryDate'];
                        $station=$row['Station'];
                        $name=$row['PName'];
                        
                       // $row1=mysql_fetch_arrray($result2);
                       // $complainants=$row1;
                        
                        $offence=$row['OffenceType'];
                        $offencelocation=$row['LocationOccurred'];
                        $gender=$row['Gender'];
                        $dob=$row['Dob'];
                        if($dob=='' || $dob==NULL)
                        {
                        $age='unknown';
                        }
                        else
                        {
                        $age = date_diff(date_create($dob), date_create('now'))->y;
                        }


                        ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            

                             <td><?php echo $name ?></td> 
                             <td><?php echo $gender?></td> 
                            <td><?php echo $age?></td> 
                            <td><?php echo $offence?></td> 
                            <td><?php echo $offencelocation?></td>
                             <td><?php echo $obno ?></td> 
                            <td><?php echo $entrydate?></td>
                            <td><?php echo $station?></td>                            
                           
                            <td><?php
                                echo '<a href="AssignCase.php?obno='.$obno.'">Assign Case</a>';

                                ?></td>
                            



                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
