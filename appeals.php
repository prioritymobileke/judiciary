<?php
session_start();
require("connect1.php");
require("config.php");

if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
header("Location:index.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Usalama Dashboard</title>
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
 <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
 
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([

         ['Month', 'Cases Filed','Cases Decided'],
         <?php 
           $query25 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='January' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row25 = mysqli_fetch_array($query25);
           $count25 = $row25['count'];
          
           $query26 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='January' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row26 = mysqli_fetch_array($query26);
          $count26 = $row26['count'];

           $query27 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='February' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row27 = mysqli_fetch_array($query27);
           $count27 = $row27['count'];
          
           $query28 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='February' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row28 = mysqli_fetch_array($query28);
          $count28 = $row28['count'];

          $query29 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='March' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row29= mysqli_fetch_array($query29);
           $count29 = $row29['count'];
          
           $query30 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='March' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row30 = mysqli_fetch_array($query30);
          $count30 = $row30['count'];
           $query31 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='April' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row31= mysqli_fetch_array($query31);
           $count31 = $row31['count'];
          
           $query32 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='April' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row32 = mysqli_fetch_array($query32);
          $count32 = $row32['count'];
           $query33 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='May' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row33= mysqli_fetch_array($query33);
           $count33 = $row33['count'];
          
           $query34 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='May' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row34 = mysqli_fetch_array($query34);
          $count34 = $row34['count'];
           $query35 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='June' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row35= mysqli_fetch_array($query35);
           $count35 = $row35['count'];
          
           $query36 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='June' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row36 = mysqli_fetch_array($query36);
          $count36 = $row36['count'];
           $query37 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='July' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row37= mysqli_fetch_array($query37);
           $count37 = $row37['count'];
          
           $query38 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='July' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row38 = mysqli_fetch_array($query38);
          $count38 = $row38['count'];
           $query39 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='August' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row39= mysqli_fetch_array($query39);
           $count39 = $row39['count'];
          
           $query40= mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='August' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row40 = mysqli_fetch_array($query40);
          $count40 = $row40['count'];
           $query41 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='September' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row41= mysqli_fetch_array($query41);
           $count41 = $row41['count'];
          
           $query42 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='September' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row42 = mysqli_fetch_array($query42);
          $count42 = $row42['count'];
           $query43 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='October' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row43= mysqli_fetch_array($query43);
           $count43 = $row43['count'];
          
           $query44= mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='October' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row44 = mysqli_fetch_array($query44);
          $count44= $row44['count'];
           $query45= mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='November' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row45= mysqli_fetch_array($query45);
           $count45 = $row45['count'];
          
           $query46 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='November' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row46 = mysqli_fetch_array($query46);
          $count46= $row46['count'];
           $query47 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='December' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

           $row47= mysqli_fetch_array($query47);
           $count47= $row47['count'];
          
           $query48 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='December' AND year = 2014 AND division ='Criminal' AND case_type ='Appeals'");

          $row48 = mysqli_fetch_array($query48);
          $count48 = $row48['count'];



          ?>
           
           ['Jan',<?php echo $count25; ?>,<?php echo $count26; ?>],
           ['Feb',<?php echo $count27; ?>,<?php echo $count28; ?>],
           ['Mar',<?php echo $count29; ?>,<?php echo $count30; ?>],
           ['Apr',<?php echo $count31; ?>,<?php echo $count32; ?>],
           ['May',<?php echo $count33; ?>,<?php echo $count34; ?>],
           ['Jun',<?php echo $count35; ?>,<?php echo $count36; ?>],
           ['Jul',<?php echo $count37; ?>,<?php echo $count38; ?>],
           ['Aug',<?php echo $count39; ?>,<?php echo $count40; ?>],
           ['Sep',<?php echo $count41; ?>,<?php echo $count42; ?>],
           ['Oct',<?php echo $count43; ?>,<?php echo $count44; ?>],
           ['Nov',<?php echo $count45; ?>,<?php echo $count46; ?>],
           ['Dec',<?php echo $count47; ?>,<?php echo $count48; ?>],
    

         
      ]);

        var options = {
          title: 'Appeals Cases 2014'
        };
      var chart = new google.visualization.ColumnChart(document.getElementById("line1chart"));
      chart.draw(data, options);
  }
  </script>
   <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([

         ['Month', 'Cases Filed','Cases Decided'],
         <?php 
           $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='January' AND year = 2015 AND division ='Criminal' AND case_type ='CR'");

           $row1 = mysqli_fetch_array($query1);
           $count1 = $row1['count'];
          
           $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='January' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row2 = mysqli_fetch_array($query2);
          $count2 = $row2['count'];

           $query3 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='February' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row3 = mysqli_fetch_array($query3);
           $count3 = $row3['count'];
          
           $query4 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='February' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row4 = mysqli_fetch_array($query4);
          $count4 = $row4['count'];

          $query5 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='March' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row5= mysqli_fetch_array($query5);
           $count5 = $row5['count'];
          
           $query6 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='March' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row6 = mysqli_fetch_array($query6);
          $count6 = $row6['count'];
           $query7 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='April' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row7= mysqli_fetch_array($query7);
           $count7 = $row7['count'];
          
           $query8 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='April' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row8 = mysqli_fetch_array($query8);
          $count8 = $row8['count'];
           $query9 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='May' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row9= mysqli_fetch_array($query9);
           $count9 = $row9['count'];
          
           $query10 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='May' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row10 = mysqli_fetch_array($query10);
          $count10 = $row10['count'];
           $query11 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='June' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row11= mysqli_fetch_array($query11);
           $count11 = $row11['count'];
          
           $query12 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='June' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row12 = mysqli_fetch_array($query12);
          $count12 = $row12['count'];
           $query13 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='July' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row13= mysqli_fetch_array($query13);
           $count13 = $row13['count'];
          
           $query14 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='July' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row14 = mysqli_fetch_array($query14);
          $count14 = $row14['count'];
           $query15 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='August' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row15= mysqli_fetch_array($query15);
           $count15 = $row15['count'];
          
           $query16= mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='August' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row16 = mysqli_fetch_array($query16);
          $count16 = $row16['count'];
           $query17 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='September' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row17= mysqli_fetch_array($query17);
           $count17 = $row17['count'];
          
           $query18 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='September' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row18 = mysqli_fetch_array($query18);
          $count18 = $row18['count'];
           $query19 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='October' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row19= mysqli_fetch_array($query19);
           $count19 = $row19['count'];
          
           $query20= mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='October' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row20 = mysqli_fetch_array($query20);
          $count20= $row20['count'];
           $query21= mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='November' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row21= mysqli_fetch_array($query21);
           $count21 = $row21['count'];
          
           $query22 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='November' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row22 = mysqli_fetch_array($query22);
          $count22= $row22['count'];
           $query23 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases where
           month_filed ='December' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

           $row23= mysqli_fetch_array($query23);
           $count23= $row23['count'];
          
           $query24 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases where
           month_decided ='December' AND year = 2015 AND division ='Criminal' AND case_type ='Appeals'");

          $row24 = mysqli_fetch_array($query24);
          $count24 = $row24['count'];



          ?>
           
           ['Jan',<?php echo $count1; ?>,<?php echo $count2; ?>],
           ['Feb',<?php echo $count3; ?>,<?php echo $count4; ?>],
           ['Mar',<?php echo $count5; ?>,<?php echo $count6; ?>],
           ['Apr',<?php echo $count7; ?>,<?php echo $count8; ?>],
           ['May',<?php echo $count9; ?>,<?php echo $count10; ?>],
           ['Jun',<?php echo $count11; ?>,<?php echo $count12; ?>],
           ['Jul',<?php echo $count13; ?>,<?php echo $count14; ?>],
           ['Aug',<?php echo $count15; ?>,<?php echo $count16; ?>],
           ['Sep',<?php echo $count17; ?>,<?php echo $count18; ?>],
           ['Oct',<?php echo $count19; ?>,<?php echo $count20; ?>],
           ['Nov',<?php echo $count21; ?>,<?php echo $count22; ?>],
           ['Dec',<?php echo $count23; ?>,<?php echo $count24; ?>],
    

         
      ]);

        var options = {
          title: 'Appeals Cases 2015'
        };
      var chart = new google.visualization.ColumnChart(document.getElementById("linechart"));
      chart.draw(data, options);
  }
  </script>
  
  </script>
  
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
</head>

<body>

<div class="mainwrapper">
    
    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">
                <li class="odd">


                </li>
                <li>

                </li>
                <li class="odd">

                </li>
                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
    
    <div class="leftpanel">
        
    <?php include"left_menu.php";?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Dashboard</li>

        </ul>
        
      <br><br>
        
        <div class="">
            <div class="">
                <div class="">
                    <div id="dashboard-left" class="span12">
                   
           
       
       <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title"></h3>
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    <a href="appeals1.php">View In Line Charts</a>
                    <td id="line1chart" style="width: 900px; height: 400px; align:center"></td>
                  </tr></table>
                  <?php
                  require('config.php');
                  
                  $query1 = mysqli_query($link,"SELECT SUM(number_of_cases) as count FROM filed_cases WHERE division = 'Criminal' AND
                   year = 2014 AND case_type = 'Appeals' ");
                   
                   $row1 = mysqli_fetch_array($query1);                   
                   $rate1 = $row1['count'];
                   
                   
                   $query2 = mysqli_query($link,"SELECT SUM(number_of_cases) as count FROM decided_cases WHERE division = 'Criminal' AND
                   year = 2014 AND case_type = 'Appeals' ");
                   
                   $row2 = mysqli_fetch_array($query2);                   
                   $rate2 = $row2['count'];
                   
                   
                   $final = round( ($rate2/$rate1)* 100 ,2 );
                   
                   echo '<h4>Total Cases Filed: &nbsp;&nbsp;'.$rate1.'</h4><br>';
                   echo '<h4>Total Cases Decided: &nbsp;&nbsp;'.$rate2.'</h4><br>';
                   echo '<h4>Clearing rate: &nbsp;&nbsp;'.$final.'% </h4>';
                  
                  ?>
                
                </div>
                </div><!-- /.box-body-->
              </div><!-- /.box -->
              <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title"></h3>
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    
                    <td id="linechart" style="width: 900px; height: 400px; "></td>
                  </tr></table>
                  <?php
                  require('config.php');
                  
                  $query1 = mysqli_query($link,"SELECT SUM(number_of_cases) as count FROM filed_cases WHERE division = 'Criminal' AND
                   year = 2015 AND case_type = 'Appeals' ");
                   
                   $row1 = mysqli_fetch_array($query1);                   
                   $rate1 = $row1['count'];
                   
                   
                   $query2 = mysqli_query($link,"SELECT SUM(number_of_cases) as count FROM decided_cases WHERE division = 'Criminal' AND
                   year = 2015 AND case_type = 'Appeals' ");
                   
                   $row2 = mysqli_fetch_array($query2);                   
                   $rate2 = $row2['count'];
                   
                   
                   $final = round( ($rate2/$rate1)* 100 ,2 );
                   
                   echo '<h4>Total Cases Filed: &nbsp;&nbsp;'.$rate1.'</h4><br>';
                   echo '<h4>Total Cases Decided: &nbsp;&nbsp;'.$rate2.'</h4><br>';
                   echo '<h4>Clearing rate: &nbsp;&nbsp;'.$final.'% </h4>';
                  
                  ?>
                
                </div>
                </div><!-- /.box-body-->
              </div>
                             </div>
            </div><!-- /.col -->           

                        
                    </div><!--span8-->
                    

                </div><!--row-fluid-->
                
                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->


  
<script type="text/javascript" src="js/jQuery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.1.1.minssssss.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="js/modernizrssss.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.uniformsss.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>

<script type="text/javascript" src="js/custom.js"></script>

  


    <!-- Page script -->
     
</body>
</html>