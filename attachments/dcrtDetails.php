<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
    header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
      
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

         <?php include"left_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">


        <div class="maincontent">
            <div class="maincontentinner">

                <h4 class="widgettitle" align="center"><?php echo $_REQUEST['caseType']; ?>|<a href="reports/scripts/today_traffic_cases.php" style="color:#fff">  Export <img src="images/images/excel.png" align="center" width="20"></a></h4>
                
           <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0"></th>
						<th class="head0">Date</th>
                        <th class="head0">CaseNo</th>
                        <th class="head0">Reg Date</th>                        
                        <th class="head0">Prev. Court</th>
                        <th class="head0">Case Type</th>
                        <th class="head0">Judge Name</th>
						<th class="head0">Coming For</th>
						<th class="head0">Outcome</th>
                        <th class="head0">Adj.Reason</th>
                        <th class="head0">Date of next activity</th>                        
                        <th class="head0">Plaintiffs/appellants</th>
                        <th class="head0">Defendants/accused</th>
                        <th class="head0">legal representation</th>
						<th class="head0">witnesses today</th>
						<th class="head0">Remanded in custody</th>
                        <th class="head0">Other Details</th>						                      

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");                   
					   
					$query="select * From cases where CaseType='".$_REQUEST['caseType']."' and NextCourtDate='".date("Y-m-d",time())."'";
                    $result=mysql_query($query);
					while($row=mysql_fetch_array($result))
                    { 
				     ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            <td><?php
                                echo '<a href="newCases.php?caseNo='.$row['CaseNo'].'">Proceed with case</a>';

                                ?></td>

                            <td><?php echo $row['CaseNo']?></td>                            
                            
							<?php  
							$query2="select * From parties where caseNo='".$row['CaseNo']."'";
							$result2=mysql_query($query2);
							?>	
								<td>
								<?php
                                while($row2=mysql_fetch_array($result2))
								{
								if($row2['Role']=="Applicant") echo $row2['PName']."<br>";
								}
								
								?>	
								</td>
                                <td>
								<?php 
								while($row2=mysql_fetch_array($result2))
								{
					if($row2['Role']=="Respondent")echo $row2['PName']."<br>";
								}
					           ?>           
							   </td>
								<td>
								<?php
								while($row2=mysql_fetch_array($result2))
								{
								if($row2['Role']=="Intrested") echo $row2['PName']."<br>";
								}								
								?>
								</td>
							
							
							
							<td><?php echo $row['CaseDetails']?></td>
                            <td><?php echo date("d-M-Y",$row['TimeStamp']) ?></td>
                          

                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
