<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))
{
header("Location:index.php");
}
include"functions.php";
$court="Homa Bay";
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
	
    
    <link rel="stylesheet" href="css/responsive-tables.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-responsive.min.css">


    <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
	 <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/forms.css" type="text/css">
   	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
	 <script src="js/app.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/custom.js"></script>	
	
	<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
	<script>
	webshims.setOptions('forms-ext', {types: 'date'});
	webshims.polyfill('forms forms-ext');
	$.webshims.formcfg = {
	en: {
		dFormat: '-',
		dateSigns: '-',
		patterns: {
			d: "yy-mm-dd"
		}
	}
	};
	</script>
	
    <script type="text/javascript">
        jQuery(document).ready(function(){
			
			jQuery('.numbersOnly').keyup(function () {
			if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
			   this.value = this.value.replace(/[^0-9\.]/g, '');
			}
		      });
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
			
		
	  jQuery("#add_doctor").click(function(e){
      jQuery('#add_doctor').attr('disabled',true);
	  jQuery('#add_doctor').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {	
            caseNo:jQuery("#set_case_No").val(),		  
			id_number:jQuery("#doctor_id").val(),		
			email:jQuery("#doctor_email").val(),
			name_of_party:jQuery("#doctor_name").val(),
			phone_number:jQuery("#doctor_phone").val(),
			postal_address:jQuery("#doctor_address").val(),
			doctor:"yes"
		},
		  function(data,status){
			
			jQuery("#doctor_id").val("")
			jQuery("#doctor_name").val(""),
			jQuery("#doctor_phone").val("")
			jQuery("#specialization").val("")
			jQuery("#doctor_email").val("")
			jQuery("#doctor_address").val("")
			jQuery('#add_doctor').attr('disabled',false);
			jQuery('#add_doctor').val('SUBMITED');
	        jQuery('#add_doctor').val('SUBMIT');
			
			
			
		  });
		
	});	
	
	
	
		jQuery("#add_investigator").click(function(e){
      jQuery('#add_investigator').attr('disabled',true);
	  jQuery('#add_investigator').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {			
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#investigator_id").val(),
			station:jQuery("#station").val(),
			email:jQuery("#investigator_email").val(),
			name_of_party:jQuery("#investigator_name").val(),
			phone_number:jQuery("#investigator_phone").val(),
			postal_address:jQuery("#investigator_address").val(),
			investigator:"yes"
		},
		  function(data,status){
			
			jQuery("#investigator_id").val("")
			jQuery("#investigator_name").val("")
			jQuery("#investigator_phone").val("")
			jQuery("#specialization").val("")
			jQuery("#investigator_email").val("")
			jQuery("#investigator_address").val("")
			jQuery('#add_investigator').attr('disabled',false);
			jQuery('#add_investigator').val('SUBMITED');
	        jQuery('#add_investigator').val('SUBMIT');
			
			
			
		  });
		
	});

jQuery("#int_parties").click(function(e){
      jQuery('#int_parties').attr('disabled',true);
	  jQuery('#int_parties').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#id_number").val(),
			name_of_party:jQuery("#name_of_party").val(),
			dob:jQuery("#int_dob").val(),
			lawyer_id:jQuery("#int_lawyer_id").val(),
			gender:jQuery("#int_gender").val(),
			phone_number:jQuery("#phone_number").val(),
			postal_address:jQuery("#postal_address").val(),
			party:"yes"
		},
		  function(data,status){
			
			jQuery("#id_number").val("")
			jQuery("#name_of_party").val("")
			jQuery("#phone_number").val("")
			jQuery("#postal_address").val("")
			jQuery("#int_dob").val("")
			jQuery("#int_gender").val("")
			jQuery('#int_parties').attr('disabled',false);
			jQuery('#int_parties').val('SUBMITED');
	        jQuery('#int_parties').val('Click to add');
			
			
			
		  });
		
	});	
			


	jQuery("#add_respondent").click(function(e){
      jQuery('#add_respondent').attr('disabled',true);
	  jQuery('#add_respondent').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#resp_id").val(),
			name_of_party:jQuery("#resp_name").val(),
			dob:jQuery("#resp_dob").val(),
			lawyer_id:jQuery("#resp_lawyer_id").val(),
			gender:jQuery("#resp_gender").val(),
			phone_number:jQuery("#resp_phone").val(),
			postal_address:jQuery("#resp_address").val(),
			crime_resp:"yes"
		},
		  function(data,status){
			
			jQuery("#resp_id").val("")
			jQuery("#resp_name").val(""),
			jQuery("#resp_phone").val("")
			jQuery("#resp_dob").val("")
			jQuery("#resp_gender").val("")
			jQuery("#resp_address").val("")
			jQuery('#add_respondent').attr('disabled',false);
			jQuery('#add_respondent').val('SUBMITED');
	        jQuery('#add_respondent').val('Click to add');
			
			
			
		  });
		
	});		

	
	jQuery("#add_accused").click(function(e){
		
      jQuery('#add_accused').attr('disabled',true);
	  jQuery('#add_accused').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#accused_id").val(),
			name_of_party:jQuery("#accused_name").val(),
			dob:jQuery("#accused_dob").val(),
			gender:jQuery("#accused_gender").val(),
			lawyer_id:jQuery("#acc_lawyer_id").val(),
			phone_number:jQuery("#accused_phone").val(),
			postal_address:jQuery("#accused_address").val(),
			accu_custody:jQuery("#accu_custody").val(),			
			crime_accused:"yes"
		},
		  function(data,status){
			
			jQuery("#accused_id").val("")
			jQuery("#accused_name").val(""),
			jQuery("#accused_phone").val("")
			jQuery("#accused_dob").val("")
			jQuery("#accused_gender").val("")
			jQuery("#accused_address").val("")
			jQuery('#add_accused').attr('disabled',false);
			jQuery('#add_accused').val('SUBMITED');
	        jQuery('#add_accused').val('Click to add');
			
			
			
		  });
		
	});	
		
	
	jQuery("#add_applicant").click(function(e){
      jQuery('#add_applicant').attr('disabled',true);
	  jQuery('#add_applicant').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#applicant_id").val(),
			dob:jQuery("#applicant_dob").val(),
			gender:jQuery("#applicant_gender").val(),
			lawyer_id:jQuery("#app_lawyer_id").val(),
			name_of_party:jQuery("#applicant_name").val(),
			phone_number:jQuery("#applicant_phone").val(),
			postal_address:jQuery("#applicant_address").val(),
			crime_applicant:"yes"
		},
		  function(data,status){
			
			jQuery("#applicant_id").val("")
			jQuery("#applicant_name").val(""),
			jQuery("#applicant_phone").val("")
			jQuery("#applicant_dob").val("")
			jQuery("#applicant_gender").val("")
			jQuery("#applicant_address").val("")
			jQuery('#add_applicant').attr('disabled',false);
			jQuery('#add_applicant').val('SUBMITED');
	        jQuery('#add_applicant').val('Click to add');
			
			
			
		  });
		
	});	


	jQuery("#add_witness").click(function(e){
      jQuery('#add_witness').attr('disabled',true);
	  jQuery('#add_witness').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#witness_id").val(),
			dob:jQuery("#witness_dob").val(),
			gender:jQuery("#witness_gender").val(),			
			name_of_party:jQuery("#witness_name").val(),
			phone_number:jQuery("#witness_phone").val(),
			postal_address:jQuery("#witness_address").val(),
			crime_witness:"yes"
		},
		  function(data,status){
			
			jQuery("#witness_id").val("")
			jQuery("#witness_name").val(""),
			jQuery("#witness_phone").val("")
			jQuery("#witness_dob").val("")
			jQuery("#witness_gender").val("")
			jQuery("#witness_address").val("")
			jQuery('#add_witness').attr('disabled',false);
			jQuery('#add_witness').val('SUBMITED');
	        jQuery('#add_witness').val('Click to add');
			
			
			
		  });
		
	});	
	jQuery("#add_petitioner").click(function(e){
      jQuery('#add_petitioner').attr('disabled',true);
	  jQuery('#add_petitioner').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#petitioner_id").val(),
			dob:jQuery("#petitioner_dob").val(),
			gender:jQuery("#petitioner_gender").val(),
			name_of_party:jQuery("#petitioner_name").val(),
			phone_number:jQuery("#petitioner_phone").val(),
			postal_address:jQuery("#petitioner_address").val(),
			family_petitioner:"yes"
		},
		  function(data,status){
			
			jQuery("#petitioner_id").val("")
			jQuery("#petitioner_name").val(""),
			jQuery("#petitioner_phone").val("")
			jQuery("#petitioner_dob").val("")
			jQuery("#petitioner_gender").val("")
			jQuery("#petitioner_address").val("")
			jQuery('#add_petitioner').attr('disabled',false);
			jQuery('#add_petitioner').val('SUBMITED');
	        jQuery('#add_petitioner').val('Click to add');
			
			
			
		  });
		
	});
	
    jQuery("#add_objector").click(function(e){
      jQuery('#add_objector').attr('disabled',true);
	  jQuery('#add_objector').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#objector_id").val(),
			dob:jQuery("#objector_dob").val(),
			gender:jQuery("#objector_gender").val(),
			name_of_party:jQuery("#objector_name").val(),
			phone_number:jQuery("#objector_phone").val(),
			postal_address:jQuery("#objector_address").val(),
			family_objector:"yes"
		},
		  function(data,status){
			
			jQuery("#objector_id").val("")
			jQuery("#objector_name").val(""),
			jQuery("#objector_phone").val("")
			jQuery("#objector_dob").val("")
			jQuery("#objector_gender").val("")
			jQuery("#objector_address").val("")
			jQuery('#add_objector').attr('disabled',false);
			jQuery('#add_objector').val('SUBMITED');
	        jQuery('#add_objector').val('Click to add');
			
			
			
		  });
		
	});
	
	jQuery("#add_protester").click(function(e){
      jQuery('#add_protester').attr('disabled',true);
	  jQuery('#add_protester').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#protester_id").val(),
			dob:jQuery("#protester_dob").val(),
			gender:jQuery("#protester_gender").val(),
			name_of_party:jQuery("#protester_name").val(),
			phone_number:jQuery("#protester_phone").val(),
			postal_address:jQuery("#protester_address").val(),
			family_protester:"yes"
		},
		  function(data,status){
			
			jQuery("#protester_id").val("")
			jQuery("#protester_name").val(""),
			jQuery("#protester_phone").val("")
			jQuery("#protestor_dob").val("")
			jQuery("#protestor_gender").val("")
			jQuery("#protester_address").val("")
			jQuery('#add_protester').attr('disabled',false);
			jQuery('#add_protester').val('SUBMITED');
	        jQuery('#add_protester').val('Click to add');
			
			
			
		  });
		
	});
	

jQuery('#set_case_No').keyup(function () {
     
	
	jQuery.post("JProcessor.php",
		  {
		  
			set_case_No:jQuery("#set_case_No").val()		
		},
		  function(data,status){     
			
			
			
		  });
		
	});	
        });
		
		function reset_form_element (e) {
			e.wrap('<form>').parent('form').trigger('reset');
			e.unwrap();
		}
		
	function upload_file(){
		
		if(jQuery("#filedate").val()==""){
			
			alert("Date of file must be entered first");			
            reset_form_element( $('#attach') );
			$('#attach').preventDefault();
				
			}else{
    
	jQuery('#uploaded').val('Attaching.......');
	var attach = document.getElementById("attach");
	var caseNo = jQuery("#set_case_No").val();
    var filedate = jQuery("#filedate").val();	
    var file = attach.files[0];
	
    formData = new FormData();
	
    formData.append('attach', file);
	formData.append('caseNo', caseNo);
    formData.append('filedate', filedate);	
		//var formData = new FormData($('#attach')[0]);		
  jQuery.ajax({
    url: 'upload_letters.php',
    type: 'POST',
    data: formData,
	enctype: 'multipart/form-data',
    async: true,
    cache: false,
    contentType: false,
    processData: false,
	
    success: function (returndata) {
		
		jQuery('#uploaded').val("Attached");
		jQuery('#att').html(returndata);
	}
  });
}
}	
		
			function reset_form_element (e) {
			e.wrap('<form>').parent('form').trigger('reset');
			e.unwrap();
		}
		
	function upload_file(){
		
		if(jQuery("#filedate").val()==""){
			
			alert("Date of file must be entered first");			
            reset_form_element( $('#attach') );
			$('#attach').preventDefault();
				
			}else{
    
	jQuery('#uploaded').val('Attaching.......');
	var attach = document.getElementById("attach");
	var caseNo = jQuery("#set_case_No").val();
    var filedate = jQuery("#filedate").val();	
    var file = attach.files[0];
	
    formData = new FormData();
	
    formData.append('attach', file);
	formData.append('caseNo', caseNo);
    formData.append('filedate', filedate);	
		//var formData = new FormData($('#attach')[0]);		
  jQuery.ajax({
    url: 'upload_letters.php',
    type: 'POST',
    data: formData,
	enctype: 'multipart/form-data',
    async: true,
    cache: false,
    contentType: false,
    processData: false,
	
    success: function (returndata) {
		
		jQuery('#uploaded').val("Attached");
		jQuery('#att').html(returndata);
	}
  });
}
}	
		
		
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">
         <?php include"reg_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">       

        <div class="maincontent">
            <div class="maincontentinner">
	<form action="regCriminalCases.php" method="post"/>
       <h4 class="widgettitle" align="center">	   
	   <?php echo $_REQUEST['caseType']; ?>
	   
	   &nbsp&nbsp&nbsp Case No: <input  type="text" id="set_case_No" name="set_case_No" placeholder=" Enter Case Number"
        VALUE="<?php if(isset($_SESSION['case_no'])) echo $_SESSION['case_no']; ?>"/>( Enter case number before entering any details)
        <br>
		<?php
	if(isset($_POST['register_case'])){
		
		
	   if($_POST['caseType']=="PA"):	
		
		$caseNo=mysql_real_escape_string($_POST['caseNo']);
		$caseType=mysql_real_escape_string($_POST['caseType']);
		$CaseState=mysql_real_escape_string($_POST['CaseState']);
		$casecode=mysql_real_escape_string($_POST['casecode']);
		
        $caseDetails="<b>Deceased Name<b> :".mysql_real_escape_string($_POST['deceased_name'])."<br>"
		."<b>Death Date<b> :".mysql_real_escape_string($_POST['death_date'])."<br>"
		."<b>Date of gazettement<b> :".mysql_real_escape_string($_POST['date_of_gazettement'])."<br>"
		."<b>Date of confirmation<b> :".mysql_real_escape_string($_POST['death_of_confirmation'])."<br>";
		
         if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}	
			
		elseif($_POST['caseType']=="Ad litem"):
		
			$caseNo=mysql_real_escape_string($_POST['caseNo']);
			$caseType=mysql_real_escape_string($_POST['caseType']);
			$casecode=mysql_real_escape_string($_POST['casecode']);
			$CaseState=mysql_real_escape_string($_POST['CaseState']);
			$caseDetails="<b>Deceased Name<b> :".mysql_real_escape_string($_POST['deceased_name'])."<br>"
		     ."<b>Death Date<b> :".mysql_real_escape_string($_POST['death_date'])."<br>";
			
			
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`Division`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','Family','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
		
		elseif($_POST['caseType']=="Citation"):
		
			$caseNo=mysql_real_escape_string($_POST['caseNo']);            
		    $caseType=mysql_real_escape_string($_POST['caseType']);
			$casecode=mysql_real_escape_string($_POST['casecode']);
			$CaseState=mysql_real_escape_string($_POST['CaseState']);
            $caseDetails="<b>Deceased Name<b> :".mysql_real_escape_string($_POST['deceased_name'])."<br>"
		    ."<b>Death Date<b> :".mysql_real_escape_string($_POST['death_date'])."<br>"
		    ."<b>Citor ID<b> :".mysql_real_escape_string($_POST['citor_id_number'])."<br>"
		    ."<b>Citee ID<b> :".mysql_real_escape_string($_POST['citee_id_number'])."<br>";			
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`Division`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','Family','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
			
			elseif($_POST['caseType']=="Adoption"):
		
			$caseNo=mysql_real_escape_string($_POST['caseNo']);            
		    $caseType=mysql_real_escape_string($_POST['caseType']);
			$CaseState=mysql_real_escape_string($_POST['CaseState']);
            $caseDetails="<b>Name of Child<b> :".mysql_real_escape_string($_POST['name_of_child'])."<br>";			
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`Division`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','Family','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
	   endif;
		
		?>
	<script>document.location="regFamilyCases.php?caseType=<?php echo $_REQUEST['caseType'];?>";</script>
  <?php		
	}
	?>
	   </h4>
   
	<input  type="hidden" name="caseNo" id="caseNo" value="<?php echo case_no();?>" />
	<input type="hidden" name="caseType" value="<?php echo $_REQUEST['caseType'];?>" />
	
	
	<div class="col-md-12">	
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Petition - P&A 80 </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
		
	<p class="form_input">Succession case number<br><input  type="text" id="applicant_name" /></p>
	<p class="form_input">Petitioners name<br><input  type="text" id="applicant_id" /></p>
	<p class="form_input">Name of Deceased<br><input  type="date" id="applicant_dob" /></p>
	<p class="form_input">Place of Death<br><input  type="date" id="applicant_dob" /></p>
	
	 <div class="mdbox" style="clear:left;">	
	<input type="file" id="attach" OnChange="upload_file()" name="attach"/><br><br>
	 <input type='button' id="uploaded"  value='Select file to attach'/>
	<div id="att">

	</div>
	</div>
	<div class="form-group form_input footer">	
	<label>Witnesses</label>
		<input  class="col-md-3" type="text" placeholder="Name" id="wt_name" />
		<input  class="col-md-3" type="text" placeholder="Phone" id="wt_phone" />
		<input  class="col-md-3" type="text" placeholder="Address" id="wt_address" />
		<input  class="col-md-3" type="text" placeholder="ID Number" id="wt_idNo" />
		
	    <input  type="button" id="add_wt" value="Click to add more witnesses" />
	</div>
	
	
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	
	
	<div class="col-md-12">	
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Affidavit - P&A 5 </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
		
	<p class="form_input">Supporter of Affidavit<br><input  type="text" id="supporter_name" /></p>
	<p class="form_input">Address of Affidavit<br><input  type="text" id="address" /></p>
	<p class="form_input">Date of Petition<br><input  type="date" id="date_of petition" /></p>
	<p class="form_input">Date of  Death<br><input  type="date" id="date_of_death" /></p>
	<p class="form_input">Death Certificate Number<br><input  type="text" id="death_cert_no" /></p>
	
	<div class="form-group form_input footer">	
	<label>Survivors</label>
		<input  class="col-md-3" type="text" placeholder="Name" id="survivor_name" />
		<input  class="col-md-3" type="text" placeholder="age" id="survivor_age" />
		<input  class="col-md-3" type="text" placeholder="ID Number" id="survivort_idNo" />
		<select class="col-md-3" type="text" id="survivor_gender" >
		<option value="">Gender</option>
		<option value="Male">Male</option>
		<option value="Female">Female</option>
		</select>
		<input  class="col-md-3" type="text" placeholder="ID Number" id="survivor_idNo" />
		
	    <input  type="button" id="add_survivor" value="Click to add more survivors" />
	</div>
	
	
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	
	
	
	<div class="col-md-12">	
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Affidavit of Means - P&A 12</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
		
	<p class="form_input">Succession Case No<br><input  type="text" id="supporter_name" /></p>
	<p class="form_input">Deceased Name<br><input  type="text" id="address" /></p>
	<p class="form_input">Date of Death<br><input  type="date" id="date_of petition" /></p>
	<p class="form_input">Net worth<br><input  type="date" id="date_of_death" /></p>
	<p class="form_input">Sworn At<br><input  type="text" id="death_cert_no" /></p>
	<p class="form_input">Date<br><input  type="text" id="death_cert_no" /></p>
	<div class="form-group form_input footer">	
	<label>Proposed Administrator</label>
		<input  class="col-md-3" type="text" placeholder="Name" id="admin_name" />
		<input  class="col-md-3" type="text" placeholder="Adress" id="admin_address" />
		<input  class="col-md-3" type="text" placeholder="ID Number" id="admin_idNo" />
		<select class="col-md-3" type="text" id="admin_gender" >
		<option value="">Gender</option>
		<option value="Male">Male</option>
		<option value="Female">Female</option>
		</select>
		<input  class="col-md-3" type="text" placeholder="Phone Number" id="admin_phone" />
		<input  class="col-md-3" type="email" placeholder="Email" id="admin_email" />
	    <input  type="button" id="add_admin" value="Click to add an Administrator" />
	</div>
	
	
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	
	
	<div class="col-md-12">	
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Affidavit of Justification of Proposed Sureties - P&A 11</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
		
	<p class="form_input">Administrator Name<br><input  type="text" id="adm_name" /></p>	
	<p class="form_input">Net worth<br><input  type="date" id="net_worth" /></p>
	<p class="form_input">Sworn At<br><input  type="text" id="sworn_at" /></p>
	<p class="form_input">Date<br><input  type="text" id="date" /></p>
	<div class="form-group form_input footer">	
	<label>Sureties</label>
		<input  class="col-md-3" type="text" placeholder="Name" id="surety_name" />
		<input  class="col-md-3" type="text" placeholder="Agw" id="surety_age" />
		<input  class="col-md-3" type="text" placeholder="ID Number" id="surety_idNo" />
		<select class="col-md-3" type="text" id="surety_gender" >
		<option value="">Gender</option>
		<option value="Male">Male</option>
		<option value="Female">Female</option>
		</select>
		<input  class="col-md-3" type="text" placeholder="Phone Number" id="surety_phone" />
		
	    <input  type="button" id="add_admin" value="Click to add Surety" />
	</div>
	
	
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	
		
	<div class="col-md-12">	
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Guarantee of Personal Surety P&E 57</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
		
	<p class="form_input">Succession Case No<br><input  type="text" id="succession_case_no" /></p>	
	<p class="form_input">Deceased Name<br><input  type="date" id="deceased_name" /></p>
	<p class="form_input">Administrator - Name<br><input  type="text" id="administrator_name" /></p>
	<p class="form_input">Surety Amount<br><input  type="text" id="surety_amout" /></p>
	<p class="form_input">Date<br><input  type="date" id="date" /></p>

	
	
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	
	
	<?php if($_REQUEST['caseType']=="Citation"):?>

	<div class="col-md-12">	
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Citor </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
	
	<p class="form_input">Citor ID Number<br><input  type="text" name="citor_id_number" /></p>
    <p class="form_input">Citee ID Number<br><input  type="text" name="citee_id_number" /></p>
	
	        </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	</div>
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="Adoption"):?>
	<div class="col-md-12">	
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Applicant  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
		
	<p class="form_input">Name of Applicant<br><input  type="text" id="applicant_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="applicant_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="applicant_dob" /></p>
	<p class="form_input">Gender<br><select id="applicant_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="applicant_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="applicant_address" /></p>
	  <div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label>
	<input  type="button" id="add_applicant" value="Click to add" />
	</div>
	
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="Adoption"):?>
	<div class="col-md-12">
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Child  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
	
		
	<p class="form_input">Name of Child<br><input  type="text" name="name_of_child" /></p>  	
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	</div>
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="PA"||$_REQUEST['caseType']=="Ad litem"):?>
	<div class="col-md-12">
	   <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Petitioners </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
		
	<p class="form_input">Name of Petitioner<br><input  type="text" id="petitioner_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="petitioner_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="petitioner_dob" /></p>
	<p class="form_input">Gender<br><select id="petitioner_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="petitioner_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="petitioner_address" /></p>
	  <div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label>
	<input  type="button" id="add_petitioner" value="Click to add" />
	</div>	
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="PA"):?>
	<div class="col-md-12">
	  <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Objectors  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
		
	<p class="form_input">Name of objector<br><input  type="text" id="objector_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="objector_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="objector_dob" /></p>
	<p class="form_input">Gender<br><select id="objector_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="objector_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="objector_address" /></p>
	  <div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label>
	<input  type="button" id="add_objector" value="Click to add" />
	</div>		
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="PA"):?>
	<div class="col-md-12">
	   <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">protesters </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
	
	<p class="form_input">Name of protester<br><input  type="text" id="protester_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="protester_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="protester_dob" /></p>
	<p class="form_input">Gender<br><select id="protestor_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="protester_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="protester_address" /></p>
	  <div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label>
	<input  type="button" id="add_protester" value="Click to add" />
	</div>
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

	<?php endif;?>
	<?php if($_REQUEST['caseType']!="Adoption"):?>	
	<div class="col-md-12">	
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Deceased Details  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
	
	<p class="form_input">Name of Deceased<br><input  type="text" name="deceased_name" /></p>
    <p class="form_input">Date of Death<br><input  type="date" name="death_date" /></p>	
    <p class="form_input">Place of Death<br><input  type="text" name="place_of_death" /></p>
    <p class="form_input">Certificate of Death NO.<br><input  type="date" name="cert_no" /></p>		
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
		
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="PA"):?>	
	<div class="col-md-12">	
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Gazettement  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
	
	<p class="form_input">Date of Gazettement<br><input  type="date" name="date_of_gazettement" /></p>
    <p class="form_input">Date of Confirmation<br><input  type="date" name="death_of_confirmation" /></p>
		
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	
	<?php endif;?>
	
	
	<div class="col-md-12">
	 <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Other details  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
		
	<p class="form_input">Case Code<br><select name="casecode"  id="casecode" />
	<?php
	$result=mysql_query("select * from casecodes");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['codename']."'>".$row['codename']."</option>";
	}	
	?>
	</select>
	</p>
	<p class="form_input">Case is coming for <br><select name="casestate"  id="casestate" />
	<?php
	$result=mysql_query("select * from case_states");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['state_name']."'>".$row['state_name']."</option>";
	}	
	?>
	</select>
	</p>
	  <div class="form-group form_input footer col-md-3">
	<p class="form_input"><input  type="submit" name="register_case" value="SUBMIT" /></p>
	</div>
	
	
	
	  </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->	
	</form>
                <div class="footer">	
			  
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
