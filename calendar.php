<?php include"header.php";?>
    <div class="leftpanel">
<?php include "menu_switch.php";?>
       <?php
	   if($_SESSION['role']=='Police'){		
         include 'js/custom_police.php';		 
	   }
	   elseif($_SESSION['role']=='Prison'){	
        include 'js/custom.php';		
	   }
	   else{		
        include 'js/custom.php';		
	   }
	   ?>
    </div><!-- leftpanel -->

    <div class="rightpanel">

        <div class="maincontent">
            <div class="maincontentinner">
			<form id="filtres" method="post" action="calendar.php"/>
			<h4 class="widgettitle" align="center">Filters: &nbsp&nbsp&nbsp
			<select name="fcasetype" id="fcasetype" class="filtres">
			<option value="">Case Type</option>
			<option value="Criminal Misc">Criminal Misc</option>
			<option value="Murder">Murder</option>
			<option value="Ordinary Cr. Appeals">Ordinary Cr. Appeals</option>
			<option value="Capital Appeals">Capital Appeals</option>
			<option value="Criminal revision">Criminal revision</option>
			<option value="PA">P&A </option>
			
			</select>
			
			<select name="fgender" id="fgender" class="filtres">
			<option value="">Gender</option>
			<option value="Female">Female</option>
			<option value="Male">Male</option>
			</select>
			
			<select name="Age" id="fage" class="filtres">
			<option value="">Age</option>
			<option value="child">Children</option>
			<option value="adult">Adults</option>
			</select>
			
			<select name="fstation" id="fstation" class="filtres" />
			<option value="">Station</option>
	<?php
	include('config.php');
	$result=mysqli_query($link,"select * from station");
	while($row=mysqli_fetch_array($result)){
		echo"<option value='".$row['StationName']."'>".$row['StationName']."</option>";
	}
	
	?>
	</select>	
	<select name="fcourt"  id="fcourt" class="filtres" />
	<option value="">Court</option>
	<?php
	$result=mysqli_query($link,"SELECT *
    FROM highcourts");
	while($row=mysqli_fetch_array($result)){
	echo"<option value='".$row['courtname']."'>".$row['courtname']."</option>";
	}
    $result=mysqli_query($link,"SELECT *
    FROM magistratecourts");
	while($row=mysqli_fetch_array($result)){
	echo"<option value='".$row['courtname']."'>".$row['courtname']."</option>";
	}	
	?>
	</select>
	
	<select name="judge" id="fjudge" class="filtres" disabled />
	<option value="">Judicial Officer</option>
	<?php
	$result=mysqli_query($link,"select * from judicial_officers");
	while($row=mysqli_fetch_array($result)){
		echo"<option value='".$row['MagistrateName']."'>".$row['MagistrateName']."</option>";
	}	
	?>
	</select>
	<input type="number" name="wt_remaining" id="wt_remaining" class="filtres" placeholder="Witnesses Remaining" />		
			
			
			</h4><br>
				<h4 class="widgettitle" align="center">			
			
			
			<select name="fadvocates" id="fadvocates" class="filtres" />
			<option value="">Advocates</option>
			<?php
			$result=mysqli_query($link,"select * from lawyer");
			while($row=mysqli_fetch_array($result)){
				echo"<option value='".$row['LawyerId']."'>".$row['Name']."</option>";
			}	
			?>
			</select>			
			<select name="fdoctors" id="fdoctors" class="filtres" />
			<option value="">Doctors</option>
			<?php
			$result=mysqli_query($link,"select * from doctorses group by IdNo ");
			while($row=mysqli_fetch_array($result)){
				echo"<option value='".$row['IdNo']."'>".$row['Names']."</option>";
			}	
			?>
			</select>
			<select name="finvestigator" id="finvestigator" class="filtres" />
			<option value="">Investigating Officer</option>
			<?php
			$result=mysqli_query($link,"select * from Investigator group by BadgeNo");
			while($row=mysqli_fetch_array($result)){
				echo"<option value='".$row['BadgeNo']."'>".$row['Name']."</option>";
			}	
			?>
			
			</select>			
			<select name="fremand" id="fremand" class="filtres" >
			<option value="">In remand</option>			
			<option value="Yes">Yes</option>
			<option value="No">No</option>
		
			</select>
			
			<select name="fbail" id="fbail" class="filtres">
			<option value="">On bail</option>
			<option value="Yes">Yes</option>
			<option value="No">No</option>>
			</select>
			
		    <select name="foutstanding" id="foutstanding" class="filtres">
			<option value="">Days outstanding</option>
			<option value="0">0-180 days</option>
			<option value="180">180-360 days</option>
			<option value="360">360 - 540 days</option>
			<option value="540">540 - 720 days</option>
			<option value="720">720-900 days</option>
			<option value="900">900-1080 days</option>
			<option value="1080">1080-1260days</option>
			<option value="1260">1260- 1440days.</option>   
			<option value="1440">1440 - 1620 days</option>
			<option value="1620">1620 - 1800 days</option>
			<option value="1800">1800 - 1980 days</option>
			<option value="1980">1980 and more</option>
			</select>
			
			<select name="health" id="fhealth" class="filtres" disabled>
			<option value="">Has Mental or other health issues</option>
			<option value="Yes">Yes</option>
			<option value="No">No</option>
			</select>			

			
			</h4>
			</form>
<!----------------------------------------------------------------------------------------------------->

	<div class="row-fluid sortable">
				<div class="box span12">
				  <div class="box-header" data-original-title>
					  <h2><i class="halflings-icon white calendar"></i><span class="break"></span>Calendar</h2>
				  </div>
				  <div class="box-content">
	                    <div id="dialog" class="span12 "></div>
						<div id="main_calendar" class="span7 offset1 "></div>
						<br><br><br><br><br><br>
						<div class="span2 offset1 ">
									
						<button type="button" class="btn btn-warning"></button> Recess
						<hr>
						<button type="button" class="btn" style="background-color:#5cb85c"></button> Weekend
						<hr>
						<button type="button" class="btn btn-danger"></button> Holiday
                                                </div>
						<div class="clearfix"></div>
					</div>
					<br><br>
				</div>
			</div><!--/row-->
<?php include"footer_cal.php";?>