<?php include"header3.php";?>
<?php
require_once dirname(__FILE__)."/src/phpfreechat.class.php";
$params = array();
$params["title"] = "Help Desk";
$params["nick"] = "Danstan";  // setup the intitial nickname
$params['firstisadmin'] = true;
//$params["isadmin"] = true; // makes everybody admin: do not use it on production servers ;)
$params["serverid"] = md5(__FILE__); // calculate a unique id for this chat
$params["debug"] = false;
$chat = new phpFreeChat( $params );

?>     
    <div class="leftpanel">        
    <?php include"left_menu.php";?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Dashboard</li>

        </ul>
        
      
        
        <div class="maincontent">
            <div class="maincontentinner">
                <div class="row-fluid">
                    <div id="dashboard-left" class="span12">
	                 
            <div class="">
              <!-- Bar chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa laptop"></i>
                  <h3 class="box-title">Live Chat</h3>
                 
                </div>
                <div class="box-body">
                <?php $chat->printChat(); ?>
  <?php if (isset($params["isadmin"]) && $params["isadmin"]) { ?>
    <p style="color:red;font-weight:bold;">Warning: because of "isadmin" parameter, everybody is admin. Please modify this script before using it on production servers !</p>
  <?php } ?>
                
                
                
                  
                </div><!-- /.box-body-->
              </div><!-- /.box -->
             </div><!-- /.col -->
			
            </div><!-- /.col -->           

                        
                    </div><!--span8-->                   

                </div><!--row-fluid-->
<?php include"footer3.php";?>