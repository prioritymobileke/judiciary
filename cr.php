<?php
session_start();
require("connect1.php");
require("config.php");

if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
header("Location:index.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Usalama Dashboard</title>
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
 <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
 <?php 

            $query = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE penalcode =''");
            $exec = mysqli_query($link,$query);
            while($row = mysqli_fetch_array($exec)){
             $hearing=$row['count'];           
            }
         
            $query1 = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE penalcode ='8'");
            $exec1 = mysqli_query($link,$query1);
            while($row1 = mysqli_fetch_array($exec1)){
             $hearing8=$row1['count'];           
            }
             $query2 = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE penalcode ='3'");
            $exec2 = mysqli_query($link,$query2);
            while($row2 = mysqli_fetch_array($exec2)){
             $hearing3=$row2['count'];           
            }
            $query3 = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE penalcode ='281'");
            $exec3 = mysqli_query($link,$query3);
            while($row3 = mysqli_fetch_array($exec3)){
             $hearing281=$row3['count'];           
            }
             $query4 = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE penalcode ='295'");
            $exec4 = mysqli_query($link,$query4);
            while($row4 = mysqli_fetch_array($exec4)){
             $hearing295=$row4['count'];           
            }
            $query5 = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE penalcode ='296'");
            $exec5 = mysqli_query($link,$query5);
            while($row5 = mysqli_fetch_array($exec5)){
             $hearing296=$row5['count'];           
            }
            $query6 = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE penalcode ='9'");
            $exec6 = mysqli_query($link,$query6);
            while($row6 = mysqli_fetch_array($exec6)){
             $hearing9=$row6['count'];           
            }
  ?>
 
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
 <script type="text/javascript">
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(drawChart);
    function drawChart() {
      var data = google.visualization.arrayToDataTable([

     

        ['', 'Unknown','Dissuasion From Enlistment','Concealment Of Treason','General punishment for forgery',' False statements for registers of births deaths and marriages','Definitions','Inciting To Mutiny'],
        ["PenalCode",<?php echo $hearing; ?>,<?php echo $hearing8; ?>,<?php echo $hearing3; ?>,<?php echo $hearing281; ?>,<?php echo $hearing295; ?>,<?php echo $hearing296; ?>,<?php echo $hearing9; ?>],
             
         
         
         
      ]);

        var options = {
          title: ''
        };
      var chart = new google.visualization.BarChart(document.getElementById("linechart"));
      chart.draw(data, options);
  }
  </script>
  

  
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
</head>

<body>

<div class="mainwrapper">
    
    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">
                <li class="odd">


                </li>
                <li>

                </li>
                <li class="odd">

                </li>
                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
    
    <div class="leftpanel">
        
    <?php include"left_menu.php";?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Dashboard</li>

        </ul>
        
      <br><br>
        
        <div class="">
            <div class="">
                <div class="">
                    <div id="dashboard-left" class="span12">
                   
           
       
       <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title"></h3>
                 
                </div>
                <div class="box-body">
                   
                  <table><tr>
                    
                    <td id="linechart" style="width: 900px; height: 400px; align:center"></td>
                  </tr></table>
                 
                
                </div>
                </div><!-- /.box-body-->
              </div><!-- /.box -->
            
            </div><!-- /.col -->           

                        
                    </div><!--span8-->
                    

                </div><!--row-fluid-->
                
                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->


  
<script type="text/javascript" src="js/jQuery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.1.1.minssssss.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="js/modernizrssss.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.uniformsss.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>

<script type="text/javascript" src="js/custom.js"></script>

  


    <!-- Page script -->
     
</body>
</html>