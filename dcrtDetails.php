<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
    header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
	function give_reasons(str){
        		
		jQuery('#area'+str).html("<textarea id='txt"+str+"'></textarea><br><input type='button' OnClick='save_reason(\""+str+"\")' id='btn"+str+"' value='Save'/>");
		}
		
function set_bail(str){
        		
		jQuery('#bail'+str).html("<input type='text' placeholder='Bail Amount' id='amount"+str+"'/><br><input type='button' OnClick='save_bail(\""+str+"\")' id='bail_btn"+str+"' value='Save'/>");
		}


	
		
	function save_reason(st){	
	
		jQuery("#btn"+st).val('Saving.......');
		
		 jQuery.post("JProcessor.php",
		  {		  
			reason:jQuery("#txt"+st).val(),
			pid:st,
			caseNo:"<?php echo $_REQUEST['caseNo'] ?>"				
		  },
		  function(data,status){
		
		jQuery("#btn"+st).val('Saved');
		
		  });
	
	}


	function save_bail(st){	
	
		jQuery("#bail_btn"+st).val('Saving.......');		
		 jQuery.post("JProcessor.php",
		  {		  
			bailamount:jQuery("#amount"+st).val(),
			pid:st			
		  },
		  function(data,status){		
		jQuery("#bail_btn"+st).val('Saved');
		
		  });
	
	}	

function upload_file(){
    
	jQuery('#uploaded').val('Attaching.......');
	var attach = document.getElementById("attach");
	var caseNo = jQuery("#caseNo").val();	
    var file = attach.files[0];
	
    formData = new FormData();
	
    formData.append('attach', file);
	formData.append('caseNo', caseNo);	
		//var formData = new FormData($('#attach')[0]);		
  jQuery.ajax({
    url: 'uploader.php',
    type: 'POST',
    data: formData,
	enctype: 'multipart/form-data',
    async: true,
    cache: false,
    contentType: false,
    processData: false,
	
    success: function (returndata) {
		
		jQuery('#uploaded').val("Attached");
		jQuery('#att').html(returndata);
	}
  });
   
   }


    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

         <?php include"left_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="maincontent">
            <div class="maincontentinner">

                <h4 class="widgettitle" align="center"> Case No: <?php echo $_REQUEST['caseNo']; ?>|<a href="reports/scripts/today_traffic_cases.php" style="color:#fff">  Export <img src="images/images/excel.png" align="center" width="20"></a></h4>
				
				<?php
	if(isset($_REQUEST['case_submit'])){
				
			if(!empty($_POST['party'])) {
				foreach($_POST['party'] as $pty) {		
					mysql_query("update parties set status='HEARD' WHERE PID='".$pty."'");
				}
			}
	
    if($_REQUEST['summary']!=""){	
	mysql_query("insert into case_summary (caseNo,summary)values('".$_REQUEST['caseNo']."','".$_REQUEST['summary']."')");	
	}
	

    mysql_query("update cases set 
	CaseState='".$_REQUEST['case_state']."',
	CaseStatus='".$_REQUEST['case_outcome']."',
	NextCourtDate='".$_REQUEST['nextdate']."'
	WHERE caseNo='".$_REQUEST['caseNo']."'");

  if($_REQUEST['case_outcome']='Adjournment'){
   mysql_query("update cases set 
	AdjournmentReason='".$_REQUEST['adj_reason']."'
	WHERE caseNo='".$_REQUEST['caseNo']."'");
    }	
				
	}
                ?>
                
                    		
<form class="stdform col-sm-12" id="frm" action="caseProgress.php" enctype="multipart/form-data"  method="post">


<div class="largebox">
	    <table  class="table table-bordered responsive">

                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>        <th class="head0">Date</th>
                        <th class="head0">CaseNo</th>
                        <th class="head0">Reg Date</th>                        
                        <th class="head0">Prev. Court and Officer</th>
                        <th class="head0">Case Type</th>
                        <th class="head0">Judge Name</th>
												                      

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");                   
				    if(isset($_REQUEST['casedate'])){
					$dcrtdate=$_REQUEST['casedate'];	
					} else{
					$dcrtdate=date("Y-m-d",time());	
					} 	 
					$query="select * From dcrt where caseNo='".$_REQUEST['caseNo']."'";
                    $result=mysql_query($query);
					while($row=mysql_fetch_array($result))
                    { 
				     ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						   <td><?php echo $row['date']?></td> 
                            <td><?php
                        echo '<a href="dcrtDetails.php?caseNo='.$row['caseNo'].'">'.$row['caseNo'].'</a>';

                                ?></td>

                                                      
                            
							<?php  
							$query2="select * From cases where CaseNo='".$row['caseNo']."'";
							$result2=mysql_query($query2);
							?>	
								<td>
								<?php
                                $row2=mysql_fetch_array($result2);								
								echo date("d-M-Y",$row2['TimeStamp']);				
								
								?>	
								</td>
                                <td>
								<?php 
							    echo $row2['xcourt'];
					             ?>           
							   </td>
								<td>
								<?php								
								 echo $row2['CaseCode'];															
								?>
								</td>						
							
							<td><?php 
							
			$query3="select * From judicial_officers where MagistrateId='".$row['judicial_officer']."'";
                    $result3=mysql_query($query3);
					$row3=mysql_fetch_array($result3);                  	
					echo $row3['MagistrateName']?></td>                  
                          

                        </tr>

                    <?php } ?>

                    </tbody>
                </table>	
</div>

<div class="largebox">
	            
           <table  class="table table-bordered responsive">


                    <colgroup>
                        
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
						<col class="con0" />
                        <col class="con1" />
                        
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        
						<th class="head0">Case coming for</th>
                        <th class="head0">Outcome</th>
						<th class="head0">Adjournment reason</th>
                        <th class="head0">Date of next activity</th> 
                        <th class="head0">No. of plaintiffs/appellants</th>						
                       <th class="head0">No. of defendants/accused</th>	
                     

                    </tr>
                    </thead>
                    <tbody>
					<tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span>
						
				
					   <td>
					  
                    <?php

                    require("connect1.php");                   
					   
					$query="select * From cases where caseNo='".$_REQUEST['caseNo']."'";
                    $result=mysql_query($query);
					$row=mysql_fetch_array($result);
                     
			
				   echo $row['CaseState'];
					
					?>
					</td> 
                     <td>
	               <?php
	                 echo $row['CaseStatus'];
                      ?>
					</td>			
  					<td>
					<?php
					if($row['CaseStatus']=='Adjournment'){
						
					echo $row['AdjournmentReason'];	
						
					}else{echo "N/A";}
					?>
                    </td>              
                    <td>
					<?php
                    echo $row['NextCourtDate'];
                      ?>
                    </td>	
                    <td>
					<?php
                   
	$query2m="select count(*) as male From parties where caseNo='".$row['caseNo']."'  and Gender='Male' and (Role='Applicant' or Role='Petitioner' or Role='Plaintiff' or Role='Plaintiff') ";
	
	$query2f="select count(*) as female From parties where caseNo='".$row['caseNo']."' and Gender='FEMALE' and (Role='Applicant' or Role='Petitioner' or Role='Plaintiff' or Role='Plaintiff') ";
	
	$result2m=mysql_query($query2m);
	$result2f=mysql_query($query2f);
	
	$row2m=mysql_fetch_array($result2m);
    $row2f=mysql_fetch_array($result2f);
	
	
    echo "Male=".$row2m['male']." ";
	echo "Female=".$row2f['female'];
                     ?>
                   
                    </td>
                      <td>
					<?php
                   
	$query2md="select count(*) as dmale From parties where caseNo='".$row['caseNo']."' and Gender='Male' and (Role='Accused' or Role='Respondent')";
	
	$result2md=mysql_query($query2md);	
    $row2md=mysql_fetch_array($result2md);
	
	
	
	$query2fd="select count(*) as dfemale From parties where caseNo='".$row['caseNo']."' and Gender='FEMALE' and (Role='Accused' or Role='Respondent') ";
	
	$result2fd=mysql_query($query2fd);	
    $row2fd=mysql_fetch_array($result2fd);
	
	
     echo "Male=".$row2md['dmale']." ";
	echo "Female=".$row2fd['dfemale'];	
                     ?>
                   
                    </td>						
		</tr>

                

                    </tbody>
                </table>	
</div>



<div class="largebox">	
	
	            
           <table  class="table table-bordered responsive">


                    <colgroup>
                        
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        
						<th class="head0">Do any of the parties have legal representation?</th>
                        <th class="head0">No. of witnesses testified in court today</th>
						<th class="head0">No. remanded in custody</th>
                        <th class="head0">Other important details </th>                        
                                           

                    </tr>
                    </thead>
             <tbody>
                  <?php

                    require("connect1.php");                   
					   
					$query="select * From cases where CaseNo='".$_REQUEST['caseNo']."'";
                    $result=mysql_query($query);
					$s=1;
					while($row=mysql_fetch_array($result))
                    { 
				     ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span>
			 
                         <td>
						 <?php
                   
	$query2="select count(*) as plenti From parties where caseNo='".$_REQUEST['caseNo']."' and LawyerId IS NULL";
	$result2=mysql_query($query2);
	
    $row2=mysql_fetch_array($result2);
	if($row2['plenti']>0) echo "YES";else echo "NO";    	
                     ?></td>			
                            
							
							
		
   	
							

					
                    <td>		
					<?php
					
	$query2="select count(*) as plenti From parties where caseNo='".$row['caseNo']."' and (Role='Applicant' or Role='Petitioner' or Role='Plaintiff' or Role='Plaintiff') ";
	$result2=mysql_query($query2);
	
    $row2=mysql_fetch_array($result2);
    echo $row2['plenti'];	
                     ?>
					 </td>
					<td>		 
					<?php
                   
	$query2="select count(*) as plenti From parties where caseNo='".$_REQUEST['caseNo']."' and IsCustody ='IN CUSTODY'";
	$result2=mysql_query($query2);
	
    $row2=mysql_fetch_array($result2);
      echo $row2['plenti'];    	
                     ?>
					 </td>
					 
                    <td>	<?php
	$query2="select * From parties where caseNo='".$_REQUEST['caseNo']."'";
	$result2=mysql_query($query2);
	$result3=mysql_query($query2);
	$rep=0;
while($row2=mysql_fetch_array($result2))
{
	if($row2['Role']=="Applicant"||$row2['Role']=="Petitioner"||$row2['Role']=="Complainant")
	{
	echo $row2['PName']."<br> ";
	$rep++;
	}	                                 
}
   if($rep==0) echo "REPUBLIC <br>";  
							echo "VS <br>";	
	while($row2=mysql_fetch_array($result3))
	{
		
	    if($row2['Role']=="Defendant"||$row2['Role']=="Accused"||$row2['Role']=="Respondent")
	    {
	   echo $row2['PName']."<br>";
	    }	
	}?></td> 
                                             
                           
                        </tr>

                    <?php $s++;} ?>
                    </tbody>
                </table>	
	

</div>



	

	   

<div style="clear:both;"></div> 

   

   
</form>



                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
