<?php
session_start();
require("connect1.php");
require("config.php");

if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
header("Location:index.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Usalama Dashboard</title>
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
 <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
 
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      
      google.charts.setOnLoadCallback(drawChart);
      google.charts.setOnLoadCallback(drawCharts);
     // google.charts.setOnLoadCallback(myCharts);
      
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          [ 'station','number of cases'],

          <?php
          require("config.php");
          
          $random = 1;
          $i= 0;
          $count ="";
          
          $query = mysqli_query($link,"SELECT DISTINCT Station FROM criminal_murder
                    WHERE Station !='NULL' && Station !='' ");
                  while($row = mysqli_fetch_array($query))
                  {
                    $r1 = $row['Station'];

                    $query2 = mysqli_query($link,"SELECT count(CaseID) as count FROM criminal_murder
                      WHERE Station = '".$r1."' AND CaseStatus !='case closed' HAVING count(CaseId) > 0 ");
			
		
                    while($row2 = mysqli_fetch_array($query2))
                    {
                    if($random > 1)
                    {
                    $count=$count.",";
                    }
                      $count = "'$r1'".','.$row2['count']; 
                     
                      $random++;
                      $i++;
                      
                      
          ?>
          
         [ <?php echo $count; ?>],
         
          <?php }
                      } ?>
         
         
        ]);

        var options = {title:'Ongoing Cases Per Police Station',};

        var chart = new google.visualization.ColumnChart(document.getElementById('chart2'));

        chart.draw(data, options);
      }
      
       function drawCharts() {
        var data = google.visualization.arrayToDataTable([
          [ 'case code','number of cases'],

          <?php
          require("config.php");
          
          $random = 1;
          $i= 0;
          $counter ="";
          
           $query5 = mysqli_query($link,"SELECT codename FROM casecodes");
                   while($row5 = mysqli_fetch_array($query5))
                   {

                    $code =$row5['codename'];
                    $query6 = mysqli_query($link,"SELECT count(CaseID) as count FROM criminal_murder
                WHERE CaseCode = '".$code."' AND criminal_murder.CaseStatus !='case closed'  AND criminal_murder.CaseStatus != 'Consent recorded - case closed' 
                  AND criminal_murder.CaseStatus != 'Judgment delivered- case closed' AND criminal_murder.CaseStatus != 'Consolidated- case closed'
                  AND criminal_murder.CaseStatus != 'Ruling delivered- case closed' AND criminal_murder.CaseStatus != 'Sentenced' 
                  AND criminal_murder.CaseStatus != 'Terminated' AND criminal_murder.CaseStatus != 'Struck out' 
                  AND criminal_murder.CaseStatus != 'Dismissed' HAVING count(CaseId) > 0 ");

                    while($row6 = mysqli_fetch_array($query6))
                    {
                    if($random > 1)
                    {
                    $counter = $counter.",";
                    }
                      $counter = "'$code'".','.$row6['count']; 
                      $random++;
                      $i++;
                      
                      
          ?>
          
         [ <?php echo $counter; ?>],
         
          <?php }
                      } ?>
         
         
        ]);

        var options = {title:'Case Codes',};

        var chart = new google.visualization.ColumnChart(document.getElementById('chart3'));

        chart.draw(data, options);
      }
      
      /*
      function myCharts() {
        var data = google.visualization.arrayToDataTable([
          [ 'station','number of cases'],

          <?php
          require("config.php");
          
          $random = 1;
          $i= 0;
          $countt ="";
          $query3 = mysqli_query($link,"SELECT state_name FROM case_states");
                  while($row3 = mysqli_fetch_array($query3))
                  {
                    $state_name = $row3['state_name'];

                    $query4 = mysqli_query($link,"SELECT count(CaseId) as count FROM cases JOIN case_states As cs ON 
                    cs.state_name = cases.CaseState  WHERE cases.CaseState='".$state_name."' HAVING count(CaseId) > 0 ");

                    while($row4 = mysqli_fetch_array($query4))
                    {
                    if($random > 1)
                    {
                    $countt=$countt.",";
                    }

                    //$r1='Registration/Filing';
                      $countt = "'$state_name'".','.$row4['count']; 
                      $random++;
                      $i++;
                      
                      
          ?>
          
         [ <?php echo $countt; ?>],
         
          <?php }
                      } ?>
         
         
        ]);

        var options = {title:'Ongoing Cases Per Police Station',};

        var chart = new google.visualization.ColumnChart(document.getElementById('chart4'));

        chart.draw(data, options);
      }
      */

	</script>

</head>

<body>

<div class="mainwrapper">
    
    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">
                <li class="odd">


                </li>
                <li>

                </li>
                <li class="odd">

                </li>
                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
    
    <div class="leftpanel">
        
    <?php include"left_menu.php";?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Dashboard</li>

        </ul>
        
      <br><br>
        
        <div class="">
            <div class="">
                <div class="">
                    <div id="dashboard-left" class="span12">
	                 
           
			 
			 <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <!-- <h3 class="box-title">Murder Cases 2014</h3>  -->
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    
                    <td id="chart2" style="width: 900px; height: 400px; align:center"></td> 
                  </tr></table>
                  
                  
                </div>
                </div><!-- /.box-body-->
              </div><!-- /.box -->
              
               <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <!-- <h3 class="box-title">Murder Cases 2014</h3>  -->
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    
                    <td id="chart3" style="width: 900px; height: 400px; align:center"></td> 
                   
                  </tr></table>                  
                  
                </div>
                </div><!-- /.box-body-->
              </div><!-- /.box -->
              
             

              
                        
                    </div><!--span8-->
                    

                </div><!--row-fluid-->
                
               <?php include"footer.php";?>