<?php
session_start();
require("connect1.php");
require("config.php");

if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
header("Location:index.php");
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Usalama Dashboard</title>
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
 <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/style.default.css" type="text/css" />
 
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
</head>

<body>

<div class="mainwrapper">
    
    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">
                <li class="odd">


                </li>
                <li>

                </li>
                <li class="odd">

                </li>
                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
    
    <div class="leftpanel">
        
    <?php include"left_menu.php";?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Dashboard</li>

        </ul>
        
      <br><br>
        
        <div class="">
            <div class="">
                <div class="">
                    <div id="dashboard-left" class="span12">
	                 
           
			 
			 <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <!-- <h3 class="box-title">Murder Cases 2014</h3>  -->
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    
                   <!-- <td id="linechart" style="width: 900px; height: 400px; align:center"></td> -->
                  </tr></table>
                  
                  
                  <?php
                  require('config.php');
                  
                   echo '<h4> Total Number of Closed Cases</h4>'; ?>

                   <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 20%" />
                        <col class="con2" style="align: center; width: 30%" /> 
                        
                    </colgroup>
                    <thead>
                    <tr>
                       
                       
                        
                        <th class="head0">Number of Cases</th>
                        

                    </tr>
                    </thead>
                    <tbody>
                <?php
                $query99 = mysqli_query($link,"SELECT count(CaseId) as total FROM criminal_murder
                  WHERE CaseStatus = 'case closed'  AND criminal_murder.CaseStatus != 'Consent recorded - case closed' 
                  AND criminal_murder.CaseStatus != 'Judgment delivered- case closed' AND criminal_murder.CaseStatus != 'Consolidated- case closed'
                  AND criminal_murder.CaseStatus != 'Ruling delivered- case closed' AND criminal_murder.CaseStatus != 'Sentenced' 
                  AND criminal_murder.CaseStatus != 'Terminated' AND criminal_murder.CaseStatus != 'Struck out' 
                  AND criminal_murder.CaseStatus != 'Dismissed' ");
                $row99 = mysqli_fetch_array($query99);
              ?>

                <tr class="gradeX">
                           
                            
                            <td><h5><?php echo $row99['total']; ?></h5></td>

                        </tr>
               
   
                    </tbody>
                </table>
<?php
                  echo '<br><br>';

                    echo '<h4> Total Number of Ongoing Cases</h4>';?>

                    <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 20%" />
                        <col class="con2" style="align: center; width: 30%" /> 
                        
                    </colgroup>
                    <thead>
                    <tr>
                       
                       
                        
                        <th class="head0">Number of Cases</th>
                        

                    </tr>
                    </thead>
                    <tbody>

                      <?php
                
                $query9 = mysqli_query($link,"SELECT count(CaseId) as total FROM criminal_murder 
                  WHERE CaseStatus != 'case closed'  AND criminal_murder.CaseStatus != 'Consent recorded - case closed' 
                  AND criminal_murder.CaseStatus != 'Judgment delivered- case closed' AND criminal_murder.CaseStatus != 'Consolidated- case closed'
                  AND criminal_murder.CaseStatus != 'Ruling delivered- case closed' AND criminal_murder.CaseStatus != 'Sentenced' 
                  AND criminal_murder.CaseStatus != 'Terminated' AND criminal_murder.CaseStatus != 'Struck out' 
                  AND criminal_murder.CaseStatus != 'Dismissed' ");
                $row9 = mysqli_fetch_array($query9);
                 ?>

                <tr class="gradeX">
                           
                            
                            <td><h5><?php echo $row9['total']; ?></h5></td>

                        </tr>
               
   
                    </tbody>
                </table>
<?php

                  echo '<br><br>';

                 /* echo '<h4> Case By Gender</h4>';
                  $query10 = mysqli_query($link,"SELECT count(PID) as count FROM parties 
                    JOIN cases on cases.CaseNo = parties.caseNo
                    WHERE parties.Gender = 'Male' AND cases.CaseStatus !='case closed' AND parties.Role='Accused' ");

                  $row10 = mysqli_fetch_array($query10);
                  echo 'Male Accused &nbsp;&nbsp;'.$row10['count'];
                  echo '<br>';

                   $query101 = mysqli_query($link,"SELECT count(PID) as count FROM parties 
                    JOIN cases on cases.CaseNo = parties.caseNo
                    WHERE parties.Gender = 'Male' AND cases.CaseStatus !='case closed' AND parties.Role='Witness' ");

                  $row101 = mysqli_fetch_array($query101);
                  echo 'Male Witness &nbsp;&nbsp;'.$row101['count'];
                  echo '<br>';

                   $query11 = mysqli_query($link,"SELECT count(PID) as count FROM parties
                   JOIN cases on cases.CaseNo = parties.caseNo 
                    WHERE Gender = 'Female' AND cases.CaseStatus !='case closed' AND parties.Role='Accused' ");

                  $row11 = mysqli_fetch_array($query11);
                  echo 'Female Accused &nbsp;&nbsp;'.$row11['count'];
                  echo '<br>';

                   $query111 = mysqli_query($link,"SELECT count(PID) as count FROM parties
                   JOIN cases on cases.CaseNo = parties.caseNo 
                    WHERE Gender = 'Female' AND cases.CaseStatus !='case closed' AND parties.Role='Witness' ");

                  $row111 = mysqli_fetch_array($query111);
                  echo 'Female Witness &nbsp;&nbsp;'.$row111['count'];


                  echo '<br><br>'; */

                   echo '<h4> Total Number of Accused Persons</h4>';
                   ?>

                    <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 20%" />
                        <col class="con2" style="align: center; width: 30%" /> 
                        
                    </colgroup>
                    <thead>
                    <tr>
                       
                       
                        
                        <th class="head0">Number of Persons</th>
                        

                    </tr>
                    </thead>
                    <tbody>

                      <?php

                   $query12 = mysqli_query($link,"SELECT count(PID) as count FROM parties
                   JOIN criminal_murder on criminal_murder.CaseNo = parties.caseNo 
                    WHERE  criminal_murder.CaseStatus !='case closed' AND criminal_murder.CaseStatus != 'Consent recorded - case closed' 
                  AND criminal_murder.CaseStatus != 'Judgment delivered- case closed' AND criminal_murder.CaseStatus != 'Consolidated- case closed'
                  AND criminal_murder.CaseStatus != 'Ruling delivered- case closed' AND criminal_murder.CaseStatus != 'Sentenced' 
                  AND criminal_murder.CaseStatus != 'Terminated' AND criminal_murder.CaseStatus != 'Struck out' 
                  AND criminal_murder.CaseStatus != 'Dismissed' AND parties.Role='Accused' ");

                  $row12 = mysqli_fetch_array($query12);
                 ?>

                <tr class="gradeX">
                           
                            
                            <td><h5><?php echo $row12['count']; ?></h5></td>

                        </tr>
               
   
                    </tbody>
                </table>
<?php

                   echo '<br><br>';
                  
                   echo '<h4> Accused On Bail</h4>';
                   ?>

                    <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 20%" />
                        <col class="con2" style="align: center; width: 30%" /> 
                        
                    </colgroup>
                    <thead>
                    <tr>
                       
                       
                        
                        <th class="head0">Number of Persons</th>
                        

                    </tr>
                    </thead>
                    <tbody>

                      <?php

                   $query12 = mysqli_query($link,"SELECT count(PID) as count FROM parties
                   JOIN criminal_murder on criminal_murder.CaseNo = parties.caseNo 
                    WHERE  criminal_murder.CaseStatus !='case closed'  AND criminal_murder.CaseStatus != 'Consent recorded - case closed' 
                  AND criminal_murder.CaseStatus != 'Judgment delivered- case closed' AND criminal_murder.CaseStatus != 'Consolidated- case closed'
                  AND criminal_murder.CaseStatus != 'Ruling delivered- case closed' AND criminal_murder.CaseStatus != 'Sentenced' 
                  AND criminal_murder.CaseStatus != 'Terminated' AND criminal_murder.CaseStatus != 'Struck out' 
                  AND criminal_murder.CaseStatus != 'Dismissed' AND parties.Role='Accused' AND parties.IsBail='YES' ");

                  $row12 = mysqli_fetch_array($query12);
                  ?>

                <tr class="gradeX">
                           
                            
                            <td><h5><?php echo $row12['count']; ?></h5></td>

                        </tr>
               
   
                    </tbody>
                </table>
<?php

                   echo '<br><br>';

                    echo '<h4> Accused In Custody</h4>';
                    ?>

                    <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 20%" />
                        <col class="con2" style="align: center; width: 30%" /> 
                        
                    </colgroup>
                    <thead>
                    <tr>
                       
                       
                        
                        <th class="head0">Number of Persons</th>
                        

                    </tr>
                    </thead>
                    <tbody>

                      <?php

                   $query13 = mysqli_query($link,"SELECT count(PID) as count FROM parties
                    JOIN criminal_murder on criminal_murder.CaseNo = parties.caseNo 
                    WHERE  criminal_murder.CaseStatus !='case closed'  AND criminal_murder.CaseStatus != 'Consent recorded - case closed' 
                  AND criminal_murder.CaseStatus != 'Judgment delivered- case closed' AND criminal_murder.CaseStatus != 'Consolidated- case closed'
                  AND criminal_murder.CaseStatus != 'Ruling delivered- case closed' AND criminal_murder.CaseStatus != 'Sentenced' 
                  AND criminal_murder.CaseStatus != 'Terminated' AND criminal_murder.CaseStatus != 'Struck out' 
                  AND criminal_murder.CaseStatus != 'Dismissed' AND parties.Role='Accused' AND parties.IsBail='NO' ");

                  $row13 = mysqli_fetch_array($query13);
                 ?>

                <tr class="gradeX">
                           
                            
                            <td><h5><?php echo $row13['count']; ?></h5></td>

                        </tr>
               
   
                    </tbody>
                </table>
<?php
                   echo '<br><br>';

                  echo '<h4> Case By Police Station</h4>';
                  ?>
                  <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 20%" />
                        <col class="con2" style="align: center; width: 30%" /> 
                        
                    </colgroup>
                    <thead>
                    <tr>
                       
                       
                        <th class="head0">Police Station</th>
                        <th class="head0">Number of Cases</th>
                        

                    </tr>
                    </thead>
                    <tbody>
                  
                  <?php
                  $query = mysqli_query($link,"SELECT DISTINCT Station FROM criminal_murder 
                    WHERE Station !='NULL' && Station !='' ");
                  while($row = mysqli_fetch_array($query))
                  {
                    $r1 = $row['Station'];

                    $query2 = mysqli_query($link,"SELECT count(CaseID) as count FROM criminal_murder
                      WHERE Station = '".$r1."' AND CaseStatus !='case closed'  AND criminal_murder.CaseStatus != 'Consent recorded - case closed' 
                  AND criminal_murder.CaseStatus != 'Judgment delivered- case closed' AND criminal_murder.CaseStatus != 'Consolidated- case closed'
                  AND criminal_murder.CaseStatus != 'Ruling delivered- case closed' AND criminal_murder.CaseStatus != 'Sentenced' 
                  AND criminal_murder.CaseStatus != 'Terminated' AND criminal_murder.CaseStatus != 'Struck out' 
                  AND criminal_murder.CaseStatus != 'Dismissed' HAVING count(CaseId) > 0 ");
			
		
                    while($row2 = mysqli_fetch_array($query2))
                    {
                      $count = $row2['count'];
		
                     
                      ?>
                       <tr class="gradeX">
                           
                            <td><h5><?php echo $r1; ?></h5></td>
                            <td><h5><?php echo $count; ?></h5></td>

                        </tr>
                <?php
                    }
             
                  }
?>
   
                    </tbody>
                </table>

<?php
                  echo '<br><br>';

                  echo '<h4> Case By Case State</h4>';
                  ?>
                  <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 20%" />
                        <col class="con2" style="align: center; width: 30%" /> 
                        
                    </colgroup>
                    <thead>
                    <tr>
                       
                       
                        <th class="head0">Case State</th>
                        <th class="head0">Number of Cases</th>
                        

                    </tr>
                    </thead>
                    <tbody>
                  <?php
                  
                  $query3 = mysqli_query($link,"SELECT state_name FROM case_states");
                  while($row3 = mysqli_fetch_array($query3))
                  {
                    $state_name = $row3['state_name'];

                    $query4 = mysqli_query($link,"SELECT count(CaseId) as count FROM criminal_murder JOIN case_states As cs ON 
                    cs.state_name = criminal_murder.CaseState  WHERE CaseState='".$state_name."' HAVING count(CaseId) > 0 ");

                    while($row4 = mysqli_fetch_array($query4))
                    {
                      $count1 = $row4['count'];
                                           
                       ?>
                       <tr class="gradeX">
                           
                            <td><h5><?php echo $state_name; ?></h5></td>
                            <td><h5><?php echo $count1; ?></h5></td>

                        </tr>
                <?php
                      
                    }
                  }
                  ?>
   
                    </tbody>
                </table>

<?php

                   echo '<br><br>';
                   
                   echo '<h4> Case By Case Codes</h4>';
                   
                   ?>
                  <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 20%" />
                        <col class="con2" style="align: center; width: 30%" /> 
                        
                    </colgroup>
                    <thead>
                    <tr>
                       
                       
                        <th class="head0">Case Code</th>
                        <th class="head0">Number of Cases</th>
                        

                    </tr>
                    </thead>
                    <tbody>
                  <?php
                   
                   $query5 = mysqli_query($link,"SELECT codename FROM casecodes");
                   while($row5 = mysqli_fetch_array($query5))
                   {

                    $code =$row5['codename'];
                    $query6 = mysqli_query($link,"SELECT count(CaseID) as count FROM criminal_murder 
                WHERE CaseCode = '".$code."' AND criminal_murder.CaseStatus !='case closed'  AND criminal_murder.CaseStatus != 'Consent recorded - case closed' 
                  AND criminal_murder.CaseStatus != 'Judgment delivered- case closed' AND criminal_murder.CaseStatus != 'Consolidated- case closed'
                  AND criminal_murder.CaseStatus != 'Ruling delivered- case closed' AND criminal_murder.CaseStatus != 'Sentenced' 
                  AND criminal_murder.CaseStatus != 'Terminated' AND criminal_murder.CaseStatus != 'Struck out' 
                  AND criminal_murder.CaseStatus != 'Dismissed' HAVING count(CaseId) > 0 ");

                    while($row6 = mysqli_fetch_array($query6))
                    {
                      $count2 = $row6['count'];
                                            
                       ?>
                       <tr class="gradeX">
                           
                            <td><h5><?php echo $code; ?></h5></td>
                            <td><h5><?php echo $count2; ?></h5></td>

                        </tr>
                <?php

                    }

                   }
                    ?>
   
                    </tbody>
                </table>

<?php

                   echo '<br><br>';
                   
                   ?>
                  <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                         <col class="con0" style="align: center; width: 10%" />
                         <col class="con1" style="align: center; width: 10%" />
                         <col class="con2" style="align: center; width: 10%" />
                         <col class="con3" style="align: center; width: 10%" />
                         <col class="con4" style="align: center; width: 20%" />
                         <col class="con5" style="align: center; width: 10%" />
                         <col class="con6" style="align: center; width: 10%" />
                         <col class="con7" style="align: center; width: 20%" />
                        <!--<col class="con1" style="align: center; width: 30%" /> -->
                        
                    </colgroup>
                    <thead>
                    <tr>
                       
                       
                        <th class="head0">Court</th>
                        <th class="head0">Case ID</th>
                        <th class="head0">Case Code</th>
                        <th class="head0">Case Type</th>
                        <th class="head0">Case Status</th>
                        <th class="head0">Case State</th>
                        <th class="head0">Station</th>
                        <th class="head0">Outstanding Days</th>
                        

                    </tr>
                    </thead>
                    <tbody>
                  <?php

                   echo '<h4> Oldest Case</h4>';
                   $quey7 =mysqli_query($link,"SELECT MIN(DateOfPlea) as mn FROM criminal_murder
                    WHERE CaseStatus !='case closed' ");
                   $row7 = mysqli_fetch_array($quey7);
                   $day = $row7['mn'];

                   $outstanding=floor((time()-strtotime($day))/(60*60*24));

                   $query8= mysqli_query($link,"SELECT * FROM criminal_murder where DateOfPlea = '".$day."' ");

                   while($row8 = mysqli_fetch_array($query8))
                   {
                    $caseid= $row8['CaseId'] ;
                    $casecode = $row8['CaseCode'] ;
                    $casetype= $row8['CaseType'] ;
                    $casestatus=$row8['CaseStatus'] ;
                    $casestate= $row8['CaseState'] ;
                    $station= $row8['Station'] ;
                    $court= $row8['Court'] ;

                   }
                  
                 
                 ?>
                       <tr class="gradeX">
                           
                            <td><h5><?php echo $court; ?></h5></td>
                            <td><h5><?php echo $caseid; ?></h5></td>
                            <td><h5><?php echo $casecode; ?></h5></td>
                            <td><h5><?php echo $casetype; ?></h5></td>
                            <td><h5><?php echo $casestatus; ?></h5></td>
                            <td><h5><?php echo $casestate; ?></h5></td>
                            <td><h5><?php echo $station; ?></h5></td>
                            <td><h5><?php echo $outstanding; ?></h5></td>

                        </tr>
                 </tbody>
                </table>
                  
                </div>
                </div><!-- /.box-body-->
              </div><!-- /.box -->
              
                        
                    </div><!--span8-->
                    

                </div><!--row-fluid-->
                
               <?php include"footer.php";?>