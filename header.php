<?php
session_start();
require("connect1.php");
if((!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))&&(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))&&(!isset($_SESSION['LawyerId']) || trim ($_SESSION['LawyerId']=='')))
{
    header("Location:index.php");
}
include"functions.php";
if(isset($_SESSION['MagistrateId'])){
	$court=$_SESSION['courtname'];
	$office=$_SESSION['courtname'];
	
	
}

if(isset($_SESSION['LawyerId'])){	
	$office=$_SESSION['firm'];
	
}
if(isset($_SESSION['BadgeNo'])&& $_SESSION['role']=="Police"){	
	$office=$_SESSION['station'];
	
}

if(isset($_SESSION['BadgeNo'])&& $_SESSION['role']=="Prison"){	
	$office=$_SESSION['station'];
	
}

$logged_user=$_SESSION['logged'];
?>
<?php
		 $query="select * From criminal_murder";
                    $result=mysqli_query($link,$query);                   
                    $a=0; $b=0; $c=0; $d=0; $e=0; $f=0; $g=0; $h=0; $i=0; $j=0;$k=0; $l=0;$m=0;         
          while($row=mysqli_fetch_array($result))
                    {
          $outstanding=floor((time()-strtotime($row['DateOfPlea']))/(60*60*24));
                  if($outstanding<=90){
                    $a++;
            } 
               elseif($outstanding<=180){
                   $b++;
            }  elseif($outstanding<=270){
                    $c++;
            } elseif($outstanding<=360){
                     $d++;
            } elseif($outstanding<=540){
                    $g++;
            } 
             elseif($outstanding<=720){
                    $e++;
            } elseif($outstanding<=1080){
                    $k++;
            } elseif($outstanding<=1440){
                    $l++;
            }         
                      else{
                    $m++;
            } 
                
          }
  
		
		
		?>
	<?php
		 $query="select * From criminal_ord_appeal";
                    $result=mysqli_query($link,$query);                   
                    $a1=0; $b1=0; $c1=0; $d1=0; $e1=0; $f1=0; $g1=0; $h1=0; $i1=0; $j1=0;$k1=0; $l1=0;$m1=0;         
          while($row=mysqli_fetch_array($result))
                    {
          $outstanding=floor((time()-strtotime($row['DateOfPlea']))/(60*60*24));
                  if($outstanding<=90){
                    $a1++;
            } 
               elseif($outstanding<=180){
                   $b1++;
            }  elseif($outstanding<=270){
                    $c1++;
            } elseif($outstanding<=360){
                     $d1++;
            } elseif($outstanding<=540){
                    $g1++;
            } 
             elseif($outstanding<=720){
                    $e1++;
            } elseif($outstanding<=1080){
                    $k1++;
            } elseif($outstanding<=1440){
                    $l1++;
            }         
                      else{
                    $m1++;
            } 
                
          }
  
		
		
		?>	
	<?php
		 $query="select * From election_petitions";
                    $result=mysqli_query($link,$query);                   
                    $a2=0; $b2=0; $c2=0; $d2=0; $e2=0; $f2=0; $g2=0; $h2=0; $i2=0; $j2=0;$k2=0; $l2=0;$m2=0;         
          while($row=mysqli_fetch_array($result))
                    {
          $outstanding=floor((time()-strtotime($row['filledDate']))/(60*60*24));
                  if($outstanding<=90){
                    $a2++;
            } 
               elseif($outstanding<=180){
                   $b2++;
            }  elseif($outstanding<=270){
                    $c2++;
            } elseif($outstanding<=360){
                     $d2++;
            } elseif($outstanding<=540){
                    $g2++;
            } 
             elseif($outstanding<=720){
                    $e2++;
            } elseif($outstanding<=1080){
                    $k2++;
            } elseif($outstanding<=1440){
                    $l2++;
            }         
                      else{
                    $m2++;
            } 
                
          }
  
		
		
		?>	
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Case Management</title>
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" type="text/css" />
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
 <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="css/responsive-tables.css">
 <link rel="stylesheet" href="css/style.default.css" type="text/css" />
 <link rel="stylesheet" href="css/forms.css" type="text/css">
   <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />	


 <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>	
	
<script>
  $(function() {
    $( document ).tooltip();
  });
 </script>
  <style>
  label {
    display: inline-block;
    width: 5em;
  }
  </style>

  <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
				dom: 'Bfrtip',
              buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
		
	
    </script>
    <script>
	 $(document).ready(function(){
	
		jQuery('.filtres').change(function(){
			
		jQuery('#filtres').submit();
	     });
		
			$("#dialog").dialog({
				autoOpen:false,
				height: 350,
				width: 700,
				modal: true,
				buttons: {
				
					Cancel: function () {
						$(this).dialog('close');
					}
				},

				close: function () {
				}
			}); 
			
			
		 jQuery("#casedate").change(function(e){
					
		jQuery("#casedateselector").submit();
					
		});	
		

       for (i = new Date().getFullYear() +1; i > new Date().getFullYear() -101; i--)
		{
			$('#startyear').append($('<option />').val(i).html(i));
			$('#endyear').append($('<option />').val(i).html(i));
		}

		

	  });
	  
	 function assign_date(date){
		alert(date);
	 }
	 </script>
	 
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
       google.charts.setOnLoadCallback(drawSarahChart);
       

      // Draw the pie chart for the Anthony's pizza when Charts is loaded.
      google.charts.setOnLoadCallback(drawAnthonyChart);
      google.charts.setOnLoadCallback(drawMbecheChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Duration ( In Days)','Murder Cases','Cr Appeal','Election'],
          ["0-90 Days",<?php echo $a; ?>,<?php echo $a1; ?>,<?php echo $a2; ?>],["90-180 Days",<?php echo $b; ?>,<?php echo $b1; ?>,<?php echo $b2; ?>],["180-270 Days",<?php echo $c; ?>,<?php echo $c1; ?>,<?php echo $c2; ?>],
          ["270-360 Days",<?php echo $d; ?>,<?php echo $d1; ?>,<?php echo $d2; ?>],
        ]);

        var options = {
          chart: {
            title: 'Cases aging',
          				 vAxis: {
   format: 0,
   minValue: 0,
   maxValue: 4
}
            
          },
          bars: 'vertical' // Required for Material Bar Charts.
        };
 
        var chart = new google.visualization.ColumnChart(document.getElementById("piechart"));
      chart.draw(data, options);
        
      }
      function drawMbecheChart() {
        var data = google.visualization.arrayToDataTable([
          ['Duration ( In Years)','Murder Cases','Cr Appeal','Election'],
          ["1 year",<?php echo $g; ?>,<?php echo $g1; ?>,<?php echo $g2; ?>], ["2 years",<?php echo $e; ?>,<?php echo $e1; ?>,<?php echo $e2; ?>],
          ["3  Years",<?php echo $k; ?>,<?php echo $k1; ?>,<?php echo $k2; ?>], ["4 years",<?php echo $l; ?>,<?php echo $l1; ?>,<?php echo $l2; ?>], 
          ["Above 5years",<?php echo $m; ?>,<?php echo $m1; ?>,<?php echo $m2; ?>],
        ]);

        var options = {
          chart: {
            title: 'Cases aging ',
            
          },
          bars: 'vertical' // Required for Material Bar Charts.
        };

        var chart = new google.visualization.ColumnChart(document.getElementById("piecharts"));
      chart.draw(data, options);
        
      }
       <?php 

            $query = ("SELECT count(CaseId) AS count,CaseType FROM criminal_murder GROUP BY CaseType");

            $exec = mysqli_query($link,$query);
            while($row = mysqli_fetch_array($exec)){
            
          $murder=$row['count'];

              
            }
            $query1 = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal");

            $exec1 = mysqli_query($link,$query1);
            while($row1 = mysqli_fetch_array($exec1)){
            
          $cr=$row1['count'];

              
            }
            $query2 = ("SELECT count(ElNo) AS count  FROM election_petitions");

            $exec2 = mysqli_query($link,$query2);
            while($row2 = mysqli_fetch_array($exec2)){
            
          $election=$row2['count'];

              
            }
         ?>
    
    function drawSarahChart() {
      var data = google.visualization.arrayToDataTable([

         ['Cases Type', 'Count'],
         ["Murder Cases",<?php echo $murder; ?>],
         ["Cr Appeal",<?php echo $cr; ?>],
         ["Elections",<?php echo $election; ?>],
         
      ]);

        var options = {
          title: ''
        };
      var chart = new google.visualization.PieChart(document.getElementById("linechart"));
      chart.draw(data, options);
  }
     <?php 

            $query = ("SELECT count(CaseId) AS count FROM criminal_murder WHERE CaseStatus ='Hearing'");
            $exec = mysqli_query($link,$query);
            while($row = mysqli_fetch_array($exec)){
             $hearing=$row['count'];           
            }
            $query1 = ("SELECT count(CaseId) AS count FROM criminal_murder WHERE CaseStatus ='Case Registered/Filed'");
            $exec1 = mysqli_query($link,$query1);
            while($row1 = mysqli_fetch_array($exec1)){
                      $filed=$row1['count'];
             
            }
            $query2 = ("SELECT count(CaseId) AS count FROM criminal_murder WHERE CaseStatus ='Mention'");
            $exec2 = mysqli_query($link,$query2);
            while($row2 = mysqli_fetch_array($exec2)){
                      $mention=$row2['count'];
             
            }
            $query3 = ("SELECT count(CaseId) AS count FROM criminal_murder WHERE CaseStatus ='Mathare'");
            $exec3 = mysqli_query($link,$query3);
            while($row3 = mysqli_fetch_array($exec3)){
                      $mathare=$row3['count'];
             
            }
            $query4 = ("SELECT count(CaseId) AS count FROM criminal_murder WHERE CaseStatus ='Unknown'");
            $exec4 = mysqli_query($link,$query4);
            while($row4 = mysqli_fetch_array($exec4)){
                      $unknown=$row4['count'];
             
            }
            $query5 = ("SELECT count(CaseId) AS count FROM criminal_murder WHERE CaseStatus ='New Case'");
            $exec5 = mysqli_query($link,$query5);
            while($row5 = mysqli_fetch_array($exec5)){
                      $new=$row5['count'];
             
            }
         ?>
         <?php 

            $queryc = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE CaseStatus ='Hearing'");
            $execc = mysqli_query($link,$queryc);
            while($rowc = mysqli_fetch_array($execc)){
             $hearingc=$rowc['count'];           
            }
            $query1c = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE CaseStatus ='Case Registered/Filed'");
            $exec1c = mysqli_query($link,$query1c);
            while($row1c = mysqli_fetch_array($exec1c)){
                      $filedc=$row1c['count'];
             
            }
            $query2c = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE CaseStatus ='Mention'");
            $exec2c = mysqli_query($link,$query2c);
            while($row2c = mysqli_fetch_array($exec2c)){
                      $mentionc=$row2c['count'];
             
            }
            $query3c = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE CaseStatus ='Mathare'");
            $exec3c = mysqli_query($link,$query3c);
            while($row3c = mysqli_fetch_array($exec3c)){
                      $matharec=$row3c['count'];
             
            }
            $query4c = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE CaseStatus ='Unknown'");
            $exec4c = mysqli_query($link,$query4c);
            while($row4c = mysqli_fetch_array($exec4c)){
                      $unknownc=$row4c['count'];
             
            }
            $query5c = ("SELECT count(CaseId) AS count FROM criminal_ord_appeal WHERE CaseStatus ='New Case'");
            $exec5c = mysqli_query($link,$query5c);
            while($row5c = mysqli_fetch_array($exec5c)){
                      $newc=$row5c['count'];
             
            }
            $link_address = '#';
         ?>
      
    
    function drawAnthonyChart() {
      var data = google.visualization.arrayToDataTable([

        ['Cases', 'Mention','Registered','Mention','Mathare','Unknown','New Case'],
        ["Murder",<?php echo $hearing; ?>,<?php echo $filed; ?>,<?php echo $mention; ?>,<?php echo $mathare; ?>,<?php echo $unknown;?>,<?php echo $new;?>],
        ["Cr Appeals",<?php echo $hearingc; ?>,<?php echo $filedc; ?>,<?php echo $mentionc; ?>,<?php echo $matharec; ?>,<?php echo $unknownc;?>,<?php echo $newc;?>],     
         
         
         
      ]);

        var options = {
          title: '',
     
        };
        
      var chart = new google.visualization.ColumnChart(document.getElementById("lines"));
      chart.draw(data, options);
       function selectHandler() {
      var selectedItem = chart.getSelection()[0];
      if (selectedItem) {
        var topping = data.getValue(selectedItem.row, 0);
        alert('The user selected ' + topping);
      }
    }

    google.visualization.events.addListener(chart, 'select', selectHandler);            
    chart.draw(data, options);
  }
  
 </script>
  <script type="text/javascript">
  function give_reasons(str){
        		
		jQuery('#area'+str).html("<textarea id='txt"+str+"'></textarea><br><input type='button' OnClick='save_reason(\""+str+"\")' id='btn"+str+"' value='Save'/>");
		}
		
function set_bail(str){
        		
		jQuery('#bail'+str).html("<input type='text' placeholder='Bail Amount' id='amount"+str+"'/><br><input type='button' OnClick='save_bail(\""+str+"\")' id='bail_btn"+str+"' value='Save'/>");
		}


	
		
	function save_reason(st){	
	
		jQuery("#btn"+st).val('Saving.......');
		
		 jQuery.post("JProcessor.php",
		  {		  
			reason:jQuery("#txt"+st).val(),
			pid:st,
			caseNo:"<?php echo $_REQUEST['caseNo'] ?>"				
		  },
		  function(data,status){
		
		jQuery("#btn"+st).val('Saved');
		
		  });
	
	}


	function save_bail(st){	
	
		jQuery("#bail_btn"+st).val('Saving.......');		
		 jQuery.post("JProcessor.php",
		  {		  
			bailamount:jQuery("#amount"+st).val(),
			pid:st			
		  },
		  function(data,status){		
		jQuery("#bail_btn"+st).val('Saved');
		
		  });
	
	}	

function upload_file(){
    
	jQuery('#uploaded').val('Attaching.......');
	var attach = document.getElementById("attach");
	var caseNo = jQuery("#caseNo").val();	
    var file = attach.files[0];
	
    formData = new FormData();
	
    formData.append('attach', file);
	formData.append('caseNo', caseNo);	
		//var formData = new FormData($('#attach')[0]);		
  jQuery.ajax({
    url: 'uploader.php',
    type: 'POST',
    data: formData,
	enctype: 'multipart/form-data',
    async: true,
    cache: false,
    contentType: false,
    processData: false,
	
    success: function (returndata) {
		
		jQuery('#uploaded').val("Attached");
		jQuery('#att').html(returndata);
	}
  });
   
   }
   
 function attach_file(){
    
	jQuery('#uploaded').val('Attaching.......');
	var attach = document.getElementById("attach");
	var caseNo = jQuery("#caseNo").val();	
    var file = attach.files[0];
	
    formData = new FormData();
	
    formData.append('attach', file);
	formData.append('caseNo', caseNo);	
		//var formData = new FormData($('#attach')[0]);		
  jQuery.ajax({
    url: 'uploader.php',
    type: 'POST',
    data: formData,
	enctype: 'multipart/form-data',
    async: true,
    cache: false,
    contentType: false,
    processData: false,
	
    success: function (returndata) {
		
		jQuery('#uploaded').val("Attached");
		jQuery('#att').html(returndata);
	}
  });
   
   }
  </script>
</head>

<body>

<div class="mainwrapper">
    
    <div class="header">
        	<div class="logo">
           
           </div>
        <div class="headerinner">
            <ul class="headmenu">
		
                <li class="odd">


                </li>
                <li>

                </li>
                <li class="odd">
   
                </li>
                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?></h5>
							
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>