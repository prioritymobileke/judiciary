<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
header("Location:index.php");
}
?>

<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">
    
  
    <link rel="stylesheet" href="css/jquery.ui.css" />
	
    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
	
	
	
	
		 <script>
	 $(document).ready(function(){
	
		jQuery('.filtres').change(function(){
			
		jQuery('#filtres').submit();
	     });
		
			$("#dialog").dialog({
				autoOpen:false,
				height: 350,
				width: 700,
				modal: true,
				buttons: {
				
					Cancel: function () {
						$(this).dialog('close');
					}
				},

				close: function () {
				}
			}); 

	  });
	  
	  
	 </script>
	
	 <?php include 'js/custom.php';?>	 
  <link rel="stylesheet" href="css/forms.css" type="text/css" />
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

       <?php include"left_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">

        <div class="maincontent">
            <div class="maincontentinner">
			<form id="filtres" method="post" action="calendar.php"/>
			<h4 class="widgettitle" align="center">Filters: &nbsp&nbsp&nbsp
			<select name="fcasetype" id="fcasetype" class="filtres">
			<option value="">Case Type</option>
			<option value="Criminal Misc">Criminal Misc</option>
			<option value="Murder">Murder</option>
			<option value="Ordinary Cr. Appeals">Ordinary Cr. Appeals</option>
			<option value="Capital Appeals">Capital Appeals</option>
			<option value="Criminal revision">Criminal revision</option>
			<option value="PA">P&A </option>
			
			</select>
			
			<select name="fgender" id="fgender" class="filtres">
			<option value="">Gender</option>
			<option value="Female">Female</option>
			<option value="Male">Male</option>
			</select>
			
			<select name="Age" id="fage" class="filtres">
			<option value="">Age</option>
			<option value="child">Children</option>
			<option value="adult">Adults</option>
			</select>
			
			<select name="fstation" id="fstation" class="filtres" />
			<option value="">Station</option>
	<?php
	$result=mysql_query("select * from station");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['StationName']."'>".$row['StationName']."</option>";
	}
	
	?>
	</select>
	
	<select name="fcourt"  id="fcourt" class="filtres" />
	<option value="">Court</option>
	<?php
	$result=mysql_query("SELECT *
    FROM highcourts");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['courtname']."'>".$row['courtname']."</option>";
	}
    $result=mysql_query("SELECT *
    FROM magistratecourts");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['courtname']."'>".$row['courtname']."</option>";
	}	
	?>
	</select>
	
			<select name="judge" id="fjudge" class="filtres" />
			<option value="">Judicial Officer</option>
	<?php
	$result=mysql_query("select * from judicial_officers");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['MagistrateName']."'>".$row['MagistrateName']."</option>";
	}	
	?>
	</select>
	<input type="number" name="wt_remaining" id="wt_remaining" class="filtres" placeholder="Witnesses Remaining" />		
			
			
			</h4><br>
				<h4 class="widgettitle" align="center">	
			
			
			
			<select name="fadvocates" id="fadvocates" class="filtres" />
			<option value="">Advocates</option>
			<?php
			$result=mysql_query("select * from lawyer");
			while($row=mysql_fetch_array($result)){
				echo"<option value='".$row['LawyerId']."'>".$row['Name']."</option>";
			}	
			?>
			</select>
			
			<select name="fdoctors" id="fdoctors" class="filtres" />
			<option value="">Doctors</option>
			<?php
			$result=mysql_query("select * from doctors");
			while($row=mysql_fetch_array($result)){
				echo"<option value='".$row['Names']."'>".$row['Names']."</option>";
			}	
			?>
			</select>
			<select name="doctors" id="fdoctors" class="filtres" />
			<option value="">Investigating Officer</option>
			<?php
			$result=mysql_query("select * from Investigator");
			while($row=mysql_fetch_array($result)){
				echo"<option value='".$row['Name']."'>".$row['Name']."</option>";
			}	
			?>
			
			</select>
			
			<select name="remand" id="fremand" class="filtres">
			<option value="">Days in remand</option>			
			<option value="1">0-180 days</option>
			<option value="2">180-360 days</option>
			<option value="3">360 - 540 days</option>
			<option value="4">540 - 720 days</option>
			<option value="5">720-900 days</option>
			<option value="6">900-1080 days</option>
			<option value="7">1080-1260days</option>
			<option value="8">1260- 1440days.</option>   
			<option value="9">1440 - 1620 days</option>
			<option value="10">1620 - 1800 days</option>
			<option value="11">1800 - 1980 days</option>
			<option value="12">1980 and more</option>
			</select>
			
			<select name="bail" id="fbail" class="filtres">
			<option value="">Days on bail</option>
			<option value="1">0-180 days</option>
			<option value="2">180-360 days</option>
			<option value="3">360 - 540 days</option>
			<option value="4">540 - 720 days</option>
			<option value="5">720-900 days</option>
			<option value="6">900-1080 days</option>
			<option value="7">1080-1260days</option>
			<option value="8">1260- 1440days.</option>   
			<option value="9">1440 - 1620 days</option>
			<option value="10">1620 - 1800 days</option>
			<option value="11">1800 - 1980 days</option>
			<option value="12">1980 and more</option>
			</select>
			
		    <select name="outstanding" id="foutstanding" class="filtres">
			<option value="">Days on outstanding</option>
			<option value="1">0-180 days</option>
			<option value="2">180-360 days</option>
			<option value="3">360 - 540 days</option>
			<option value="4">540 - 720 days</option>
			<option value="5">720-900 days</option>
			<option value="6">900-1080 days</option>
			<option value="7">1080-1260days</option>
			<option value="8">1260- 1440days.</option>   
			<option value="9">1440 - 1620 days</option>
			<option value="10">1620 - 1800 days</option>
			<option value="11">1800 - 1980 days</option>
			<option value="12">1980 and more</option>
			</select>
			
			<select name="health" id="fhealth" class="filtres">
			<option value="">Has Mental or other health issues</option>
			<option value="Yes">Yes</option>
			<option value="No">No</option>
			</select>			
	
			
			</h4>
			</form>
<!----------------------------------------------------------------------------------------------------->

	<div class="row-fluid sortable">
				<div class="box span12">
				  <div class="box-header" data-original-title>
					  <h2><i class="halflings-icon white calendar"></i><span class="break"></span>Calendar</h2>
				  </div>
				  <div class="box-content">
	                    <div id="dialog" class="span12"></div>
						<div id="main_calendar" class="span12"></div>
						

						<div class="clearfix"></div>
					</div>
				</div>
			</div><!--/row-->		
			

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->

<!-- start: JavaScript-->

	
	
		<script src='js/fullcalendar.min.js'></script>
	
		
	
		<script src="js/jquery.chosen.min.js"></script>
	
		
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>	
	
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>
		

		
</body>

</html>
