<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Usalama Dashboard</title>
 <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
 <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/style.default.css" type="text/css" />


<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->

<!-- 2013 cases -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      
      google.charts.setOnLoadCallback(drawChart);
      google.charts.setOnLoadCallback(drawCharts);

     
   

<!-- 2014 cases -->
   
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'cases filed', 'cases decided'],

          <?php
          require("config.php");

          //January 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'january' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count1 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'january' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count2 = $row2['count']; 

          //Feb 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'february' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count3 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'february' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count4 = $row2['count']; 

          //March 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'march' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count5 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'march' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count6 = $row2['count']; 

          //April 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'april' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count7 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'april' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count8 = $row2['count']; 

          //May 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'may' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count9 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'may' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count10 = $row2['count']; 

          //June 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'june' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count11 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'june' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count12 = $row2['count']; 

          //July 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'july' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count13 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'july' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count14 = $row2['count']; 

          //August 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'august' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count15 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'august' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count16 = $row2['count']; 

          //September 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'september' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count17 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'september' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count18 = $row2['count']; 

          //October 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'october' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count19 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'october' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count20 = $row2['count']; 

          //November 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'november' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count21 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'november' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count22 = $row2['count']; 

          //December 2014
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'december' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count23 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'december' AND year = 2014 
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count24 = $row2['count']; 
          
          ?>
          
          ['Jan', <?php echo $count1; ?>, <?php echo $count2; ?>],
          ['Feb', <?php echo $count3; ?>, <?php echo $count4; ?>],
          ['Mar', <?php echo $count5; ?>, <?php echo $count6; ?>],
          ['Apr', <?php echo $count7; ?>, <?php echo $count8; ?>],
          ['May', <?php echo $count9; ?>, <?php echo $count10; ?>],
          ['Jun', <?php echo $count11; ?>, <?php echo $count12; ?>],
          ['Jul', <?php echo $count13; ?>, <?php echo $count14; ?>],
          ['Aug', <?php echo $count15; ?>, <?php echo $count16; ?>],
          ['Sept', <?php echo $count17; ?>, <?php echo $count18; ?>],
          ['Oct', <?php echo $count19; ?>, <?php echo $count20; ?>],
          ['Nov', <?php echo $count21; ?>, <?php echo $count22; ?>],
          ['Dec', <?php echo $count23; ?>, <?php echo $count24; ?>],
          
         
        ]);

        var options = {title:'2014 Judicial Review',};

        var chart = new google.visualization.LineChart(document.getElementById('chart2'));

        chart.draw(data, options);
      }
   
   //January 2015
      function drawCharts() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'cases filed', 'cases decided'],

          <?php
          require("config.php");

           //Jan 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'january' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count1 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'january' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count2 = $row2['count']; 

          //Feb 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'february' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count3 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'february' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count4 = $row2['count']; 

          //March 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'march' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count5 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'march' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count6 = $row2['count']; 

          //April 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'april' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count7 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'april' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count8 = $row2['count']; 

          //May 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'may' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count9 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'may' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count10 = $row2['count']; 

          //June 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'june' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count11 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'june' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count12 = $row2['count']; 

          //July 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'july' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count13 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'july' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count14 = $row2['count']; 

          //August 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'august' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count15 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'august' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count16 = $row2['count']; 

          //September 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'september' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count17 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'september' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count18 = $row2['count']; 

          //October 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'october' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count19 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'october' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count20 = $row2['count']; 

          //November 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'november' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count21 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'november' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count22 = $row2['count']; 

          //December 2015
          $query1 = mysqli_query($link,"SELECT number_of_cases as count FROM filed_cases WHERE
           month_filed = 'december' AND year = 2015 
           AND division ='civil' AND case_type ='judicial review' ");

          $row1 = mysqli_fetch_array($query1);
          $count23 = $row1['count'];
          
          $query2 = mysqli_query($link,"SELECT number_of_cases as count FROM decided_cases WHERE
           month_decided = 'december' AND year = 2015
           AND division ='civil' AND case_type ='judicial review' ");

          $row2 = mysqli_fetch_array($query2);
          $count24 = $row2['count']; 
          ?>
          
          ['Jan', <?php echo $count1; ?>, <?php echo $count2; ?>],
          ['Feb', <?php echo $count3; ?>, <?php echo $count4; ?>],
          ['Mar', <?php echo $count5; ?>, <?php echo $count6; ?>],
          ['Apr', <?php echo $count7; ?>, <?php echo $count8; ?>],
          ['May', <?php echo $count9; ?>, <?php echo $count10; ?>],
          ['Jun', <?php echo $count11; ?>, <?php echo $count12; ?>],
          ['Jul', <?php echo $count13; ?>, <?php echo $count14; ?>],
          ['Aug', <?php echo $count15; ?>, <?php echo $count16; ?>],
          ['Sept', <?php echo $count17; ?>, <?php echo $count18; ?>],
          ['Oct', <?php echo $count19; ?>, <?php echo $count20; ?>],
          ['Nov', <?php echo $count21; ?>, <?php echo $count22; ?>],
          ['Dec', <?php echo $count23; ?>, <?php echo $count24; ?>],
          
         
        ]);

        var options = {title:'2015 Judicial Review',};

        var chart = new google.visualization.LineChart(document.getElementById('chart3'));

        chart.draw(data, options);
      }
    </script>

</head>

<body>

<div class="mainwrapper">
    
    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">
                <li class="odd">


                </li>
                <li>

                </li>
                <li class="odd">

                </li>
                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
    
    <div class="leftpanel">
        
    <?php include"left_menu.php";?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Dashboard</li>

        </ul>
      
      
 <br><br>
    
   
        <div class="">
            <div class="">
                <div class="">
                    <div id="dashboard-left" class="span12">
                   
           
       
       <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title"></h3>
                 
                </div>
                <div class="box-body">
                  <a href="judicial-review.php">View In Bar Charts</a>
                  <table><tr>
                    
                    <td id="chart2" style="width: 900px; height: 400px; align:center"></td>
                  </tr></table>
                   <?php
                  require('config.php');
                  
                  $query1 = mysqli_query($link,"SELECT SUM(number_of_cases) as count FROM filed_cases WHERE division = 'civil' AND
                   year = 2014 AND case_type = 'judicial review' ");
                   
                   $row1 = mysqli_fetch_array($query1);                   
                   $rate1 = $row1['count'];
                   
                   
                   $query2 = mysqli_query($link,"SELECT SUM(number_of_cases) as count FROM decided_cases WHERE division = 'civil' AND
                   year = 2014 AND case_type = 'judicial review' ");
                   
                   $row2 = mysqli_fetch_array($query2);                   
                   $rate2 = $row2['count'];
                   
                   
                   $final = round( ($rate2/$rate1)* 100 ,2 );
                   
                   echo '<h4>Total Cases Filed: &nbsp;&nbsp;'.$rate1.'</h4><br>';
                   echo '<h4>Total Cases Decided: &nbsp;&nbsp;'.$rate2.'</h4><br>';
                   echo '<h4>Clearing rate: &nbsp;&nbsp;'.$final.'% </h4>';
                  
                  ?>
                
                </div>
                </div><!-- /.box-body-->
              </div><!-- /.box -->
              <div class="">
              <!-- Donut chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-bar-chart-o"></i>
                  <h3 class="box-title"></h3>
                 
                </div>
                <div class="box-body">
                  
                  <table><tr>
                    
                    <td id="chart3" style="width: 900px; height: 400px; "></td>
                  </tr></table>
                   <?php
                  require('config.php');
                  
                  $query1 = mysqli_query($link,"SELECT SUM(number_of_cases) as count FROM filed_cases WHERE division = 'civil' AND
                   year = 2015 AND case_type = 'judicial review' ");
                   
                   $row1 = mysqli_fetch_array($query1);                   
                   $rate1 = $row1['count'];
                   
                   
                   $query2 = mysqli_query($link,"SELECT SUM(number_of_cases) as count FROM decided_cases WHERE division = 'civil' AND
                   year = 2015 AND case_type = 'judicial review' ");
                   
                   $row2 = mysqli_fetch_array($query2);                   
                   $rate2 = $row2['count'];
                   
                   
                   $final = round( ($rate2/$rate1)* 100 ,2 );
                   
                   echo '<h4>Total Cases Filed: &nbsp;&nbsp;'.$rate1.'</h4><br>';
                   echo '<h4>Total Cases Decided: &nbsp;&nbsp;'.$rate2.'</h4><br>';
                   echo '<h4>Clearing rate: &nbsp;&nbsp;'.$final.'% </h4>';
                  
                  ?>
                
                </div>
                </div><!-- /.box-body-->
              </div>
                             </div>
            </div><!-- /.col -->           

                        
                    </div><!--span8-->
                    

                </div><!--row-fluid-->
                
                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->
                
            </div><!--maincontentinner-->
        </div><!--maincontent-->
        
    </div><!--rightpanel-->
    
</div><!--mainwrapper-->


  
<script type="text/javascript" src="js/jQuery-2.1.3.min.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.1.1.minssssss.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
<script type="text/javascript" src="js/modernizrssss.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="js/jquery.uniformsss.min.js"></script>
<script type="text/javascript" src="js/responsive-tables.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>

<script type="text/javascript" src="js/custom.js"></script>

  


    <!-- Page script -->

</body>
</html>