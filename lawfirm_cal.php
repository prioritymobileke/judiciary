<?php
session_start();
require("connect1.php");
if((!isset($_SESSION['LawyerId']) || trim ($_SESSION['LawyerId']=='')))
{
    header("Location:index.php");
}
?>

<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">
    
    <link rel="stylesheet" href="css/forms.css" type="text/css" />
    <link rel="stylesheet" href="css/jquery.ui.css" />
	
    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
	
		 <script>
	 $(document).ready(function(){
	
		jQuery('.filtres').change(function(){
			
		jQuery('#filtres').submit();
	     });
		
			$("#dialog").dialog({
				autoOpen:false,
				height: 350,
				width: 700,
				modal: true,
				buttons: {
				
					Cancel: function () {
						$(this).dialog('close');
					}
				},

				close: function () {
				}
			}); 

	  });
	  
	  
	 </script>
	
	 <?php include 'js/custom.php';?>	 

</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

       <?php include"lawfirm_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
     
	  
	 
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">
			
			
<!----------------------------------------------------------------------------------------------------->

	<div class="row-fluid sortable">
				<div class="box span12">
				  <div class="box-header" data-original-title>
					  <h2><i class="halflings-icon white calendar"></i><span class="break"></span>Calendar</h2>
				  </div>
				  <div class="box-content">
	                    <div id="dialog" class="span12"></div>
						<div id="law_calendar" class="span12"></div>
						

						<div class="clearfix"></div>
					</div>
				</div>
			</div><!--/row-->		
			

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->

<!-- start: JavaScript-->

	
	
		<script src='js/fullcalendar.min.js'></script>
	
		
	
		<script src="js/jquery.chosen.min.js"></script>
	
		
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>	
	
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>
		

		
</body>

</html>
