<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))
{
header("Location:index.php");
}
include"functions.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">
	
    <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
	
	
    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
			
			jQuery('.numbersOnly').keyup(function () {
			if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
			   this.value = this.value.replace(/[^0-9\.]/g, '');
			}
		      });
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
			


	jQuery("#add_lawyer").click(function(e){
      jQuery('#add_lawyer').attr('disabled',true);
	  jQuery('#add_lawyer').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {			
			id_number:jQuery("#lawyer_id").val(),
			firm:jQuery("#lawyer_firm").val(),
			email:jQuery("#lawyer_email").val(),
			name_of_party:jQuery("#lawyer_name").val(),
			phone_number:jQuery("#lawyer_phone").val(),
			postal_address:jQuery("#lawyer_address").val(),
			lawyer:"yes"
		},
		  function(data,status){
			
			jQuery("#lawyer_id").val("")
			jQuery("#lawyer_name").val(""),
			jQuery("#lawyer_phone").val("")
			jQuery("#lawyer_firm").val("")
			jQuery("#lawyer_email").val("")
			jQuery("#lawyer_address").val("")
			jQuery('#add_lawyer').attr('disabled',false);
			jQuery('#add_lawyer').val('SUBMITED');
	        jQuery('#add_lawyer').val('SUBMIT');
			
			
			
		  });
		
	});		

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
       
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

         <?php include"reg_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="maincontent">
            <div class="maincontentinner">
       <h4 class="widgettitle" align="center"></h4>
	   
	   
    <form action="lawyerReg.php" method="post"/>
	
	 <div class="col-md-12">
              <div class="box box-default  col-md-12">
                <div class="box-header with-border">
                  <h3 class="box-title">Register Lawyer </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
				
				
				
				
	<div class="form_input">Advocate Name<br><input  type="text" name="lawyer_name" /></div>
	<div class="form_input">P105 Number<br><input  type="text" name="lawyer_id" /></div>
	<div class="form_input">Advocate's Firm<br><input  type="text" name="lawyer_firm" /></div>	
	<div class="form_input">Office Number<br><input  type="text" name="lawyer_office_phone" /></div>
	<div class="form_input">Mobile Number<br><input  type="text" name="lawyer_mobile_phone" /></div>
	<div class="form_input">Postal Address<br><input  type="text" name="lawyer_address" /></div>
	<div class="form_input">Advocate Email address<br><input  type="email" name="lawyer_email" /></div>
	<div class="form_input">Firm's Email Address<br><input  type="email" name="Firm_email" /></div>
	<div class="form-group form_input footer col-md-3"><input  type="submit"  name="add_lawyer" value="SUBMIT" /></div>		
	
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	

	
	</form>
  
	<?php
	if(isset($_POST['add_lawyer'])){
		
		$lawyer_name=mysql_real_escape_string($_POST['lawyer_name']);
        $lawyer_id=mysql_real_escape_string($_POST['lawyer_id']);
		$lawyer_firm=mysql_real_escape_string($_POST['lawyer_firm']);
		$lawyer_office_phone=mysql_real_escape_string($_POST['lawyer_office_phone']);
        $lawyer_mobile_phone=mysql_real_escape_string($_POST['lawyer_mobile_phone']);
		$lawyer_address=mysql_real_escape_string($_POST['lawyer_address']);
		$lawyer_email=mysql_real_escape_string($_POST['lawyer_email']);
        $Firm_email=mysql_real_escape_string($_POST['Firm_email']);		
		
         if(mysql_query("INSERT INTO `lawyer` (`Name`, `IdNo`, `LawFirm`, `PhoneNo`, `mobileNo`,`Address`, `Email`, `lawyerEmail` )
            VALUES 
			('{$lawyer_name}','{$lawyer_id}','{$lawyer_firm}','{$lawyer_office_phone}','{$lawyer_mobile_phone}','{$lawyer_address}','{$lawyer_email}','{$Firm_email}')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}		
	} 
	?>
 
               

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
