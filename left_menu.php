<div class="leftmenu">        
           <ul class="nav nav-tabs nav-stacked">
               <li class="nav-header">Navigation</li>
                 <li class="active"><a href="dashboard.php"><i class="iconfa-home"></i></span> HOME</a></li>
                   
           
        <li class="dropdown"><a href=""><span class="iconfa-pencil"></span> Incoming Cases</a>
                 <ul>
                   <li class="dropdown"><a href="newCasesTable.php"><span class="iconfa-book"></span> Criminal Division </a>
             <ul>
             <li class=""><a href="newCasesTable.php?caseType=Criminal Misc"><span class="iconfa-laptop"></span>Criminal Misc</a>
             </li>
             <li class=""><a href="newCasesTable.php?caseType=Murder"><span class="iconfa-laptop"></span> Murder</a></li>
           
             <li class=""><a href="newCasesTable.php?caseType=Criminal revision"><span class="iconfa-laptop"></span> Criminal revision</a></li>
                 <li class=""><a href="newCasesTable.php?caseType=Appeal""><span class="iconfa-laptop"></span>Criminal Ord Appeal</a></li>
       
              </ul>
           </li>
                     <li class="dropdown"><a href="newCasesTable.php.php"><span class="iconfa-book"></span> Family Division</a>
              <ul>
             <li class=""><a href="newCasesTable.php.php?caseType=PA"><span class="iconfa-laptop"></span>P&A </a>
             </li>
             <li class=""><a href="newCasesTable.php.php?caseType=Ad litem"><span class="iconfa-laptop"></span> Ad litem</a></li>
             <li class=""><a href="newCasesTable.php.php?caseType=Citation"><span class="iconfa-laptop"></span>Citation</a></li>
             <li class=""><a href="newCasesTable.php.php?caseType=Adoption"><span class="iconfa-laptop"></span>Adoption </a>
             </li>
             
           
       
              </ul>
           
           </li>
                 <li class="dropdown"><a href="newCasesTable.php"><span class="iconfa-book"></span>Civil Divisions </a>
           
           <ul>
             <li class=""><a href="newCasesTable.php?caseType=Misc.appeals"><span class="iconfa-laptop"></span>Misc.appeals </a>
             </li>
             <li class=""><a href="newCasesTable.php?caseType=Civil Appeals"><span class="iconfa-laptop"></span> Civil Appeals</a></li>
             <li class=""><a href="newCasesTable.php?caseType=Judicial Review"><span class="iconfa-laptop"></span>Judicial Review</a></li>
             <!--<li class=""><a href="newCasesTable.php?caseType=Divorce"><span class="iconfa-laptop"></span>Divorce</a>
             </li>-->
             <li class=""><a href="newCasesTable.php?caseType=Civil"><span class="iconfa-laptop"></span>Civil </a></li>
             <li class=""><a href="todayElectionPetition.php?caseType=Petition"><span class="iconfa-laptop"></span>Petition </a>
             </li>
             
           
       
              </ul>
           
           
           </li>
             <li class="dropdown"><a href=""><span class="iconfa-book"></span> Commercial Division</a>						   
             <ul>
             <li class=""><a href="newCasesTable.php?caseType=Commercial Matters"><span class="iconfa-laptop"></span>Commercial Matters</a>
             </li>
             <li class=""><a href="newCasesTable.php?caseType=Commercial High Court Misc"><span class="iconfa-laptop"></span> Commercial High Court Misc</a></li>
           <li class=""><a href="newCasesTable.php?caseType=Winding Up Cause"><span class="iconfa-laptop"></span>Winding Up Cause</a></li>
             <li class=""><a href="newCasesTable.php?caseType=Income Tax Appeal"><span class="iconfa-laptop"></span>Income Tax Appeal</a>
             </li>
           <li class=""><a href="newCasesTable.php?caseType=Bankruptcy Cause"><span class="iconfa-laptop"></span>Bankruptcy Cause</a></li>						
           
         <li class=""><a href="newCasesTable.php?caseType=Bankruptcy Notice"><span class="iconfa-laptop"></span>Bankruptcy Notice</a></li>	
       
              </ul>
           </li>
     <li class=""><a href="newElectionCasesTable.php?caseType=Election Petition&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Election Petition </a>
     </li>
     
                   </ul>
               </li>
       
      <li class="dropdown"><a href=""><span class="iconfa-pencil"></span> Case Summary</a>
                 <ul>						
           
                     <li class="dropdown"><a href=""><span class="iconfa-book"></span> Criminal Division </a>
           
             <ul>
 <li class=""><a href="summaryCriminalCases.php?caseType=Criminal Misc&y=<?php echo date("Y");?>"> <span class="iconfa-laptop"></span>Criminal Misc</a></li>
 <li class=""><a href="summaryCriminalCases.php?caseType=Murder&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span> Murder</a></li>
 <li class=""><a href="summaryCriminalCases.php?caseType=Ordinary Cr. Appeals&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Ordinary Cr. Appeals </a></li>
 <li class=""><a href="summaryCriminalCases.php?caseType=Capital Appeals&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Capital Appeals </a></li>
 <li class=""><a href="summaryCriminalCases.php?caseType=Criminal revision&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span> Criminal revision</a></li>
           
       
              </ul>
           
           
           
           </li>
           
   <li class="dropdown"><a href="summaryFamilyCases.php"><span class="iconfa-book"></span> Family Division</a>
              <ul>
             <li class=""><a href="summaryFamilyCases.php?caseType=PA"><span class="iconfa-laptop"></span>P&A </a>
             </li>
             <li class=""><a href="summaryFamilyCases.php?caseType=Ad litem"><span class="iconfa-laptop"></span> Ad litem</a></li>
             <li class=""><a href="summaryFamilyCases.php?caseType=Citation"><span class="iconfa-laptop"></span>Citation</a></li>
             <li class=""><a href="summaryFamilyCases.php?caseType=Adoption"><span class="iconfa-laptop"></span>Adoption </a>
             </li>
             
           
       
              </ul>
           
           
           
           
           
           
           
           </li>
                 <li class="dropdown"><a href="summaryCivilCases.php"><span class="iconfa-book"></span>Civil Divisions </a>
           
           <ul>
           
             <li class=""><a href="summaryCivilCases.php?caseType=Misc.appeals&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Misc.appeals </a>
             </li>
             <li class=""><a href="summaryCivilCases.php?caseType=Civil Appeals&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span> Civil Appeals</a></li>
             <li class=""><a href="summaryCivilCases.php?caseType=Judicial Review&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Judicial Review</a></li>
             <!--<li class=""><a href="summaryCivilCases.php?caseType=Divorce"><span class="iconfa-laptop"></span>Divorce</a>
             </li>-->
             <li class=""><a href="summaryCivilCases.php?caseType=Civil&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Civil </a></li>
             <li class=""><a href="summaryCivilCases.php?caseType=Petition&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Petition </a>
             </li>
             
           
       
              </ul>
           
           
           </li>
             <li class="dropdown"><a href=""><span class="iconfa-book"></span> Commercial Division</a>						   
             <ul>
             <li class=""><a href="summaryCommercialCases.php?caseType=Commercial Matters"><span class="iconfa-laptop"></span>Commercial Matters</a>
             </li>
             <li class=""><a href="summaryCommercialCases.php?caseType=Commercial High Court Misc"><span class="iconfa-laptop"></span> Commercial High Court Misc</a></li>
           <li class=""><a href="summaryCommercialCases.php?caseType=Winding Up Cause"><span class="iconfa-laptop"></span>Winding Up Cause</a></li>
             <li class=""><a href="summaryCommercialCases.php?caseType=Income Tax Appeal"><span class="iconfa-laptop"></span>Income Tax Appeal</a>
             </li>
           <li class=""><a href="summaryCommercialCases.php?caseType=Bankruptcy Cause"><span class="iconfa-laptop"></span>Bankruptcy Cause</a></li>						
           
         <li class=""><a href="summaryCommercialCases.php?caseType=Bankruptcy Notice"><span class="iconfa-laptop"></span>Bankruptcy Notice</a></li>	
       
              </ul>
           </li>				
           
                   <li class=""><a href="summaryCivilCases.php?caseType=Election Petition&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Election Petition </a>
             </li>	
     
                   </ul>
               </li>
     
       
     <li ><a href="calendar.php"><span class="iconfa-calendar"></span>Calendar</a></li>
     
            <li class=""><a href="perfomanceTargets.php"><span class="iconfa-laptop"></span>Performance & Targets</a></li>

<li class="dropdown"><a href=""><span class="iconfa-pencil"></span> Review Cases</a>
                 <ul>
                   <li class="dropdown"><a href="ReviewCases.php"><span class="iconfa-book"></span> Criminal Division </a>
             <ul>
             <li class=""><a href="ReviewCases.php?caseType=Criminal Misc"><span class="iconfa-laptop"></span>Criminal Misc</a>
             </li>
             <li class=""><a href="ReviewCases.php?caseType=Murder"><span class="iconfa-laptop"></span> Murder</a></li>
             <li class=""><a href="ReviewCases.php?caseType=Ordinary Cr. Appeals"><span class="iconfa-laptop"></span>Ordinary Cr. Appeals </a></li>
             <li class=""><a href="ReviewCases.php?caseType=Capital Appeals"><span class="iconfa-laptop"></span>Capital Appeals </a>
             </li>
             <li class=""><a href="ReviewCases.php?caseType=Criminal revision"><span class="iconfa-laptop"></span> Criminal revision</a></li>
           
       
              </ul>
           </li>
                     <li class="dropdown"><a href="ReviewCases.php"><span class="iconfa-book"></span> Family Division</a>
              <ul>
             <li class=""><a href="ReviewCases.php?caseType=PA"><span class="iconfa-laptop"></span>P&A </a>
             </li>
             <li class=""><a href="ReviewCases.php?caseType=Ad litem"><span class="iconfa-laptop"></span> Ad litem</a></li>
             <li class=""><a href="ReviewCases.php?caseType=Citation"><span class="iconfa-laptop"></span>Citation</a></li>
             <li class=""><a href="ReviewCases.php?caseType=Adoption"><span class="iconfa-laptop"></span>Adoption </a>
             </li>
             
           
       
              </ul>
           
           
                       
           
           
           </li>
                 <li class="dropdown"><a href="ReviewCases.php"><span class="iconfa-book"></span>Civil Divisions </a>						
           <ul>
           <li class=""><a href="ReviewCases.php?caseType=Election Petition&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Election Petition </a>
             </li>
             <li class=""><a href="ReviewCases.php?caseType=Misc.appeals&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Misc.appeals </a>
             </li>
             <li class=""><a href="ReviewCases.php?caseType=Civil Appeals&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span> Civil Appeals</a></li>
             <li class=""><a href="ReviewCases.php?caseType=Judicial Review&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Judicial Review</a></li>
             <!--<li class=""><a href="ReviewCases.php?caseType=Divorce"><span class="iconfa-laptop"></span>Divorce</a>
             </li>-->
             <li class=""><a href="ReviewCases.php?caseType=Civil&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Civil </a></li>
             <li class=""><a href="ReviewCases.php?caseType=Petition&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Petition </a>
             </li>
             
           
       
              </ul>
           
           
           </li>
             <li class="dropdown"><a href=""><span class="iconfa-book"></span> Commercial Division</a>						   
             <ul>
             <li class=""><a href="ReviewCases.php?caseType=Commercial Matters"><span class="iconfa-laptop"></span>Commercial Matters</a>
             </li>
             <li class=""><a href="ReviewCases.php?caseType=Commercial High Court Misc"><span class="iconfa-laptop"></span> Commercial High Court Misc</a></li>
           <li class=""><a href="ReviewCases.php?caseType=Winding Up Cause"><span class="iconfa-laptop"></span>Winding Up Cause</a></li>
             <li class=""><a href="ReviewCases.php?caseType=Income Tax Appeal"><span class="iconfa-laptop"></span>Income Tax Appeal</a>
             </li>
           <li class=""><a href="ReviewCases.php?caseType=Bankruptcy Cause"><span class="iconfa-laptop"></span>Bankruptcy Cause</a></li>						
           
         <li class=""><a href="ReviewCases.php?caseType=Bankruptcy Notice"><span class="iconfa-laptop"></span>Bankruptcy Notice</a></li>	
         
       
           </ul>
           </li>
     <li class=""><a href="ReviewCases.php?caseType=Election Petition&y=<?php echo date("Y");?>"><span class="iconfa-laptop"></span>Election Petition </a>
             </li>
                   </ul>
               </li>	
               
               
               
               
                  
               
                 <li class="dropdown"><a href=""><span class="iconfa-list"></span> Cause List</a>
                 <ul>
                   <li class="dropdown"><a href="newCasesTable.php"><span class="iconfa-book"></span> Criminal Division </a>
       <ul>
     <li class=""><a href="causecriminal.php?caseType=Criminal Misc"><span class="iconfa-laptop"></span>Criminal Misc</a>
     </li>
     <li class=""><a href="causecriminal.php?caseType=Murder"><span class="iconfa-laptop"></span> Murder</a></li>
           
     <li class=""><a href="causecriminal.php?caseType=Criminal revision"><span class="iconfa-laptop"></span> Criminal revision</a></li>
     <li class=""><a href="causecriminal2.php?caseType=Criminal Ord Appeal"><span class="iconfa-laptop"></span>Criminal Ord Appeal</a></li>
     </ul>
       </li>		
           </ul>
              </li>
              <li><a href="sentenceRegister.php"><span class="iconfa-book"></span> Sentencing Register</a>
                 
              </li>
              <li class=""><a href="dcrt.php"><span class="iconfa-laptop"></span>DCRT</a></li>
               <li class="dropdown"><a href=""><span class="iconfa-book"></span>Analytics</a>

      <ul>
       <li class="dropdown"><a href="#"><span class="iconfa-laptop"></span>General Analytics</a>
       
       <ul>
  <li class=""><a href="general.php"><span class="iconfa-laptop"></span>Tabulated General Statistics
</a></li>						
  <li class=""><a href="general-stats-graphs.php"><span class="iconfa-laptop"></span>Graphical General Statistics  
</a></li>
      </ul>        
       </li>
       
    <li class="dropdown"><a href="#"><span class="iconfa-laptop"></span>Criminal Division </a>

      <ul>
  <li class=""><a href="Murdercases.php"><span class="iconfa-laptop"></span>Murder
</a></li>						
  <li class=""><a href="cr.php"><span class="iconfa-laptop"></span>CR MISC APP   
</a></li>

<li class=""><a href="appeals.php"><span class="iconfa-laptop"></span>Appeals  
</a></li>
<li class=""><a href="revisions.php"><span class="iconfa-laptop"></span>Revisions 
</a></li>
   
              </ul>

    </li>						
    <li class="dropdown"><a href="#"><span class="iconfa-laptop"></span>Civil Division</a>


    <ul>
  <li class=""><a href="civil-misc.php"><span class="iconfa-laptop"></span>CIVIL MISC APPL</a></li>						
  <li class=""><a href="civil-cases.php"><span class="iconfa-laptop"></span>CIVIL CASES</a></li>
  <li class=""><a href="civil-appeals.php"><span class="iconfa-laptop"></span>CIVIL APPEALS</a></li>						
  <li class=""><a href="judicial-review.php"><span class="iconfa-laptop"></span>JUDICIAL REVIEW</a></li>
  <li class=""><a href="petitions.php"><span class="iconfa-laptop"></span>PETITIONS</a></li>						
  <li class=""><a href="const-hr.php"><span class="iconfa-laptop"></span>CONST.&HR</a></li>
   
    </ul>

          </li>
       </ul>       
   <li class="dropdown"><a href=""><span class="iconfa-book"></span> Periodical Reports</a>						   
                      
             <ul>
 <li class=""><a href="reports.php?outcome=heard"><span class="iconfa-laptop"></span>Cases heard</a></li>						
 <li class=""><a href="reports.php?outcome=adjournment"><span class="iconfa-laptop"></span>cases Adjourned</a></li>
 <li class=""><a href="reports.php?outcome=File transferred"><span class="iconfa-laptop"></span>Transferred</a></li>
 <li class=""><a href="reports.php?outcome=case closed"><span class="iconfa-laptop"></span>Closed</a></li>
   
   </ul>
   
 </li>
 
   <li class="dropdown"><a href=""><span class="iconfa-book"></span> Action required</a>						   
             <ul>
 <li class=""><a href="actionRequiredTable.php?period=30"><span class="iconfa-laptop"></span>Over 30 days</a></li>						
 <li class=""><a href="actionRequiredTable.php?period=60"><span class="iconfa-laptop"></span>Over 60 days</a></li>
             
 <li class=""><a href="actionRequiredTable.php?period=90"><span class="iconfa-laptop"></span> Over 90 days</a></li>	
              </ul>
       </li>			
        <li ><a href="registeredLawyers.php"><i class="iconfa-legal"></i></span>Registered Lawyers</a></li>
        
   <li class="dropdown"><a href=""><span class="iconfa-envelope"></span>Messages</a>
            <ul>
          <li ><a href="compose.php"><i class="iconfa-envelope"></i></span>Compose</a></li>
             <li ><a href="messages.php"><i class="iconfa-envelope"></i></span>Inbox</a></li>
                
            
     
              </ul> 
         </li>
<li class=""><a href="livechat.php"><span class="fa fa-comments"></span>Live Chat</a></li>
   
 <li class="dropdown"><a href=""><span class="fa fa-map"></span>Maps</a>
            <ul>
 <li ><a href="map.php?loc=Oyugis&&lng=34.7382&&lat=0.5079"><i class="fa-map"></i></span>Oyugis</a></li>
 <li ><a href="map.php?loc=Ndhiwa&&lng=34.3671&&lat=0.7299"><i class="fa-map"></i></span>Ndhiwa</a></li>
 <li ><a href="map.php?loc=Homa Bay&&lng=34.4531&&lat=0.5350"><i class="fa-map"></i></span>Homa Bay</a></li>
 <li ><a href="map.php?loc=Mbita&&lng=34.2060&&lat=0.4368"><i class="fa-map"></i></span>Mbita</a></li>
 <li ><a href="onemap.php"><i class="fa-map"></i></span>All Courts</a></li>	
            
     
              </ul> 
         </li>
       </div><!--leftmenu-->