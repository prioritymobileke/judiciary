<?php
session_start();
require("connect1.php");
if((!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))&&(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))&&(!isset($_SESSION['LawyerId']) || trim ($_SESSION['LawyerId']=='')))
{
    header("Location:index.php");
}
include"functions.php";
if(isset($_SESSION['MagistrateId'])){
	$court=$_SESSION['courtname'];
	$office=$_SESSION['courtname'];
	
}

if(isset($_SESSION['LawyerId'])){	
	$office=$_SESSION['firm'];
	
}
if(isset($_SESSION['BadgeNo'])&& $_SESSION['role']=="Police"){	
	$office=$_SESSION['station'];
	
}

if(isset($_SESSION['BadgeNo'])&& $_SESSION['role']=="Prison"){	
	$office=$_SESSION['station'];
	
}

$logged_user=$_SESSION['logged'];
?>
<?php
require_once dirname(__FILE__)."/src/phpfreechat.class.php";
$params = array();
$params["title"] = "Help Desk";
$params["nick"] = $_SESSION['names'];  // setup the intitial nickname
$params['firstisadmin'] = true;
//$params["isadmin"] = true; // makes everybody admin: do not use it on production servers ;)
$params["serverid"] = md5(4); // calculate a unique id for this chat
$params["debug"] = false;
$chat = new phpFreeChat( $params );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/responsive-tables.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-responsive.min.css">
    <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
	 <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/forms.css" type="text/css">
   	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />

   
 </head>
 <body>
 <div class="mainwrapper">
   <div class="header">
        	<div class="logo">
            <?php echo $office; ?>
           </div>
        <div class="headerinner">
            <ul class="headmenu">
		
                <li class="odd">


                </li>
                <li>

                </li>
                <li class="odd">
   
                </li>
                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
							
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>
    
    <div class="leftpanel">
<?php	
    	if(isset($_SESSION['MagistrateId'])) 
			{
			include"left_menu.php";
			}
		elseif($_SESSION['role']=='Police'){
		 include"police_menu.php";
         		 
	   }
	   elseif($_SESSION['role']=='Prison'){
		include"prison_menu.php"; 
        		
	   }
	    elseif($_SESSION['role']=='Registrar'){
		include"reg_menu.php"; 
        		
	   }
	   else{
		include"left_menu.php";
       		
	   }
	   ?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Dashboard</li>

        </ul>
        
      
        
        <div class="maincontent">
            <div class="maincontentinner">
                <div class="row-fluid">
                    <div id="dashboard-left" class="span12">
	                 
            <div class="">
              <!-- Bar chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa laptop"></i>
                  <h3 class="box-title">Live Chat</h3>
                 
                </div>
                <div class="box-body">
<!-- Place this tag where you want the Live Helper Plugin to render. -->
<div id="lhc_status_container_page" ></div>

<!-- Place this tag after the Live Helper Plugin tag. -->
<script type="text/javascript">
var LHCChatOptionsPage = {};
LHCChatOptionsPage.opt = {};
(function() {
var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
po.src = '//judiciary.prioritymobile.co.ke/livehelper/index.php/chat/getstatusembed/(leaveamessage)/true/(department)/1';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
})();
</script>        
                </div><!-- /.box-body-->
              </div><!-- /.box -->
             </div><!-- /.col -->
			
            </div><!-- /.col -->           

                        
                    </div><!--span8-->                   

                </div><!--row-fluid-->
<?php include"footer_cal.php";?>