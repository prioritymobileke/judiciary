<?php include"header.php";?>
    
    <div class="leftpanel">
        
    <?php include"left_menu.php";?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Maps</li>

        </ul>
        
      
        
        <div class="maincontent">
            <div class="maincontentinner">
                <div class="row-fluid">
                    <div id="dashboard-left" class="span12">
	                 
            <div class="">
              <!-- Bar chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  
                  <h3 class="box-title"><?php echo $_REQUEST['loc'];?></h3>
                 
                </div>
                <div class="box-body">
                 <div class="box-body" id="map" style="height:700px;">

                </div><!-- /.box-body -->
                  
                
                
                
                  
                </div><!-- /.box-body-->
              </div><!-- /.box -->
             </div><!-- /.col -->
			 
			 
            </div><!-- /.col -->           

                        
                    </div><!--span8-->                   

                </div><!--row-fluid-->
	<?php			
	$lat=$_REQUEST['lat'];
	$lng=$_REQUEST['lng'];
	$location=$_REQUEST['loc'];
  ?>	
	<script>

// This example displays a marker at the center of Australia.

// When the user clicks the marker, an info window opens.

// The maximum width of the info window is set to 200 pixels.



function initMap() {

  var uluru = {lat: <?php echo $lat;?>, lng: <?php echo $lng;?>};

  var map = new google.maps.Map(document.getElementById('map'), {

    zoom: 11,

    center: uluru

  });



  var contentString = '<div id="content">'+

      '<div id="siteNotice">'+

      '</div>'+


      '<p><b>Location: </b><?php echo $location;?> Law Courts</p> ' +
   

      '</div>'+

      '</div>';



  var infowindow = new google.maps.InfoWindow({

    content: contentString,

    maxWidth: 300

  });



  var marker = new google.maps.Marker({

    position: uluru,

    map: map,

    title: '<?php echo $location;?>'

  });

  marker.addListener('click', function() {

    infowindow.open(map, marker);

  });

}



    </script>

    <script async defer

        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVDefCDk-hF532EgTtL2QLE_Nzn9gXJ7k&signed_in=true&callback=initMap"></script>
<?php include"footer.php";?>