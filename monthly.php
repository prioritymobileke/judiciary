<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
    header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
   
   	<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
	<script>
	webshims.setOptions('forms-ext', {types: 'date'});
	webshims.polyfill('forms forms-ext');
	$.webshims.formcfg = {
	en: {
		dFormat: '-',
		dateSigns: '-',
		patterns: {
			d: "yy-mm-dd"
		}
	}
	};
	</script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>
	
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" type="text/css" />
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
				dom: 'Bfrtip',
              buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
			
			
			jQuery("#casedate").change(function(e){
					
					jQuery("#casedateselector").submit();
					
				});

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
      
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

         <?php include"left_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">


        <div class="maincontent">
            <div class="maincontentinner">

                <h4 class="widgettitle" align="center"><?php echo $_REQUEST['caseType']; ?>
				
	    <!--<form id="casedateselector" method="post" action="#"  style="float:right;">
		<p class="form_input">SELECT DATE <input type="date" name="casedate" id="casedate" placeholder="Select  Date" /></p>
		</form> -->
				
				</h4><br>
                
           <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>        <th class="head0">Date</th>
                        <th class="head0">CaseNo</th>
                        <th class="head0">Reg Date</th>                        
                        <th class="head0">Prev. Court and Officer</th>
                        <th class="head0">Case Type</th>
                        <th class="head0">Judge Name</th>
												                      

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");                   
				    if(isset($_REQUEST['casedate'])){
					$dcrtdate=$_REQUEST['casedate'];	
					} else{
					$dcrtdate=date("Y-m-d",time());	
					}

					if($_REQUEST['outcome']=='heard'){
					$query="select * From dcrt join cases on dcrt.caseNo=cases.caseNo where dcrt.judicial_officer='".$_SESSION['MagistrateId']."' and MONTH(dcrt.date) = MONTH(CURDATE()) and YEAR(dcrt.date) = YEAR(CURDATE()) and cases.CaseStatus <> 'Adjournment'";
					}
					elseif($_REQUEST['outcome']=='adjournment'){
					$query="select * From dcrt join cases on dcrt.caseNo=cases.caseNo where dcrt.judicial_officer='".$_SESSION['MagistrateId']."' and MONTH(dcrt.date) = MONTH(CURDATE()) and YEAR(dcrt.date) = YEAR(CURDATE()) and cases.CaseStatus = 'Adjournment'";
					}
					if($_REQUEST['outcome']=='File transferred'){
					$query="select * From dcrt join cases on dcrt.caseNo=cases.caseNo where dcrt.judicial_officer='".$_SESSION['MagistrateId']."' and YEARWEEK(dcrt.date, 1) = YEARWEEK(CURDATE(), 1) and cases.CaseStatus = 'File transferred'";
					}
					if($_REQUEST['outcome']=='case closed'){
					$query="select * From dcrt join cases on dcrt.caseNo=cases.caseNo where dcrt.judicial_officer='".$_SESSION['MagistrateId']."' and YEARWEEK(dcrt.date, 1) = YEARWEEK(CURDATE(), 1) and cases.CaseStatus = 'case closed'";
					}
					
                    $result=mysql_query($query);
					while($row=mysql_fetch_array($result))
                    { 
				     ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						   <td><?php echo $row['date']?></td> 
                            <td><?php
                        echo '<a href="dcrtDetails.php?caseNo='.$row['caseNo'].'">'.$row['caseNo'].'</a>';

                                ?></td>

                                                      
                            
							<?php  
							$query2="select * From cases where CaseNo='".$row['caseNo']."'";
							$result2=mysql_query($query2);
							?>	
								<td>
								<?php
                                $row2=mysql_fetch_array($result2);								
								echo date("d-M-Y",$row2['TimeStamp']);				
								
								?>	
								</td>
                                <td>
								<?php 
							    echo $row2['xcourt'];
					             ?>           
							   </td>
								<td>
								<?php								
								 echo $row2['CaseCode'];															
								?>
								</td>						
							
							<td><?php 
							
			$query3="select * From judicial_officers where MagistrateId='".$row['judicial_officer']."'";
                    $result3=mysql_query($query3);
					$row3=mysql_fetch_array($result3);                  	
					echo $row3['MagistrateName']?></td>                  
                          

                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
