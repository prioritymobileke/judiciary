

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.html"><img src="images/logo.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="images/photos/thumb1.png" alt="" />
                        <div class="userinfo">
                            <h5>Betty Riungi </h5>
                            <ul>
                                <li><a href="editprofile.html">Edit Profile</a></li>
                                <li><a href="">Account Settings</a></li>
                                <li><a href="index.html">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>

                  <li class=""><a href="dashboard.html"><i class="iconfa-home"></i></span> HOME</a></li>
<li class=""><a href="newCasesTable.php"><span class="iconfa-th-list"></span> New cases </a></li>
<li class=""><a href="newCasesTrafficTable.php"><span class="iconfa-th-list"></span> New Traffic cases </a></li>
                
                <li class=""><a href="ongoingCasesTable.php"><span class="iconfa-th-list"></span> Ongoing cases </a></li>
                <li class=""><a href="closedCasesTable.php"><span class="iconfa-th-list"></span> Terminated cases </a></li>
                <li class=""><a href="chargedSexOffenders.php"><span class="iconfa-th-list"></span> Charged sex offenders</a></li>


            </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>All cases</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">

                <h4 class="widgettitle">Cases Details Table</h4>
                <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0"></th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");
                    $case_no=$_GET['var'];
                    $query="select				 	cases.IsBroughtToCourt,,bail_bond.isBailed,bail_bond.BailAmount,bail_bond.BondAmount,surety.SuretyAmount,surety.Security,bail_bond.BailCondition,
                    bail_bond.NoCashBailReason,Remand.RemandPlace,casehistory.NextCourtDate from cases left join bail_bond on bail_bond.
                    where cases.caseNo='$case_no' ";

                    $result=mysql_query($query);

                    if(!($result=mysql_query($query)))
                    {
                        print("could not execute query !");
                        die(mysql_error());
                    }

                    while($row=mysql_fetch_array($result))
                    {

                        $brought_court=$row['broughtToCourt'];
                        $warrant=$row['warrantIssued'];//warrants
                        $dateissued=$row['dateIssued'];//warrants
                        $description=$row['description'];//warrants
                        $nowarrant=$row['noWarrantReason'];//warrants
                        $isbailed=$row['isBailed'];
                        $bailamount=$row['bailAmount'];
                        $bondamount=$row['bondAmount'];
                        $suretyamount=$row['suretyAmount'];
                        $nobailreason=$row['noBailReason'];
                        $remandname=$row['remandName'];
                        $courtdate=$row['courtDate'];
                        $bailcondition=$row['bailCondition'];

                        ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                        </tr>
                        <tr>
                            <td><th class="head0">Brought to court</th></td>
                            <td><?php echo $brought_court ?></td>
                        </tr>
                        <tr>
                            <td><th class="head0">Warrent isssued</th></td>
                            <td><?php echo $warrant ?></td>
                        </tr>
                        <tr>
                            <td><th class="head0">Warrent details</th></td>
                            <td><?php echo $description ?></td>
                        </tr>
                        <tr>
                            <td><th class="head0">Reason for no warrant</th></td>
                            <td><?php echo $nowarrant ?></td>
                        </tr>
                        <tr>
                            <td><th class="head0">Suspect bailed</th></td>
                            <td><?php echo $isbailed ?></td>
                        </tr>
                        <tr>
                            <td><th class="head0">Bailed Conditions</th></td>
                            <td><?php echo $bailcondition ?></td>
                        </tr>
                        <tr>
                            <td><th class="head0">Bail Amount</th></td>
                            <td><?php echo $bailamount ?></td>
                        </tr>
                        <tr>
                            <td><th class="head1">Bond Amount</th></td>
                            <td><?php echo $bondamount ?></td>
                        </tr>
                        <tr>
                            <td><th class="head1">Surety Amount</th></td>
                            <td><?php echo $suretyamount ?></td>
                        </tr>
                        <tr>
                            <td><th class="head0">Reason for denied bail</th></td>
                            <td><?php echo $nobailreason ?></td>
                        </tr>

                        <tr>
                            <td><th class="head1">Remand facility</th></td>
                            <td><?php echo $remandname ?></td>
                        </tr>
                        <tr>
                            <td><th class="head1">Next court date</th></td>
                            <td><?php echo $courtdate ?></td>
                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
