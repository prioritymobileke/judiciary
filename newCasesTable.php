<?php include"header.php";?>

    <div class="leftpanel">

         <?php include"left_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">





        <div class="maincontent">
            <div class="maincontentinner">

                <h4 class="widgettitle" align="center"></h4><br>


			<?php if($_REQUEST['caseType']=="Murder" ):?>
			<table id="dyntable" class="table table-bordered responsive col-md-6" style="font-size:5px;">


                    <colgroup>
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
						<col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
						 <col class="con1" />
                        <col class="con0" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0">Penal Code<br>(Section)</th>
                        <th class="head0">CaseNo</th>
						<th class="head0">Date of<br> Plea</th>
                        <th class="head0">Parties</th>
                        <th class="head0">Advocate</th>
                        <th class="head0">No. of<br> witnesses<br> heard</th>
                        <th class="head0">No. of<br> witnesses <br>remaining</th>
						<th class="head0">Days<br> Outstanding</th>
						<th class="head0">Police <br>Station</th>
						<th class="head0">Custody</th>
                        <th class="head0">Result<br>/Status</th>


                    </tr>
                    </thead>
                    <tbody>
                  <?php

                    require("connect1.php");

					$query= "select * From criminal_murder INNER JOIN parties
          ON criminal_murder.CaseNo=parties.caseNo INNER JOIN lawyer
          ON parties.LawyerId=lawyer.LawyerId where criminal_murder.NextCourtDate=''";
          $result=mysql_query($query);
					$s=1;
					while($row=mysql_fetch_array($result))
                    {
				     ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span>

	    <td><?php echo $row['penalcode'];?></td>
            <td><?php echo '<a href="assignDate.php?caseNo='.$row['CaseNo'].'">'. $row['CaseNo'].'</a>'?></td>
            <td><?php echo $row['DateOfPlea'] ?></td>
            <td><?php echo $row['PName'] ?></td>
            <td><?php echo $row['Name'] ?></td>
            <td><?php echo $row['witnessHeard'] ?></td>
            <td><?php echo $row['witnessRemaining'] ?></td>
            
            <td><?php
            

$now = time(); // or your date as well
$your_date = strtotime($row['DateOfPlea']);
$datediff = $now - $your_date;

echo floor($datediff / (60 * 60 * 24));

          ?></td> 
          <td><?php echo $row['Station'] ?></td>
            <td><?php echo $row['RemandFacility'] ?></td>
            <td><?php echo $row['CaseState'] ?></td></tr>
          </tr>

                    <?php $s++;} ?>
                    </tbody>
                </table>
                
                
                
                <?php elseif($_REQUEST['caseType']=="Appeal" ):?>
			<table id="dyntable" class="table table-bordered responsive col-md-6" style="font-size:5px;">


                    <colgroup>
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
						<col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
						 <col class="con1" />
                        <col class="con0" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0">Penal Code<br>(Section)</th>
                        <th class="head0">CaseNo</th>
						<th class="head0">Date of<br> Plea</th>
                        <th class="head0">Parties</th>
                        <th class="head0">Advocate</th>
                        <th class="head0">No. of<br> witnesses<br> heard</th>
                        <th class="head0">No. of<br> witnesses <br>remaining</th>
						<th class="head0">Days<br> Outstanding</th>
						<th class="head0">Police <br>Station</th>
						<th class="head0">Custody</th>
                        <th class="head0">Result<br>/Status</th>


                    </tr>
                    </thead>
                    <tbody>
                  <?php

                    require("connect1.php");

					$query= "select * From criminal_ord_appeal INNER JOIN parties
          ON criminal_ord_appeal.CaseNo=parties.caseNo INNER JOIN lawyer
          ON parties.LawyerId=lawyer.LawyerId where criminal_ord_appeal.NextCourtDate=''";
          $result=mysql_query($query);
					$s=1;
					while($row=mysql_fetch_array($result))
                    {
				     ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span>
            <td><?php echo $row['penalcode'];?></td>
            <td><?php echo '<a href="assignDate.php?caseNo='.$row['CaseNo'].'">'. $row['CaseNo'].'</a>'?></td>
            <td><?php echo $row['DateOfPlea'] ?></td>
            <td><?php echo $row['PName'] ?></td>
            <td><?php echo $row['Name'] ?></td>
            <td><?php echo $row['witnessHeard'] ?></td>
            <td><?php echo $row['witnessRemaining'] ?></td>
            
            <td><?php
            

$now = time(); // or your date as well
$your_date = strtotime($row['DateOfPlea']);
$datediff = $now - $your_date;

echo floor($datediff / (60 * 60 * 24));

          ?></td> 
           <td><?php echo $row['Station'] ?></td>
            <td><?php echo $row['RemandFacility'] ?></td>
            <td><?php echo $row['CaseState'] ?></td></tr>

                    <?php $s++;} ?>
                    </tbody>
                </table>
                
                 
               
				
						
						<?php else: ?>

          
			<table id="dyntable" class="table table-bordered responsive col-md-6" style="font-size:5px;">


                    <colgroup>
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
						<col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
						 <col class="con1" />
                        <col class="con0" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0">Penal Code<br>(Section)</th>
                        <th class="head0">CaseNo</th>
						<th class="head0">Date of<br> Plea</th>
                        <th class="head0">Parties</th>
                        <th class="head0">Advocate</th>
                        <th class="head0">No. of<br> witnesses<br> heard</th>
                        <th class="head0">No. of<br> witnesses <br>remaining</th>
						<th class="head0">Days<br> Outstanding</th>
						<th class="head0">Police <br>Station</th>
						<th class="head0">Custody</th>
                        <th class="head0">Result<br>/Status</th>


                    </tr>
                    </thead>
                    <tbody>
                  

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
           <td></td>
           <td></td>
            <td></td>
           <td></td>
            <td></td>
           <td></td>

                    
                    </tbody>
                </table>
<?php endif;?>
<?php include"footer.php";?>
