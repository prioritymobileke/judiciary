<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
header("Location:index.php");
}
?>

<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">
    
    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.php"><img src="images/logo1.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

    <?php include"left_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>All Cases</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">

              <h4 class="widgettitle">Case Details Table|<a href="reports/scripts/new_cases.php" style="color:#fff">  Export <img src="images/images/excel.png" align="center" width="20"></a></h4>
				
                <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0"></th>
                        <th class="head0">CaseNo</th>
                        <th class="head0">ObNo</th>
                        
                        <th class="head0">Station</th>
                        <th class="head0">Accused</th>
                        <th class="head0">Offence</th>
                        

                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");


                    $query="select ob.OffenceType,ob.CourtDate,parties.ObNo,ob.Station,GROUP_CONCAT(parties.PName SEPARATOR ' , ' ) as PName,cases.CaseNo from parties inner join ob on ob.ObNo=parties.ObNo inner join cases on cases.MagistrateName='".$_SESSION['names']."'  
                     where parties.ObNo =cases.ObNo and parties.Role='Accused' and cases.CaseNo not in (select CaseNo from casehistory) and ob.CourtDate is not null and ob.CourtDate!='' and parties.PName !='' and ob.IncidentType <> 'Traffic' group by CaseNo";

                    $result=mysql_query($query);
                    
                   /* $from_ob=mysql_query("select ObNo,OffenceType,CourtDate,Station from ob " );
                    $row1=mysql_fetch_array($from_ob);
                    
                    $from_parties=mysql_query("select ObNo,GROUP_CONCAT(parties.PName SEPARATOR ' , ' ) as PName from parties where ObNo=' ".$row1['ObNo']." ' ");
                    $row2=mysql_fetch_array($from_parties);*/
                    
                    //$from_cases=mysql_query("select CaseNo,ObNo from cases where MagistrateName=' ".$_SESSION['names']." ' group by CaseNo ");

                    while($row3=mysql_fetch_array($result))
                    {
                        $caseno=$row3['CaseNo'];
                        $obno=$row3['ObNo'];                        
                        $station=$row3['Station'];
                        $name=$row3['PName'];
                        $offence=$row3['OffenceType'];
                        
                        $my_accused=mysql_query("select PName from parties where Role='accused' and ObNo='$obno'");
                       
                       while($myrow=mysql_fetch_array($my_accused))
                       {
                       $accused=$myrow['PName'];
                       }
                        
                        $my_complainant=mysql_query("select PName from parties where Role='complainant' and ObNo='$obno'");
                        if(mysql_num_rows($my_complainant)==0)
                       {
                       $complainant='unknown';
                       }
                       
                       while($myrow1=mysql_fetch_array($my_complainant))
                       { 
                       $complainant=$myrow1['PName'];                       
                       }
                       
                       $parties=$complainant ." ". "vs"." " .$accused;


                        ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            <td><?php
                                echo '<a href="newCases.php?obno='.$obno.'">Proceed with case</a>';

                                ?></td>

                            <td><?php echo $caseno?></td>
                            <td><?php echo $obno ?></td>                            
                            <td><?php echo $station?></td>
                            <td><?php echo $parties?></td>
                            <td><?php echo $offence?></td>
                          

                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
