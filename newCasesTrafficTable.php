
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.html"><img src="images/logo.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="images/photos/thumb1.png" alt="" />
                        <div class="userinfo">
                            <h5>Betty Riungi </h5>
                            <ul>
                                <li><a href="editprofile.html">Edit Profile</a></li>
                                <li><a href="">Account Settings</a></li>
                                <li><a href="index.html">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>


               <li class=""><a href="dashboard.html"><i class="iconfa-home"></i></span> HOME</a></li>
<li class=""><a href="newCasesTable.php"><span class="iconfa-th-list"></span> New cases </a></li>
<li class="active"><a href="newCasesTrafficTable.php"><span class="iconfa-th-list"></span> New Traffic cases </a></li>
                
                <li class=""><a href="ongoingCasesTable.php"><span class="iconfa-th-list"></span> Ongoing cases </a></li>
                <li class=""><a href="closedCasesTable.php"><span class="iconfa-th-list"></span> Terminated cases </a></li>
                <li class=""><a href="chargedSexOffenders.php"><span class="iconfa-th-list"></span> Charged sex offenders</a></li>


            </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>All Cases</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">

                <h4 class="widgettitle">New Cases Table</h4>
                <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0"></th>
                        <th class="head0">ObNo</th>
                        <th class="head0">Accused</th>
                        <th class="head0">Offence </th>
                        <th class="head0">Station</th>
                        



                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");


                    $query="select ob.ObNo,ob.OffenceType,ob.IncidentType,parties.ObNo,parties.PName,ob.CourtDate,ob.IncidentType,ob.Station from ob inner join parties on parties.ObNo=ob.ObNo where parties.ObNo  not in (select ObNo from cases) AND ob.IncidentType='Traffic' and parties.Role='Accused' and ob.CourtDate is not null and ob.CourtDate!=''   ";

                    $result=mysql_query($query);


                    while($row=mysql_fetch_array($result))
                    {
                        $obno=$row['ObNo'];
                        $name=$row['PName'];
                        $station=$row['Station'];
                        $offence=$row['OffenceType'];
                        

                        ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            <td><?php
                                echo '<a href="newCasesTraffic.php?obno='.$obno.'">Proceed with case</a>';

                                ?></td>

                            <td><?php echo $obno ?></td>
                            <td><?php echo $name ?></td>
                            <td><?php echo $offence ?></td>
                            <td><?php echo $station ?></td>
                                                       


                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
