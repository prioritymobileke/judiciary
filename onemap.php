<?php include"header.php";?>
    
    <div class="leftpanel">
        
    <?php include"left_menu.php";?>
        
    </div><!-- leftpanel -->
    
    <div class="rightpanel">
        
        <ul class="breadcrumbs">
            <li><a href="dashboard.php"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
            <li>Maps</li>

        </ul>
        
      
        
        <div class="maincontent">
            <div class="maincontentinner">
                <div class="row-fluid">
                    <div id="dashboard-left" class="span12">
	                 
            <div class="">
              <!-- Bar chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                
                 
                </div>
                <div class="box-body">
                 <div class="box-body" id="map" style="height:700px;">

                </div><!-- /.box-body -->
                  
                
                
                
                  
                </div><!-- /.box-body-->
              </div><!-- /.box -->
             </div><!-- /.col -->
			 
			 
            </div><!-- /.col -->           

                        
                    </div><!--span8-->                   

                </div><!--row-fluid-->
	<?php			
	$lat=$_REQUEST['lat'];
	$lng=$_REQUEST['lng'];
	$location=$_REQUEST['loc'];
  ?>	
	<script>

// This example displays a marker at the center of Australia.

// When the user clicks the marker, an info window opens.

// The maximum width of the info window is set to 200 pixels.


function initMap() {
	
	var oyugis = {
		info: '<strong>Oyugis Law Courts<strong>',
		lat: 0.5079,
		long: 34.7382
	};

	var ndhiwa = {
		info: '<strong>Ndhiwa Law Courts</strong>',
		lat: 0.7299,
		long: 34.3671
	};

	var homabay = {
		info: '<strong>Homa Bay Law Courts</strong>',
		lat: 0.5350,
		long:34.4531
	};
	
	var mbita = {
		info: '<strong>Mbita Law Courts</strong>',
		lat: 0.4368,
		long: 34.2060
	};

	var locations = [
      [oyugis.info, oyugis.lat, oyugis.long, 0],
      [ndhiwa.info, ndhiwa.lat, ndhiwa.long, 1],
      [homabay.info, homabay.lat, homabay.long, 2],
	  [mbita.info, mbita.lat, mbita.long, 3],
    ];

	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 11,
		center: new google.maps.LatLng(0.5350,34.4531),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	var infowindow = new google.maps.InfoWindow({});

	var marker, i;

	for (i = 0; i < locations.length; i++) {
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map
		});

		google.maps.event.addListener(marker, 'click', (function (marker, i) {
			return function () {
				infowindow.setContent(locations[i][0]);
				infowindow.open(map, marker);
			}
		})(marker, i));
	}
}


    </script>

    <script async defer

        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVDefCDk-hF532EgTtL2QLE_Nzn9gXJ7k&signed_in=true&callback=initMap"></script>
<?php include"footer.php";?>