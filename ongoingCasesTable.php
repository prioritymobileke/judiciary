<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.html"><img src="images/logo.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="images/photos/thumb1.png" alt="" />
                        <div class="userinfo">
                            <h5>Betty Riungi </h5>
                            <ul>
                                <li><a href="editprofile.html">Edit Profile</a></li>
                                <li><a href="">Account Settings</a></li>
                                <li><a href="index.html">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>


                <li class=""><a href="dashboard.html"><i class="iconfa-home"></i></span> HOME</a></li>
                <li class=""><a href="newCasesTable.php"><span class="iconfa-th-list"></span> New cases </a></li>
                <li class=""><a href="newCasesTrafficTable.php"><span class="iconfa-th-list"></span> New Traffic cases </a></li>

                <li class="active"><a href="ongoingCasesTable.php"><span class="iconfa-th-list"></span> Ongoing cases </a></li>
                <li class=""><a href="closedCasesTable.php"><span class="iconfa-th-list"></span> Terminated cases </a></li>
                <li class=""><a href="chargedSexOffenders.php"><span class="iconfa-th-list"></span> Charged sex offenders</a></li>
                <li class=""><a href="remandees.php"><span class="iconfa-th-list"></span> Remandees</a></li>


            </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>All Cases</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">

                <h4 class="widgettitle">Case Details Table</h4>
                <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        
                        

                       	<th class="head1"></th>
                       	<th class="head1"></th>
                        <th class="head1">Case No</th>
                        <th class="head0">ObNo</th>
                        <th class="head0">Party name</th>
                        <th class="head0">Court </th>
                        <th class="head0">Magistrate </th>
                        <th class="head0">Case Status</th>


                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");


                    $query="select GROUP_CONCAT(parties.PName SEPARATOR ' , ') AS PName,cases.CaseNo,cases.ObNo,cases.MagistrateName,cases.CaseStatus,ob.CourtName from cases 
	inner join ob on ob.ObNo=cases.ObNo inner join parties on parties.ObNo=cases.ObNo 
	
	where cases.CaseStatus!='Sentenced' and parties.Role='Accused' GROUP BY cases.CaseNo" ;

                    $result=mysql_query($query);


                    while($row=mysql_fetch_array($result))
                    {

                        $caseno=$row['CaseNo'];
                        $partyname=$row['PName'];
                        $obno=$row['ObNo'];
                        $courtid=$row['CourtName'];
                        $magid=$row['MagistrateName'];
                        $casestatus=$row['CaseStatus'];

                        ?>
                        <?php

                        if($row['CaseStatus']=='mention')
                        {

                        ?>


                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            <td><?php
                                echo '<a href="caseprogress.php?caseno='.$caseno.'">Hearing</a>';
                                echo"</br>";

                                ?></td>


                            <td><?php echo $caseno ?></td>
                            <td><?php echo $obno ?></td>
                            <td><?php echo $partyname?></td>
                            <td><?php echo $courtid ?></td>
                            <td><?php echo $magid ?></td>
                            <td><?php echo $casestatus ?></td>



                        </tr>
                        <?php }
                        else if($row['CaseStatus']=='Hearing'){
                        ?>



                    <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            <td><?php
                                echo '<a href="caseprogress.php?caseno='.$caseno.'">Hearing</a>';
                                echo"</br>";
                                echo '<a href="terminateCase.php?caseno='.$caseno.'">Sentencing</a>';
                                echo"</br>";
                                //echo '<a href="sentenceCase.php?caseno='.$caseno.'">Terminate</a>';

                                ?></td>


                        <td><?php echo $caseno ?></td>
                        <td><?php echo $obno ?></td>
                        <td><?php echo $partyname?></td>
                        <td><?php echo $courtid ?></td>
                        <td><?php echo $magid ?></td>
                        <td><?php echo $casestatus ?></td>



                        </tr>
                    <?php }
                        else if($row['CaseStatus']=='Sentencing')
                        {

                        ?>
                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            <td><?php

                                
                                echo '<a href="sentenceCase.php?caseno='.$caseno.'">Terminate</a>';
                                echo"</br>";

                                ?></td>


                            <td><?php echo $caseno ?></td>
                            <td><?php echo $obno ?></td>
                            <td><?php echo $partyname?></td>
                            <td><?php echo $courtid ?></td>
                            <td><?php echo $magid ?></td>
                            <td><?php echo $casestatus ?></td>



                        </tr>
                    <?php }

                    }

                    ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
