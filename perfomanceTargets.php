<?php include"header.php";?>
    <div class="leftpanel">

         <?php include"left_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">



     

        <div class="maincontent">
            <div class="maincontentinner col-md-8">

                
                
           <table  class="table table-bordered responsive" >


                    <colgroup>
                        <col class="con0 col-md-4" />
                        <col class="con1 col-md-4" />
                        <col class="con0 col-md-4" />
                        <col class="con1 col-md-4" />
                        <col class="con0 col-md-4" />
                        <col class="con1 col-md-4" />
						<col class="con0 col-md-4" />                        
                        <col class="con1 col-md-4" />
                        <col class="con0 " />
						 <col class="con1" />
                        <col class="con0 " />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0 ">Matter</th>
                        <th class="head0 ">Recommended Time Frame</th>
						<th class="head0 ">No of cases</th>
                        <th class="head0 ">Cases within recommended <br>time frame</th>                        
                        <th class="head0 ">Percentage score</th>
                                              

                    </tr>
                    </thead>
                    <tbody>
                 

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						<td>Certified Urgent Applications</td>             
						<td>Within 30 days from the date of filing</td> 
						<td></td>			
						<td></td>                
						<td></td> 
						</tr>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						<td>All applications</td>             
						<td>Within 180 daysfrom the <br>date of filing</td> 
						<td></td>			
						<td></td>                
						<td></td> 
						</tr>	

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						<td>Injunction applications</td>             
						<td>Hearing within 60 days from the <br>date of filing<br>

Determination within 30 days <br>from the date of hearing</td> 
						<td></td>			
						<td></td>                
						<td></td> 
						</tr>	

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						<td>Hearing and determination of<br> civil and criminal matters</td>             
						<td>Determination within 360 daysfrom the <br>date of filing</td> 
						<td></td>			
						<td></td>                
						<td></td> 
						</tr>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						<td>Election petitions</td>             
						<td>	
180 days from the date of filing</td> 
						<td></td>			
						<td></td>                
						<td></td> 
						</tr>	
                         
                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						<td>Delivery of Judgments and rulings</td>             
						<td>Within 60 days from the date of<br> finalisation of the hearing</td> 
						<td></td>			
						<td></td>                
						<td></td> 
						</tr>	

                         <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						<td>Criminal Reviews and Judicial<br>< Review matters</td>             
						<td>Determination within 90 days <br>from the date of filing</td> 
						<td></td>			
						<td></td>                
						<td></td> 
						</tr>

 <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
						  
						<td>Time for dissemination of all decisions to all<br> subordinate courts and to all Judicial Officers</td>             
						<td>Within 7 days after delivery<br> of final decision.</td> 
						<td></td>			
						<td></td>                
						<td></td> 
						</tr>

						
                    </tbody>
                </table>

<?php include"footer.php";?>