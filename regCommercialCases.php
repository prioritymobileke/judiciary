<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))
{
header("Location:index.php");
}
include"functions.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
	
		
	jQuery("#int_parties").click(function(e){
      jQuery('#int_parties').attr('disabled',true);
	  jQuery('#int_parties').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#id_number").val(),
			name_of_party:jQuery("#name_of_party").val(),
			dob:jQuery("#int_dob").val(),
			gender:jQuery("#int_gender").val(),
			phone_number:jQuery("#phone_number").val(),
			postal_address:jQuery("#postal_address").val(),
			party:"yes"
		},
		  function(data,status){
			
			jQuery("#id_number").val("")
			jQuery("#name_of_party").val("")
			jQuery("#phone_number").val("")
			jQuery("#postal_address").val("")
			jQuery("#int_dob").val("")
			jQuery("#int_gender").val("")
			jQuery('#int_parties').attr('disabled',false);
			jQuery('#int_parties').val('SUBMITED');
	        jQuery('#int_parties').val('Click to add');
			
			
			
		  });
		
	});	
			
	jQuery("#add_respondent").click(function(e){
      jQuery('#add_respondent').attr('disabled',true);
	  jQuery('#add_respondent').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#resp_id").val(),
			name_of_party:jQuery("#resp_name").val(),
			dob:jQuery("#resp_dob").val(),
			gender:jQuery("#resp_gender").val(),
			phone_number:jQuery("#resp_phone").val(),
			postal_address:jQuery("#resp_address").val(),
			crime_resp:"yes"
		},
		  function(data,status){
			
			jQuery("#resp_id").val("")
			jQuery("#resp_name").val(""),
			jQuery("#resp_phone").val("")
			jQuery("#resp_dob").val("")
			jQuery("#resp_gender").val("")
			jQuery("#resp_address").val("")
			jQuery('#add_respondent').attr('disabled',false);
			jQuery('#add_respondent').val('SUBMITED');
	        jQuery('#add_respondent').val('Click to add');
			
			
			
		  });
		
	});			


	
	
	jQuery("#add_applicant").click(function(e){
      jQuery('#add_applicant').attr('disabled',true);
	  jQuery('#add_applicant').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#applicant_id").val(),
			dob:jQuery("#applicant_dob").val(),
			gender:jQuery("#applicant_gender").val(),
			name_of_party:jQuery("#applicant_name").val(),
			phone_number:jQuery("#applicant_phone").val(),
			postal_address:jQuery("#applicant_address").val(),
			crime_applicant:"yes"
		},
		  function(data,status){
			
			jQuery("#applicant_id").val("")
			jQuery("#applicant_name").val(""),
			jQuery("#applicant_phone").val("")
			jQuery("#applicant_dob").val("")
			jQuery("#applicant_gender").val("")
			jQuery("#applicant_address").val("")
			jQuery('#add_applicant').attr('disabled',false);
			jQuery('#add_applicant').val('SUBMITED');
	        jQuery('#add_applicant').val('Click to add');
			
			
			
		  });
		
	});
		
	
	jQuery("#add_plaintiff").click(function(e){
      jQuery('#add_plaintiff').attr('disabled',true);
	  jQuery('#add_plaintiff').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#plaintiff_id").val(),
			dob:jQuery("#plaintiff_dob").val(),
			gender:jQuery("#plaintiff_gender").val(),
			name_of_party:jQuery("#plaintiff_name").val(),
			phone_number:jQuery("#plaintiff_phone").val(),
			postal_address:jQuery("#plaintiff_address").val(),
			plaintiff:"yes"
		},
		  function(data,status){
			
			jQuery("#plaintiff_id").val("")
			jQuery("#plaintiff_name").val(""),
			jQuery("#plaintiff_phone").val("")
			jQuery("#plaintiff_dob").val("")
			jQuery("#plaintiff_gender").val("")
			jQuery("#plaintiff_address").val("")
			jQuery('#add_plaintiff').attr('disabled',false);
			jQuery('#add_plaintiff').val('SUBMITED');
	        jQuery('#add_plaintiff').val('Click to add');
			
			
			
		  });
		
	});		

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.php"><img src="images/logo1.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

         <?php include"reg_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>Commercial Cases</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">
        <h4 class="widgettitle" align="center"><?php echo $_REQUEST['caseType']; ?></h4>
		
		
    <form action="regCivilCases.php" method="post">
	<input  type="hidden" name="caseNo" id="caseNo" value="<?php echo case_no();?>" />
	<input type="hidden" name="caseType" value="<?php echo $_REQUEST['caseType'];?>" />
	
	<?php if($_REQUEST['caseType']=="Income Tax Appeal"):?>
	<div style="width:320px;margin:10px; float:left;">
	
	<fieldset><legend>Applicant </legend>
	<p class="form_input">Name of Applicant<br><input  type="text" id="applicant_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="applicant_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="applicant_dob" /></p>
	<p class="form_input">Gender<br><select id="applicant_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="applicant_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="applicant_address" /></p>
	<p class="form_input">Add all the applicants before submitting the form</p>
	<p class="form_input"><input  type="button" id="add_applicant" value="Click to add" /></p>	
	</fieldset>
	</div>
	<?php else:?>
	
	<div style="width:320px;margin:10px; float:left;">
	<input type="hidden" name="caseType" value="<?php echo $_REQUEST['caseType'];?>" />
	<fieldset><legend>Plaintiff Information</legend>
	<p class="form_input">Name of plaintiff<br><input  type="text" id="plaintiff_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="plaintiff_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="plaintiff_dob" /></p>
	<p class="form_input">Gender<br><select id="plaintiff_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="plaintiff_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="plaintiff_address" /></p>
	<p class="form_input">Add all the plaintiffs before submitting the form</p>
	<p class="form_input"><input  type="button" id="add_plaintiff" value="Click to add" /></p>	
	</fieldset>
	</div>
	<?php endif;?>
	<div style="width:320px;margin:10px;float:left;">
	<fieldset><legend>Respondent</legend>	
	<p class="form_input">Name of Respondent<br><input  type="text" id="resp_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="resp_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="resp_dob" /></p>
	<p class="form_input">Gender<br><select id="resp_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="resp_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="resp_address" /></p>
	<p class="form_input">Add all the respondents before submitting the form</p>
	<p class="form_input"><input  type="button" id="add_respondent" value="Click to add" /></p>
	</fieldset>
	</div>
	<div style="width:320px;margin:10px;float:left;">
	<fieldset ><legend>Interested Parties</legend>		
	<p class="form_input">Name<br><input  type="text" id="name_of_party" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="id_number" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="int_dob" /></p>
	<p class="form_input">Gender<br><select id="int_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="phone_number" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="postal_address" /></p>
	<p class="form_input">Add all the parties involved before submitting the form</p>
	<p class="form_input"><input  type="button" id="int_parties" value="Click to add" /></p>
	
	</fieldset>
	</div>
	
	
	
	<?php if($_REQUEST['caseType']=="Judicial Review"):?>
	<div style="width:320px;margin:10px;float:left;">
	<fieldset><legend> Narrative  </legend>
	<p class="form_input">Narrative<br><textarea   name="case_particulars"></textarea></p>	
	</fieldset>
	</div>
	<?php endif;?>
	
	<div style="width:320px;margin:10px;float:left;">
	<fieldset><legend>   </legend>		
	<p class="form_input">Case Code<br><select name="casecode"  id="casecode" />
	<?php
	$result=mysql_query("select * from casecodes");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['codename']."'>".$row['codename']."</option>";
	}	
	?>
	</select>
	</p>
	<p class="form_input">Case is coming for <br><select name="casestate"  id="casestate" />
	<?php
	$result=mysql_query("select * from case_states");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['state_name']."'>".$row['state_name']."</option>";
	}	
	?>
	</select>
	</p>
  <?php if($_REQUEST['caseType']=="Income Tax Appeal"):?>
	<p class="form_input">Case is coming from <br><select name="formercourt"  id="formercourt" />
	<?php
	$result=mysql_query("SELECT *
    FROM highcourts");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['courtname']."'>".$row['courtname']."</option>";
	}
    $result=mysql_query("SELECT *
    FROM magistratecourts");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['courtname']."'>".$row['courtname']."</option>";
	}	
	?>
	</select>
	</p>
	<p class="form_input">Former Case Number<br>
	<input  type="text" name="FormerCaseNumber" id="FormerCaseNumber" /></p>
	<?php endif; ?>
	<p class="form_input"><input  type="submit" name="register_case" value="SUBMIT" /></p>
	</fieldset>
	</div>
	
	
	</form>
                
     <div style="width:320px;margin:10px;float:left;">
	<?php
	if(isset($_POST['register_case'])){
		
	   if($_POST['caseType']!="Judicial Review"):	
		
		$caseNo=mysql_real_escape_string($_POST['caseNo']);
        $casecode=mysql_real_escape_string($_POST['casecode']); 
        $CaseState=mysql_real_escape_string($_POST['CaseState']);
        $formercourt=mysql_real_escape_string($_POST['formercourt']);
		$FormerCaseNumber=mysql_real_escape_string($_POST['FormerCaseNumber']);		
		$caseType=mysql_real_escape_string($_POST['caseType']);
         if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`xcourt`,`Division`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','".$formercourt."-".$FormerCaseNumber."','Civil','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}	
			
		else:
		
			$caseNo=mysql_real_escape_string($_POST['caseNo']);
			$casecode=mysql_real_escape_string($_POST['casecode']);
			$CaseState=mysql_real_escape_string($_POST['CaseState']);
			$formercourt=mysql_real_escape_string($_POST['formercourt']);
		    $FormerCaseNumber=mysql_real_escape_string($_POST['FormerCaseNumber']);
			$caseDetails=mysql_real_escape_string($_POST['case_particulars']);
			$caseType=mysql_real_escape_string($_POST['caseType']);
			
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`xcourt`,`Division`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','".$formercourt."-".$FormerCaseNumber."','Civil','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
		
		
	   endif;
		
	?>
	<script>document.location="regCivilCases.php?caseType=<?php echo $_REQUEST['caseType'];?>";</script>
  <?php		
	}
	?>
	</div>                     

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
