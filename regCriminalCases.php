<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))
{
header("Location:index.php");
}
include"functions.php";
$court="Homa Bay";
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
	
    
    <link rel="stylesheet" href="css/responsive-tables.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-responsive.min.css">


    <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
	 <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/forms.css" type="text/css">
   	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
	 <script src="js/app.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/custom.js"></script>
	
	
	<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
	<script>
	webshims.setOptions('forms-ext', {types: 'date'});
	webshims.polyfill('forms forms-ext');
	$.webshims.formcfg = {
	en: {
		dFormat: '-',
		dateSigns: '-',
		patterns: {
			d: "yy-mm-dd"
		}
	}
	};
	</script>
	
    <script type="text/javascript">
        jQuery(document).ready(function(){
			
			jQuery('.numbersOnly').keyup(function () {
			if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
			   this.value = this.value.replace(/[^0-9\.]/g, '');
			}
		      });
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
			
		
	  jQuery("#add_doctor").click(function(e){
      jQuery('#add_doctor').attr('disabled',true);
	  jQuery('#add_doctor').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {	
            caseNo:jQuery("#set_case_No").val(),		  
			id_number:jQuery("#doctor_id").val(),		
			email:jQuery("#doctor_email").val(),
			name_of_party:jQuery("#doctor_name").val(),
			phone_number:jQuery("#doctor_phone").val(),
			postal_address:jQuery("#doctor_address").val(),
			doctor:"yes"
		},
		  function(data,status){
			
			jQuery("#doctor_id").val("")
			jQuery("#doctor_name").val(""),
			jQuery("#doctor_phone").val("")
			jQuery("#specialization").val("")
			jQuery("#doctor_email").val("")
			jQuery("#doctor_address").val("")
			jQuery('#add_doctor').attr('disabled',false);
			jQuery('#add_doctor').val('SUBMITED');
	        jQuery('#add_doctor').val('SUBMIT');
			
			
			
		  });
		
	});	
	
	
	
		jQuery("#add_investigator").click(function(e){
      jQuery('#add_investigator').attr('disabled',true);
	  jQuery('#add_investigator').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {	
            caseNo:jQuery("#set_case_No").val(),		  
			id_number:jQuery("#investigator_id").val(),
			station:jQuery("#station").val(),
			email:jQuery("#investigator_email").val(),
			name_of_party:jQuery("#investigator_name").val(),
			phone_number:jQuery("#investigator_phone").val(),
			postal_address:jQuery("#investigator_address").val(),
			investigator:"yes"
		},
		  function(data,status){
			
			jQuery("#investigator_id").val("")
			jQuery("#investigator_name").val("")
			jQuery("#investigator_phone").val("")
			jQuery("#specialization").val("")
			jQuery("#investigator_email").val("")
			jQuery("#investigator_address").val("")
			jQuery('#add_investigator').attr('disabled',false);
			jQuery('#add_investigator').val('SUBMITED');
	        jQuery('#add_investigator').val('SUBMIT');
			
			
			
		  });
		
	});

jQuery("#int_parties").click(function(e){
      jQuery('#int_parties').attr('disabled',true);
	  jQuery('#int_parties').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#id_number").val(),
			name_of_party:jQuery("#name_of_party").val(),
			dob:jQuery("#int_dob").val(),
			lawyer_id:jQuery("#int_lawyer_id").val(),
			gender:jQuery("#int_gender").val(),
			phone_number:jQuery("#phone_number").val(),
			postal_address:jQuery("#postal_address").val(),
			party:"yes"
		},
		  function(data,status){
			
			jQuery("#id_number").val("")
			jQuery("#name_of_party").val("")
			jQuery("#phone_number").val("")
			jQuery("#postal_address").val("")
			jQuery("#int_dob").val("")
			jQuery("#int_gender").val("")
			jQuery('#int_parties').attr('disabled',false);
			jQuery('#int_parties').val('SUBMITED');
	        jQuery('#int_parties').val('Click to add');
			
			
			
		  });
		
	});	
			


	jQuery("#add_respondent").click(function(e){
      jQuery('#add_respondent').attr('disabled',true);
	  jQuery('#add_respondent').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#resp_id").val(),
			name_of_party:jQuery("#resp_name").val(),
			dob:jQuery("#resp_dob").val(),
			lawyer_id:jQuery("#resp_lawyer_id").val(),
			gender:jQuery("#resp_gender").val(),
			phone_number:jQuery("#resp_phone").val(),
			postal_address:jQuery("#resp_address").val(),
			crime_resp:"yes"
		},
		  function(data,status){
			
			jQuery("#resp_id").val("")
			jQuery("#resp_name").val(""),
			jQuery("#resp_phone").val("")
			jQuery("#resp_dob").val("")
			jQuery("#resp_gender").val("")
			jQuery("#resp_address").val("")
			jQuery('#add_respondent').attr('disabled',false);
			jQuery('#add_respondent').val('SUBMITED');
	        jQuery('#add_respondent').val('Click to add');
			
			
			
		  });
		
	});		

	
	jQuery("#add_accused").click(function(e){
		
      jQuery('#add_accused').attr('disabled',true);
	  jQuery('#add_accused').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#accused_id").val(),
			name_of_party:jQuery("#accused_name").val(),
			dob:jQuery("#accused_dob").val(),
			gender:jQuery("#accused_gender").val(),
			lawyer_id:jQuery("#acc_lawyer_id").val(),
			phone_number:jQuery("#accused_phone").val(),
			postal_address:jQuery("#accused_address").val(),
			accu_custody:jQuery("#accu_custody").val(),			
			crime_accused:"yes"
		},
		  function(data,status){
			
			jQuery("#accused_id").val("")
			jQuery("#accused_name").val(""),
			jQuery("#accused_phone").val("")
			jQuery("#accused_dob").val("")
			jQuery("#accused_gender").val("")
			jQuery("#accused_address").val("")
			jQuery('#add_accused').attr('disabled',false);
			jQuery('#add_accused').val('SUBMITED');
	        jQuery('#add_accused').val('Click to add');
			
			
			
		  });
		
	});	
		
	
	jQuery("#add_applicant").click(function(e){
      jQuery('#add_applicant').attr('disabled',true);
	  jQuery('#add_applicant').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#applicant_id").val(),
			dob:jQuery("#applicant_dob").val(),
			gender:jQuery("#applicant_gender").val(),
			lawyer_id:jQuery("#app_lawyer_id").val(),
			name_of_party:jQuery("#applicant_name").val(),
			phone_number:jQuery("#applicant_phone").val(),
			postal_address:jQuery("#applicant_address").val(),
			crime_applicant:"yes"
		},
		  function(data,status){
			
			jQuery("#applicant_id").val("")
			jQuery("#applicant_name").val(""),
			jQuery("#applicant_phone").val("")
			jQuery("#applicant_dob").val("")
			jQuery("#applicant_gender").val("")
			jQuery("#applicant_address").val("")
			jQuery('#add_applicant').attr('disabled',false);
			jQuery('#add_applicant').val('SUBMITED');
	        jQuery('#add_applicant').val('Click to add');
			
			
			
		  });
		
	});	


	jQuery("#add_witness").click(function(e){
      jQuery('#add_witness').attr('disabled',true);
	  jQuery('#add_witness').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#witness_id").val(),
			dob:jQuery("#witness_dob").val(),
			gender:jQuery("#witness_gender").val(),			
			name_of_party:jQuery("#witness_name").val(),
			phone_number:jQuery("#witness_phone").val(),
			postal_address:jQuery("#witness_address").val(),
			crime_witness:"yes"
		},
		  function(data,status){
			
			jQuery("#witness_id").val("")
			jQuery("#witness_name").val(""),
			jQuery("#witness_phone").val("")
			jQuery("#witness_dob").val("")
			jQuery("#witness_gender").val("")
			jQuery("#witness_address").val("")
			jQuery('#add_witness').attr('disabled',false);
			jQuery('#add_witness').val('SUBMITED');
	        jQuery('#add_witness').val('Click to add');
			
			
			
		  });
		
	});	


jQuery('#set_case_No').keyup(function () {
     
	
	jQuery.post("JProcessor.php",
		  {
		  
			set_case_No:jQuery("#set_case_No").val()		
		},
		  function(data,status){     
			
			
			
		  });
		
	});	
        });
		
		function reset_form_element (e) {
			e.wrap('<form>').parent('form').trigger('reset');
			e.unwrap();
		}
		
	function upload_file(){
		
		if(jQuery("#filedate").val()==""){
			
			alert("Date of file must be entered first");			
            reset_form_element( $('#attach') );
			$('#attach').preventDefault();
				
			}else{
    
	jQuery('#uploaded').val('Attaching.......');
	var attach = document.getElementById("attach");
	var caseNo = jQuery("#set_case_No").val();
    var filedate = jQuery("#filedate").val();	
    var file = attach.files[0];
	
    formData = new FormData();
	
    formData.append('attach', file);
	formData.append('caseNo', caseNo);
    formData.append('filedate', filedate);	
		//var formData = new FormData($('#attach')[0]);		
  jQuery.ajax({
    url: 'upload_letters.php',
    type: 'POST',
    data: formData,
	enctype: 'multipart/form-data',
    async: true,
    cache: false,
    contentType: false,
    processData: false,
	
    success: function (returndata) {
		
		jQuery('#uploaded').val("Attached");
		jQuery('#att').html(returndata);
	}
  });
}
}	
		
		
		
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">
         <?php include"reg_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">       

        <div class="maincontent">
            <div class="maincontentinner">
			
			<div style="height:100%; padding-top:20%;padding-bottom:20%; width:600px; margin:auto;">
			
			<div class="form-group form_input col-md-3">
			<a href="regnewCriminalCases.php?caseType=<?php echo $_REQUEST['caseType'];?>"><input  type="button" value="NEW CASES" /></a>
			</div>
			
			<div class="form-group form_input col-md-3">
			<a href="regoldCriminalCases.php?caseType=<?php echo $_REQUEST['caseType'];?>"><input  type="button" value="OLD CASES" /></a>
			</div>				
            </div>
                <div class="footer">
	
			  
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
