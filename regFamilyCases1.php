<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))
{
header("Location:index.php");
}
include"functions.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
	
    <link rel="stylesheet" href="css/responsive-tables.css">

    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
			
    	
	jQuery("#int_parties").click(function(e){
      jQuery('#int_parties').attr('disabled',true);
	  jQuery('#int_parties').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#id_number").val(),
			name_of_party:jQuery("#name_of_party").val(),
			dob:jQuery("#int_dob").val(),
			gender:jQuery("#int_gender").val(),
			phone_number:jQuery("#phone_number").val(),
			postal_address:jQuery("#postal_address").val(),
			party:"yes"
		},
		  function(data,status){
			
			jQuery("#id_number").val("")
			jQuery("#name_of_party").val("")
			jQuery("#phone_number").val("")
			jQuery("#postal_address").val("")
			jQuery("#int_dob").val("")
			jQuery("#int_gender").val("")
			jQuery('#int_parties').attr('disabled',false);
			jQuery('#int_parties').val('SUBMITED');
	        jQuery('#int_parties').val('Click to add');
			
			
			
		  });
		
	});	
			
	jQuery("#add_respondent").click(function(e){
      jQuery('#add_respondent').attr('disabled',true);
	  jQuery('#add_respondent').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#resp_id").val(),
			name_of_party:jQuery("#resp_name").val(),
			dob:jQuery("#resp_dob").val(),
			gender:jQuery("#resp_gender").val(),
			phone_number:jQuery("#resp_phone").val(),
			postal_address:jQuery("#resp_address").val(),
			crime_resp:"yes"
		},
		  function(data,status){
			
			jQuery("#resp_id").val("")
			jQuery("#resp_name").val(""),
			jQuery("#resp_phone").val("")
			jQuery("#resp_dob").val("")
			jQuery("#resp_gender").val("")
			jQuery("#resp_address").val("")
			jQuery('#add_respondent').attr('disabled',false);
			jQuery('#add_respondent').val('SUBMITED');
	        jQuery('#add_respondent').val('Click to add');
			
			
			
		  });
		
	});			


	
	
	jQuery("#add_applicant").click(function(e){
      jQuery('#add_applicant').attr('disabled',true);
	  jQuery('#add_applicant').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#applicant_id").val(),
			dob:jQuery("#applicant_dob").val(),
			gender:jQuery("#applicant_gender").val(),
			name_of_party:jQuery("#applicant_name").val(),
			phone_number:jQuery("#applicant_phone").val(),
			postal_address:jQuery("#applicant_address").val(),
			crime_applicant:"yes"
		},
		  function(data,status){
			
			jQuery("#applicant_id").val("")
			jQuery("#applicant_name").val(""),
			jQuery("#applicant_phone").val("")
			jQuery("#applicant_dob").val("")
			jQuery("#applicant_gender").val("")
			jQuery("#applicant_address").val("")
			jQuery('#add_applicant').attr('disabled',false);
			jQuery('#add_applicant').val('SUBMITED');
	        jQuery('#add_applicant').val('Click to add');
			
			
			
		  });
		
	});
		
	
	jQuery("#add_plaintiff").click(function(e){
      jQuery('#add_plaintiff').attr('disabled',true);
	  jQuery('#add_plaintiff').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#plaintiff_id").val(),
			dob:jQuery("#plaintiff_dob").val(),
			gender:jQuery("#plaintiff_gender").val(),
			name_of_party:jQuery("#plaintiff_name").val(),
			phone_number:jQuery("#plaintiff_phone").val(),
			postal_address:jQuery("#plaintiff_address").val(),
			plaintiff:"yes"
		},
		  function(data,status){
			
			jQuery("#plaintiff_id").val("")
			jQuery("#plaintiff_name").val(""),
			jQuery("#plaintiff_phone").val("")
			jQuery("#plaintiff_dob").val("")
			jQuery("#plaintiff_gender").val("")
			jQuery("#plaintiff_address").val("")
			jQuery('#add_plaintiff').attr('disabled',false);
			jQuery('#add_plaintiff').val('SUBMITED');
	        jQuery('#add_plaintiff').val('Click to add');
			
			
			
		  });
		
	});		

	jQuery("#add_petitioner").click(function(e){
      jQuery('#add_petitioner').attr('disabled',true);
	  jQuery('#add_petitioner').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#petitioner_id").val(),
			dob:jQuery("#petitioner_dob").val(),
			gender:jQuery("#petitioner_gender").val(),
			name_of_party:jQuery("#petitioner_name").val(),
			phone_number:jQuery("#petitioner_phone").val(),
			postal_address:jQuery("#petitioner_address").val(),
			family_petitioner:"yes"
		},
		  function(data,status){
			
			jQuery("#petitioner_id").val("")
			jQuery("#petitioner_name").val(""),
			jQuery("#petitioner_phone").val("")
			jQuery("#petitioner_dob").val("")
			jQuery("#petitioner_gender").val("")
			jQuery("#petitioner_address").val("")
			jQuery('#add_petitioner').attr('disabled',false);
			jQuery('#add_petitioner').val('SUBMITED');
	        jQuery('#add_petitioner').val('Click to add');
			
			
			
		  });
		
	});
	
    jQuery("#add_objector").click(function(e){
      jQuery('#add_objector').attr('disabled',true);
	  jQuery('#add_objector').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#objector_id").val(),
			dob:jQuery("#objector_dob").val(),
			gender:jQuery("#objector_gender").val(),
			name_of_party:jQuery("#objector_name").val(),
			phone_number:jQuery("#objector_phone").val(),
			postal_address:jQuery("#objector_address").val(),
			family_objector:"yes"
		},
		  function(data,status){
			
			jQuery("#objector_id").val("")
			jQuery("#objector_name").val(""),
			jQuery("#objector_phone").val("")
			jQuery("#objector_dob").val("")
			jQuery("#objector_gender").val("")
			jQuery("#objector_address").val("")
			jQuery('#add_objector').attr('disabled',false);
			jQuery('#add_objector').val('SUBMITED');
	        jQuery('#add_objector').val('Click to add');
			
			
			
		  });
		
	});
	
	jQuery("#add_protester").click(function(e){
      jQuery('#add_protester').attr('disabled',true);
	  jQuery('#add_protester').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#caseNo").val(),
			id_number:jQuery("#protester_id").val(),
			dob:jQuery("#protester_dob").val(),
			gender:jQuery("#protester_gender").val(),
			name_of_party:jQuery("#protester_name").val(),
			phone_number:jQuery("#protester_phone").val(),
			postal_address:jQuery("#protester_address").val(),
			family_protester:"yes"
		},
		  function(data,status){
			
			jQuery("#protester_id").val("")
			jQuery("#protester_name").val(""),
			jQuery("#protester_phone").val("")
			jQuery("#protestor_dob").val("")
			jQuery("#protestor_gender").val("")
			jQuery("#protester_address").val("")
			jQuery('#add_protester').attr('disabled',false);
			jQuery('#add_protester').val('SUBMITED');
	        jQuery('#add_protester').val('Click to add');
			
			
			
		  });
		
	});
	

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.php"><img src="images/logo1.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

         <?php include"reg_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>Family Cases</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">
 <h4 class="widgettitle" align="center"><?php echo $_REQUEST['caseType']; ?></h4>
 
 
    <form action="regFamilyCases.php" method="post">
	<input  type="hidden" name="caseNo" id="caseNo" value="<?php echo case_no();?>" />
	<input type="hidden" name="caseType" value="<?php echo $_REQUEST['caseType'];?>" />
	
	<?php if($_REQUEST['caseType']=="Citation"):?>
	<div style="width:320px;margin:10px; float:left;">	
	<fieldset><legend></legend>
	<p class="form_input">Citor ID Number<br><input  type="text" name="citor_id_number" /></p>
    <p class="form_input">Citee ID Number<br><input  type="text" name="citee_id_number" /></p>
		
	</fieldset>
	</div>
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="Adoption"):?>
	<div style="width:320px;margin:10px; float:left;">	
	<fieldset><legend>Applicant Details</legend>	
	<p class="form_input">Name of Applicant<br><input  type="text" id="applicant_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="applicant_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="applicant_dob" /></p>
	<p class="form_input">Gender<br><select id="applicant_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="applicant_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="applicant_address" /></p>
	<p class="form_input">Add all the applicants before submitting the form</p>
	<p class="form_input"><input  type="button" id="add_applicant" value="Click to add" /></p>	
	</fieldset>
	</div>
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="Adoption"):?>
	<div style="width:320px;margin:10px; float:left;">
	
	<fieldset><legend>Name of Child</legend>	
	<p class="form_input">Name of Child<br><input  type="text" name="name_of_child" /></p>  	
	</fieldset>
	</div>
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="PA"||$_REQUEST['caseType']=="Ad litem"):?>
	<div style="width:320px;margin:10px; float:left;">
	
	<fieldset><legend>Petitioners</legend>	
	<p class="form_input">Name of Petitioner<br><input  type="text" id="petitioner_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="petitioner_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="petitioner_dob" /></p>
	<p class="form_input">Gender<br><select id="petitioner_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="petitioner_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="petitioner_address" /></p>
	<p class="form_input">Add all the petitioners before submitting the form</p>
	<p class="form_input"><input  type="button" id="add_petitioner" value="Click to add" /></p>	
	</fieldset>
	</div>
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="PA"):?>
	<div style="width:320px;margin:10px; float:left;">
	
	<fieldset><legend>Objectors</legend>	
	<p class="form_input">Name of objector<br><input  type="text" id="objector_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="objector_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="objector_dob" /></p>
	<p class="form_input">Gender<br><select id="objector_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="objector_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="objector_address" /></p>
	<p class="form_input">Add all the objectors before submitting the form</p>
	<p class="form_input"><input  type="button" id="add_objector" value="Click to add" /></p>		
	</fieldset>
	</div>
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="PA"):?>
	<div style="width:320px;margin:10px; float:left;">
	
	<fieldset><legend>Protesters</legend>	
	<p class="form_input">Name of protester<br><input  type="text" id="protester_name" /></p>
	<p class="form_input">ID Number<br><input  type="text" id="protester_id" /></p>
	<p class="form_input">Date of Birth<br><input  type="date" id="protester_dob" /></p>
	<p class="form_input">Gender<br><select id="protestor_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</p>
	<p class="form_input">Phone Number<br><input  type="text" id="protester_phone" /></p>
	<p class="form_input">Postal Address<br><input  type="text" id="protester_address" /></p>
	<p class="form_input">Add all the protesters before submitting the form</p>
	<p class="form_input"><input  type="button" id="add_protester" value="Click to add" /></p>
	</fieldset>
	</div>
	<?php endif;?>
	<?php if($_REQUEST['caseType']!="Adoption"):?>	
	<div style="width:320px;margin:10px; float:left;">	
	<fieldset><legend>Deceased Details</legend>
	<p class="form_input">Name of Deceased<br><input  type="text" name="deceased_name" /></p>
    <p class="form_input">Date of Death<br><input  type="date" name="death_date" /></p>		
	</fieldset>
	</div>	
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="PA"):?>	
	<div style="width:320px;margin:10px; float:left;">	
	<fieldset><legend>     </legend>
	<p class="form_input">Date of Gazettement<br><input  type="date" name="date_of_gazettement" /></p>
    <p class="form_input">Date of Confirmation<br><input  type="date" name="death_of_confirmation" /></p>
		
	</fieldset>
	</div>
	<?php endif;?>
	
	
	
	<div style="width:320px;margin:10px;float:left;">
	<fieldset><legend></legend>		
	<p class="form_input">Case Code<br><select name="casecode"  id="casecode" />
	<?php
	$result=mysql_query("select * from casecodes");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['codename']."'>".$row['codename']."</option>";
	}	
	?>
	</select>
	</p>
	<p class="form_input">Case is coming for <br><select name="casestate"  id="casestate" />
	<?php
	$result=mysql_query("select * from case_states");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['state_name']."'>".$row['state_name']."</option>";
	}	
	?>
	</select>
	</p>
	<p class="form_input"><input  type="submit" name="register_case" value="SUBMIT" /></p>
	</fieldset>
	</div>
	
	
	</form>
        <div style="width:320px;margin:10px;float:left;">
	<?php
	if(isset($_POST['register_case'])){
		
		
	   if($_POST['caseType']=="PA"):	
		
		$caseNo=mysql_real_escape_string($_POST['caseNo']);
		$caseType=mysql_real_escape_string($_POST['caseType']);
		$CaseState=mysql_real_escape_string($_POST['CaseState']);
		$casecode=mysql_real_escape_string($_POST['casecode']);
		
        $caseDetails="<b>Deceased Name<b> :".mysql_real_escape_string($_POST['deceased_name'])."<br>"
		."<b>Death Date<b> :".mysql_real_escape_string($_POST['death_date'])."<br>"
		."<b>Date of gazettement<b> :".mysql_real_escape_string($_POST['date_of_gazettement'])."<br>"
		."<b>Date of confirmation<b> :".mysql_real_escape_string($_POST['death_of_confirmation'])."<br>";
		
         if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}	
			
		elseif($_POST['caseType']=="Ad litem"):
		
			$caseNo=mysql_real_escape_string($_POST['caseNo']);
			$caseType=mysql_real_escape_string($_POST['caseType']);
			$casecode=mysql_real_escape_string($_POST['casecode']);
			$CaseState=mysql_real_escape_string($_POST['CaseState']);
			$caseDetails="<b>Deceased Name<b> :".mysql_real_escape_string($_POST['deceased_name'])."<br>"
		     ."<b>Death Date<b> :".mysql_real_escape_string($_POST['death_date'])."<br>";
			
			
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`Division`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','Family','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
		
		elseif($_POST['caseType']=="Citation"):
		
			$caseNo=mysql_real_escape_string($_POST['caseNo']);            
		    $caseType=mysql_real_escape_string($_POST['caseType']);
			$casecode=mysql_real_escape_string($_POST['casecode']);
			$CaseState=mysql_real_escape_string($_POST['CaseState']);
            $caseDetails="<b>Deceased Name<b> :".mysql_real_escape_string($_POST['deceased_name'])."<br>"
		    ."<b>Death Date<b> :".mysql_real_escape_string($_POST['death_date'])."<br>"
		    ."<b>Citor ID<b> :".mysql_real_escape_string($_POST['citor_id_number'])."<br>"
		    ."<b>Citee ID<b> :".mysql_real_escape_string($_POST['citee_id_number'])."<br>";			
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`Division`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','Family','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
			
			elseif($_POST['caseType']=="Adoption"):
		
			$caseNo=mysql_real_escape_string($_POST['caseNo']);            
		    $caseType=mysql_real_escape_string($_POST['caseType']);
			$CaseState=mysql_real_escape_string($_POST['CaseState']);
            $caseDetails="<b>Name of Child<b> :".mysql_real_escape_string($_POST['name_of_child'])."<br>";			
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`Division`,`CaseDetails`,`TimeStamp`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','Family','{$caseDetails}','".time()."')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
	   endif;
		
		?>
	<script>document.location="regFamilyCases.php?caseType=<?php echo $_REQUEST['caseType'];?>";</script>
  <?php		
	}
	?>
	</div>             
               

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
