<div class="leftmenu">        
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>
                  <li class="active"><a href="Registrar.php"><i class="iconfa-home"></i></span>HOME</a></li>
                				
				 <li class="dropdown"><a href=""><span class="iconfa-pencil"></span>Cases</a>
				 	
                	<ul>
					<li class=""><a href="viewCases.php"><i class="iconfa-pencil"></i></span> View Cases</a></li>
					<li class="dropdown"><a href=""><span class="iconfa-pencil"></span> Add Cases</a>
					    <ul>
						<li class="dropdown"><a href=""><span class="iconfa-book"></span> Criminal Division </a>
								<ul>
								<li class=""><a href="regCriminalCases.php?caseType=Criminal Misc"><span class="iconfa-laptop"></span>Criminal Misc</a>
								</li>
								<li class=""><a href="murdercases.php"><span class="iconfa-laptop"></span> Murder</a></li>
								<li class=""><a href="cr_cases.php"><span class="iconfa-laptop"></span>Ordinary Cr. Appeals </a></li>
								<li class=""><a href="regCriminalCases.php?caseType=Capital Appeals"><span class="iconfa-laptop"></span>Capital Appeals </a>
								</li>
								<li class=""><a href="regCriminalCases.php?caseType=Criminal revision"><span class="iconfa-laptop"></span> Criminal revision</a></li>
							
					
							   </ul>
							</li>
						
						
						
                    	<li class="dropdown"><a href="regFamilyCases.php"><span class="iconfa-book"></span> Family Division</a>
						   <ul>
							<li class=""><a href="regFamilyCases.php?caseType=PA"><span class="iconfa-laptop"></span>P&A </a>
							</li>
							<li class=""><a href="regFamilyCases.php?caseType=Ad litem"><span class="iconfa-laptop"></span> Ad litem</a></li>
							<li class=""><a href="regFamilyCases.php?caseType=Citation"><span class="iconfa-laptop"></span>Citation</a></li>
							<li class=""><a href="regFamilyCases.php?caseType=Adoption"><span class="iconfa-laptop"></span>Adoption </a>
							</li>
							
						
				
						   </ul>
						
						
						
						
						
						
						
						</li>
			            <li class="dropdown"><a href="regCivilCases.php"><span class="iconfa-book"></span>Civil Divisions </a>
						
						<ul>
						
							<li class=""><a href="regCivilCases.php?caseType=Misc.appeals"><span class="iconfa-laptop"></span>Misc.appeals </a>
							</li>
							<li class=""><a href="regCivilCases.php?caseType=Civil Appeals"><span class="iconfa-laptop"></span> Civil Appeals</a></li>
							<li class=""><a href="regCivilCases.php?caseType=Judicial Review"><span class="iconfa-laptop"></span>Judicial Review</a></li>
							<!--<li class=""><a href="regCivilCases.php?caseType=Divorce"><span class="iconfa-laptop"></span>Divorce</a>
							</li>-->
							<li class=""><a href="regCivilCases.php?caseType=Civil"><span class="iconfa-laptop"></span>Civil </a></li>
							<li class=""><a href="regCivilCases.php?caseType=Petition"><span class="iconfa-laptop"></span>Petition </a>
							</li>
							
						
				
						   </ul>
						
						
						</li>
						   <li class="dropdown"><a href=""><span class="iconfa-book"></span> Commercial Division</a>						   
							<ul>
							<li class=""><a href="regCommercialCases.php?caseType=Commercial Matters"><span class="iconfa-laptop"></span>Commercial Matters</a>
							</li>
							<li class=""><a href="regCommercialCases.php?caseType=Commercial High Court Misc"><span class="iconfa-laptop"></span> Commercial High Court Misc</a></li>
						<li class=""><a href="regCommercialCases.php?caseType=Winding Up Cause"><span class="iconfa-laptop"></span>Winding Up Cause</a></li>
							<li class=""><a href="regCommercialCases.php?caseType=Income Tax Appeal"><span class="iconfa-laptop"></span>Income Tax Appeal</a>
							</li>
						<li class=""><a href="regCommercialCases.php?caseType=Bankruptcy Cause"><span class="iconfa-laptop"></span>Bankruptcy Cause</a></li>
						
						
					<li class=""><a href="regCommercialCases.php?caseType=Bankruptcy Notice"><span class="iconfa-laptop"></span>Bankruptcy Notice</a></li>	
				
						   </ul>
						</li>
						<li class=""><a href="electionpetition.php"><span class="iconfa-laptop"></span>Election Petition </a>
							</li>
					</ul>
			       </li>
                    </ul>
                </li>				
				<li class="dropdown"><a href=""><span class="iconfa-legal"></span>Lawyers</a>
				     <ul>
					    <li ><a href="registeredLawyers.php"><i class="iconfa-legal"></i></span>View Lawyers</a></li>
			           <li ><a href="lawyerReg.php"><i class="iconfa-legal"></i></span>Register Lawyer</a></li>
			
			         </ul>           
				  
				  
				  <li class="dropdown"><a href=""><span class="iconfa-book"></span> Sentencing Register</a>
                              <ul>
        
			<li class=""><a href="sentenceRegister2.php?caseType=Criminal Murder"><span class="iconfa-laptop"></span>Criminal Murder</a>
			</li>
			
			<li class=""><a href="sentenceRegister2.php?caseType=Criminal Ord Appeal"><span class="iconfa-laptop"></span>Criminal Ord Appeal</a></li>
			</ul>
			 </li>		
				  
				  
				  
				  
				  
				  
				  
    <li class="dropdown"><a href=""><span class="iconfa-list"></span> Cause List</a>
      <ul>
        <li class="dropdown"><a href="#"><span class="iconfa-book"></span> Criminal Division </a>
				<ul>
			<li class=""><a href="causelist.php?caseType=Criminal Misc"><span class="iconfa-laptop"></span>Criminal Misc</a>
			</li>
			<li class=""><a href="causelist.php?caseType=Murder"><span class="iconfa-laptop"></span> Murder</a></li>
		  <li class=""><a href="causelist.php?caseType=Criminal revision"><span class="iconfa-laptop"></span> Criminal revision</a></li>
			<li class=""><a href="causelist.php?caseType=Criminal Ord Appeal"><span class="iconfa-laptop"></span>Criminal Ord Appeal</a></li>
			</ul>
			 </li>		
				  </ul>
				       </li>
			<li class="dropdown"><a href=""><span class="iconfa-envelope"></span>Messages</a>
				     <ul>
					 <li ><a href="compose.php"><i class="iconfa-envelope"></i></span>Compose</a></li>
					    <li ><a href="messages.php"><i class="iconfa-envelope"></i></span>Lawyers</a></li>
			           <li ><a href="messages.php"><i class="iconfa-envelope"></i></span>Prosecuters</a></li>
					   <li ><a href="messages.php"><i class="iconfa-envelope"></i></span>Investigators</a></li>
			
			         </ul> 
          </li>
		  <li class=""><a href="livechat.php"><span class="fa fa-comments"></span>Live Chat</a></li>
            </ul>
        </div><!--leftmenu-->