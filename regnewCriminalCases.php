<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['BadgeNo']) || trim ($_SESSION['BadgeNo']==''))
{
header("Location:index.php");
}
include"functions.php";
$court="Kisumu High Court";
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
	
    
    <link rel="stylesheet" href="css/responsive-tables.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-responsive.min.css">


    <link href="css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
	 <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/forms.css" type="text/css">
   	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    
	<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
	 <script src="js/app.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/custom.js"></script>
	
	
	<script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
	<script>
	webshims.setOptions('forms-ext', {types: 'date'});
	webshims.polyfill('forms forms-ext');
	$.webshims.formcfg = {
	en: {
		dFormat: '-',
		dateSigns: '-',
		patterns: {
			d: "yy-mm-dd"
		}
	}
	};
	</script>
	
    <script type="text/javascript">
        jQuery(document).ready(function(){
			
			jQuery('.numbersOnly').keyup(function () {
			if (this.value != this.value.replace(/[^0-9\.]/g, '')) {
			   this.value = this.value.replace(/[^0-9\.]/g, '');
			}
		      });
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
			
		
	  jQuery("#add_doctor").click(function(e){
      jQuery('#add_doctor').attr('disabled',true);
	  jQuery('#add_doctor').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {	
            caseNo:jQuery("#set_case_No").val(),		  
			id_number:jQuery("#doctor_id").val(),		
			email:jQuery("#doctor_email").val(),
			name_of_party:jQuery("#doctor_name").val(),
			phone_number:jQuery("#doctor_phone").val(),
			postal_address:jQuery("#doctor_address").val(),
			doctor:"yes"
		},
		  function(data,status){
			
			jQuery("#doctor_id").val("")
			jQuery("#doctor_name").val(""),
			jQuery("#doctor_phone").val("")
			jQuery("#specialization").val("")
			jQuery("#doctor_email").val("")
			jQuery("#doctor_address").val("")
			jQuery('#add_doctor').attr('disabled',false);
			jQuery('#add_doctor').val('SUBMITED');
	        jQuery('#add_doctor').val('SUBMIT');
			
			
			
		  });
		
	});	
	
	
	
		jQuery("#add_investigator").click(function(e){
      jQuery('#add_investigator').attr('disabled',true);
	  jQuery('#add_investigator').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {	
            caseNo:jQuery("#set_case_No").val(),		  
			id_number:jQuery("#investigator_id").val(),
			station:jQuery("#station").val(),
			email:jQuery("#investigator_email").val(),
			name_of_party:jQuery("#investigator_name").val(),
			phone_number:jQuery("#investigator_phone").val(),
			postal_address:jQuery("#investigator_address").val(),
			investigator:"yes"
		},
		  function(data,status){
			
			jQuery("#investigator_id").val("")
			jQuery("#investigator_name").val("")
			jQuery("#investigator_phone").val("")
			jQuery("#specialization").val("")
			jQuery("#investigator_email").val("")
			jQuery("#investigator_address").val("")
			jQuery('#add_investigator').attr('disabled',false);
			jQuery('#add_investigator').val('SUBMITED');
	        jQuery('#add_investigator').val('SUBMIT');
			
			
			
		  });
		
	});

jQuery("#int_parties").click(function(e){
      jQuery('#int_parties').attr('disabled',true);
	  jQuery('#int_parties').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#id_number").val(),
			name_of_party:jQuery("#name_of_party").val(),
			dob:jQuery("#int_dob").val(),
			lawyer_id:jQuery("#int_lawyer_id").val(),
			gender:jQuery("#int_gender").val(),
			phone_number:jQuery("#phone_number").val(),
			postal_address:jQuery("#postal_address").val(),
			party:"yes"
		},
		  function(data,status){
			
			jQuery("#id_number").val("")
			jQuery("#name_of_party").val("")
			jQuery("#phone_number").val("")
			jQuery("#postal_address").val("")
			jQuery("#int_dob").val("")
			jQuery("#int_gender").val("")
			jQuery('#int_parties').attr('disabled',false);
			jQuery('#int_parties').val('SUBMITED');
	        jQuery('#int_parties').val('Click to add');
			
			
			
		  });
		
	});	
			


	jQuery("#add_respondent").click(function(e){
      jQuery('#add_respondent').attr('disabled',true);
	  jQuery('#add_respondent').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#resp_id").val(),
			name_of_party:jQuery("#resp_name").val(),
			dob:jQuery("#resp_dob").val(),
			lawyer_id:jQuery("#resp_lawyer_id").val(),
			gender:jQuery("#resp_gender").val(),
			phone_number:jQuery("#resp_phone").val(),
			postal_address:jQuery("#resp_address").val(),
			crime_resp:"yes"
		},
		  function(data,status){
			
			jQuery("#resp_id").val("")
			jQuery("#resp_name").val(""),
			jQuery("#resp_phone").val("")
			jQuery("#resp_dob").val("")
			jQuery("#resp_gender").val("")
			jQuery("#resp_address").val("")
			jQuery('#add_respondent').attr('disabled',false);
			jQuery('#add_respondent').val('SUBMITED');
	        jQuery('#add_respondent').val('Click to add');
			
			
			
		  });
		
	});		

	
	jQuery("#add_accused").click(function(e){
		
      jQuery('#add_accused').attr('disabled',true);
	  jQuery('#add_accused').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#accused_id").val(),
			name_of_party:jQuery("#accused_name").val(),
			dob:jQuery("#accused_dob").val(),
			gender:jQuery("#accused_gender").val(),
			lawyer_id:jQuery("#acc_lawyer_id").val(),
			phone_number:jQuery("#accused_phone").val(),
			postal_address:jQuery("#accused_address").val(),
			accu_custody:jQuery("#accu_custody").val(),			
			crime_accused:"yes"
		},
		  function(data,status){
			
			jQuery("#accused_id").val("")
			jQuery("#accused_name").val(""),
			jQuery("#accused_phone").val("")
			jQuery("#accused_dob").val("")
			jQuery("#accused_gender").val("")
			jQuery("#accused_address").val("")
			jQuery('#add_accused').attr('disabled',false);
			jQuery('#add_accused').val('SUBMITED');
	        jQuery('#add_accused').val('Click to add');
			
			
			
		  });
		
	});	
		
	
	jQuery("#add_applicant").click(function(e){
      jQuery('#add_applicant').attr('disabled',true);
	  jQuery('#add_applicant').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#applicant_id").val(),
			dob:jQuery("#applicant_dob").val(),
			gender:jQuery("#applicant_gender").val(),
			lawyer_id:jQuery("#app_lawyer_id").val(),
			name_of_party:jQuery("#applicant_name").val(),
			phone_number:jQuery("#applicant_phone").val(),
			postal_address:jQuery("#applicant_address").val(),
			crime_applicant:"yes"
		},
		  function(data,status){
			
			jQuery("#applicant_id").val("")
			jQuery("#applicant_name").val(""),
			jQuery("#applicant_phone").val("")
			jQuery("#applicant_dob").val("")
			jQuery("#applicant_gender").val("")
			jQuery("#applicant_address").val("")
			jQuery('#add_applicant').attr('disabled',false);
			jQuery('#add_applicant').val('SUBMITED');
	        jQuery('#add_applicant').val('Click to add');
			
			
			
		  });
		
	});	


	jQuery("#add_witness").click(function(e){
      jQuery('#add_witness').attr('disabled',true);
	  jQuery('#add_witness').val('Submitting....');
	
	jQuery.post("JProcessor.php",
		  {
		  
			caseNo:jQuery("#set_case_No").val(),
			id_number:jQuery("#witness_id").val(),
			dob:jQuery("#witness_dob").val(),
			gender:jQuery("#witness_gender").val(),			
			name_of_party:jQuery("#witness_name").val(),
			phone_number:jQuery("#witness_phone").val(),
			postal_address:jQuery("#witness_address").val(),
			crime_witness:"yes"
		},
		  function(data,status){
			
			jQuery("#witness_id").val("")
			jQuery("#witness_name").val(""),
			jQuery("#witness_phone").val("")
			jQuery("#witness_dob").val("")
			jQuery("#witness_gender").val("")
			jQuery("#witness_address").val("")
			jQuery('#add_witness').attr('disabled',false);
			jQuery('#add_witness').val('SUBMITED');
	        jQuery('#add_witness').val('Click to add');
			
			
			
		  });
		
	});	


jQuery('#set_case_No').keyup(function () {
     
	
	jQuery.post("JProcessor.php",
		  {
		  
			set_case_No:jQuery("#set_case_No").val()		
		},
		  function(data,status){     
			
			
			
		  });
		
	});	
        });
		
		function reset_form_element (e) {
			e.wrap('<form>').parent('form').trigger('reset');
			e.unwrap();
		}
		
	function upload_file(){
		
		if(jQuery("#filedate").val()==""){
			
			alert("Date of file must be entered first");			
            reset_form_element( $('#attach') );
			$('#attach').preventDefault();
				
			}else{
    
	jQuery('#uploaded').val('Attaching.......');
	var attach = document.getElementById("attach");
	var caseNo = jQuery("#set_case_No").val();
    var filedate = jQuery("#filedate").val();	
    var file = attach.files[0];
	
    formData = new FormData();
	
    formData.append('attach', file);
	formData.append('caseNo', caseNo);
    formData.append('filedate', filedate);	
		//var formData = new FormData($('#attach')[0]);		
  jQuery.ajax({
    url: 'upload_letters.php',
    type: 'POST',
    data: formData,
	enctype: 'multipart/form-data',
    async: true,
    cache: false,
    contentType: false,
    processData: false,
	
    success: function (returndata) {
		
		jQuery('#uploaded').val("Attached");
		jQuery('#att').html(returndata);
	}
  });
}
}	
		
		
		
    </script>
   
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">
         <?php include"reg_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">       

        <div class="maincontent">
            <div class="maincontentinner">
	<form action="regnewCriminalCases.php" method="post"/>
       <h4 class="widgettitle" align="center">	   
	   <?php echo $_REQUEST['caseType']; ?>
	   
	   &nbsp&nbsp&nbsp Case No: <input  type="text" id="set_case_No" name="set_case_No" placeholder=" Enter Case Number"
        VALUE="<?php if(isset($_SESSION['case_no'])) echo $_SESSION['case_no']; ?>"/>( Enter case number before entering any details)
        <br>
	<?php
	if(isset($_POST['register_case'])){
		
	   if($_POST['caseType']!="Criminal revision" && $_POST['caseType']!="Murder"):	
		
		$caseNo=mysql_real_escape_string($_POST['set_case_No']);
		$casecode=mysql_real_escape_string($_POST['casecode']);
		$CaseState=mysql_real_escape_string($_POST['casestate']);
		$formercourt=mysql_real_escape_string($_POST['formercourt']);
		$FormerCaseNumber=mysql_real_escape_string($_POST['formerofficer']);
        $caseDetails=mysql_real_escape_string($_POST['case_particulars']);
		$caseType=mysql_real_escape_string($_POST['caseType']);
		$station=mysql_real_escape_string($_POST['station']);
		$court=mysql_real_escape_string($court);
		$wh=$_POST['wh'];
		$wl=$_POST['wl'];
		$dpp=$_POST['dpp'];
		$ag=$_POST['ag'];
		$status=$_POST['status'];
		$judge=$_POST['judge'];
		$filingDay=mysql_real_escape_string($_POST['filingDay']);
		
		$penal=$_POST['penalcode'];
         foreach($penal as $section){
			 if($section!=""){
				$penalcode=$section; 
			 }
		 }
		
         if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`xcourt`,`Division`,`CaseDetails`,`TimeStamp`,`Station`,`Court`,`wh`,`wl`,`penalcode`,`dpp`,`ag`,`filingDay`,`status`,`JudgeName`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','".$formercourt."-(".$FormerCaseNumber.")','Criminal','{$caseDetails}','".time()."','{$station}','{$court}','{$wh}','{$wl}','{$penalcode}','{$dpp}','{$ag}','{$filingDay}','{$status}','{$judge}')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}	
			
		elseif($_POST['caseType']=="Murder"):
		        $status=$_POST['status'];
			$caseNo=mysql_real_escape_string($_POST['set_case_No']);
			$casecode=mysql_real_escape_string($_POST['casecode']);
			$CaseState=mysql_real_escape_string($_POST['casestate']);
			$formercourt=mysql_real_escape_string($_POST['formercourt']);
		    $FormerCaseNumber=mysql_real_escape_string($_POST['formerofficer']);
			$caseDetails=mysql_real_escape_string($_POST['case_particulars']);
			$caseType=mysql_real_escape_string($_POST['caseType']);
			$DateOfPlea=$_POST['DateOfPlea'];
			$station=mysql_real_escape_string($_POST['station']);
		    $court=mysql_real_escape_string($court);
			$wh=$_POST['wh'];
		    $wl=$_POST['wl'];
			$dpp=$_POST['dpp'];
		    $ag=$_POST['ag'];
		    $status=$_POST['status'];
		    $judge=$_POST['judge'];
			$filingDay=mysql_real_escape_string($_POST['filingDay']);
			$penal=$_POST['penalcode'];
            foreach($penal as $section){
			 if($section!=""){
				$penalcode=$section; 
			 }
		    }
		
            if(mysql_query("INSERT INTO `cases`(`caseNo`,`CaseType`,`casecode`,`CaseState`,`xcourt`,`Division`,`CaseDetails`,`DateOfPlea`,`TimeStamp`,`Station`,`Court`,`wh`,`wl`,`penalcode`,`dpp`,`ag`,`filingDay`,`status`,`JudgeName`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','".$formercourt."-(".$FormerCaseNumber.")','Criminal','{$caseDetails}','{$DateOfPlea}','".time()."','{$station}','{$court}','{$wh}','{$wl}','{$penalcode}','{$dpp}','{$ag}','{$filingDay}','{$status}','{$judge}')"))
			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}			
		
		elseif($_POST['caseType']=="Criminal revision"):
		        $status=$_POST['status'];
			$caseNo=mysql_real_escape_string($_POST['set_case_No']);
			$casecode=mysql_real_escape_string($_POST['casecode']);
			$CaseState=mysql_real_escape_string($_POST['casestate']);
			$formercourt=mysql_real_escape_string($_POST['formercourt']);
		    $FormerCaseNumber=mysql_real_escape_string($_POST['formerofficer']);
            $caseDetails=mysql_real_escape_string($_POST['case_particulars']);
		    $caseType=mysql_real_escape_string($_POST['caseType']);
			$CommunityServinceOrder=mysql_real_escape_string($_POST['CommunityServinceOrder']);
			$station=mysql_real_escape_string($_POST['station']);
		    $court=mysql_real_escape_string($court);
			$wh=$_POST['wh'];
		     $wl=$_POST['wl'];
			 $dpp=$_POST['dpp'];
		      $ag=$_POST['ag'];
		      $status=$_POST['status'];
		      $judge=$_POST['judge'];
			  $filingDay=mysql_real_escape_string($_POST['filingDay']);
			 $penal=$_POST['penalcode'];
              foreach($penal as $section){
			 if($section!=""){
				$penalcode=$section; 
			 }
		     }
		
            if(mysql_query("INSERT INTO `cases` (`caseNo`,`CaseType`,`casecode`,`CaseState`,`xcourt`,`Division`,`CaseDetails`,`CommunityServinceOrder`,`TimeStamp`,`Station`,`Court`,`wh`,`wl`,`penalcode`,`dpp`,`ag`,`filingDay`,`status`,`JudgeName`)
            VALUES 
			('{$caseNo}','{$caseType}','{$casecode}','{$CaseState}','".$formercourt."-(".$FormerCaseNumber.")','Criminal','{$caseDetails}','{$CommunityServinceOrder}','".time()."','{$station}','{$court}','{$wh}','{$wl}','{$penalcode}','{$dpp}','{$ag}','{$filingDay}',`{$status}`,'{$judge}')"))			
			{
			echo"Submitted";	
			}else{ echo mysql_error();}
	   endif;		
	?>
	<!--<script>document.location="regCriminalCases.php?caseType=<?php echo $_REQUEST['caseType'];?>";</script>-->
  <?php	
	}
	?>			
	   </h4>
   
	<input  type="hidden" name="caseNo" id="caseNo" value="<?php echo case_no();?>" />
	<input type="hidden" name="caseType" value="<?php echo $_REQUEST['caseType'];?>" />
	
	<?php if($_REQUEST['caseType']!="Murder"):?>	
	 <div class="col-md-12">
              <div class="box box-default collapsed-box col-md-12">
                <div class="box-header with-border">
<?php if($_REQUEST['caseType']!="Ordinary Cr. Appeals" && $_REQUEST['caseType']!="Capital Appeals"):?>	
                  <h3 class="box-title">Applicant  </h3>
				<?php else: ?>  
				<h3 class="box-title">Appellant	 </h3>
				<?php endif;?>
				
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
      	
	<div class="form-group form_input col-md-3">
        Name of Applicant<br>
        <input type="text" id="applicant_name"  placeholder="Enter ..."/>
    </div>
					
	
	<div class="form-group form_input col-md-3">ID Number<br><input  type="text" id="applicant_id" /></div>
	<div class="form-group form_input col-md-3">Date of Birth<br><input  type="date" id="applicant_dob" /></div>
	<div class="form-group form_input col-md-3">Gender<br><select id="applicant_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</div>
	
	
<?php if($_REQUEST['caseType']!="Ordinary Cr. Appeals" && $_REQUEST['caseType']!="Capital Appeals"):?>
	<div class="form-group form_input col-md-3">Lawyer<br><select  id="app_lawyer_id" />
	<option value="">No lawyer</option>
	<?php
	$result=mysql_query("select * from lawyer");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['LawyerId']."'>".$row['Name']."(".$row['IdNo'].")</option>";
	}
	
	?>
	</select>
	</div>
<?php endif;?>
	<div class="form-group form_input col-md-3">Phone Number<br><input  type="text" id="applicant_phone" /></div>
	
	<div class="form-group form_input col-md-3">Postal Address<br><input  type="text" id="applicant_address" /></div>
	
	<div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label><input  type="button" id="add_applicant" value="Click to add" /></div>	
	
	
	
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->

	
	
	
	
	<?php endif;?>

		
	<?php if($_REQUEST['caseType']!="Murder"&&$_REQUEST['caseType']!="Ordinary Cr. Appeals" && $_REQUEST['caseType']!="Capital Appeals"):?>
	 <div class="col-md-12">
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Respondent  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
	<div class="form-group form_input col-md-3">Name of Respondent<br><input  type="text" id="resp_name" /></div>
	<div class="form-group form_input col-md-3">ID Number<br><input  type="text" id="resp_id" /></div>
	<div class="form-group form_input col-md-3">Date of Birth<br><input  type="date" id="resp_dob" /></div>
	<div class="form-group form_input col-md-3">Gender<br><select id="resp_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</div>
	<div class="form-group form_input col-md-3">Lawyer<br><select  id="resp_lawyer_id" />
	<option value="">No lawyer</option>
	<?php
	$result=mysql_query("select * from lawyer");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['LawyerId']."'>".$row['Name']."(".$row['IdNo'].")</option>";
	}
	
	?>
	</select>
	</div>
	<div class="form-group form_input col-md-3">Phone Number<br><input  type="text" id="resp_phone" /></div>
	<div class="form-group form_input col-md-3">Postal Address<br><input  type="text" id="resp_address" /></div>
    <div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label>
	<input  type="button" id="add_respondent" value="Click to add" /></div>
	   </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="Murder"):?>
	 <div class="col-md-12">
              <div class="box box-default collapsed-box col-md-12">
                <div class="box-header with-border">
                  <h3 class="box-title">Accused  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
	<div class="form-group form_input col-md-3">
	<label>Name of Accused</label>
	<input  type="text" class="form-control" id="accused_name" />
	</div>
	
	<div class="form-group form_input col-md-3">
	<label>ID Number<label>
	<input  type="text" class="form-control" id="accused_id" />
	</div>
	
	
	<div class="form-group form_input col-md-3">
	<label>Date of Birth</label>
	<input  type="date" id="accused_dob" />
	</div>
	
	<div class="form-group form_input col-md-3">
	<label>Gender</label>
	<select id="accused_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</div>
	<div class="form-group form_input col-md-3">Lawyer<br>
	<select  id="acc_lawyer_id" />
	<option value="">No lawyer</option>
	<?php
	$result=mysql_query("select * from lawyer");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['LawyerId']."'>".$row['Name']."(".$row['IdNo'].")</option>";
	}
	
	?>
	</select>
	</div>
	<div class="form-group form_input col-md-3"><label>Phone Number</label><input  type="text" id="accused_phone" /></div>
	<div class="form-group form_input col-md-3">Postal Address<br><input  type="text" id="accused_address" /></div>
	<div class="form-group form_input col-md-3">Custody<br>
	<select  id="accu_custody"  />
	<option value=""></option>
    <option value="IN CUSTODY">IN CUSTODY</option>
	<option value="NOT IN CUSTODY">NOT IN CUSTODY</option>
	<option value="ON BAIL">ON BAIL</option>
	<option value="NO BAIL">NO BAIL</option>
	<option value="ESCAPED">ESCAPED</option>
	<option value="WARRANT ISSUED">WARRANT ISSUED</option>
	</select>
	</div>	
	
	<div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label>
	<input  type="button" id="add_accused" value="Click to add" /></div>
	   </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	<?php endif;?>
<?php if($_REQUEST['caseType']!="Capital Appeals"):?>
 <div class="col-md-12">
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Interested Parties  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">	
	<div class="form-group form_input col-md-3">Name<br><input  type="text" id="name_of_party" /></div>
	<div class="form-group form_input col-md-3">ID Number<br><input  type="text" id="id_number" /></div>
	<div class="form-group form_input col-md-3">Date of Birth<br><input  type="date" id="int_dob" /></div>
	<div class="form-group form_input col-md-3">Gender<br><select id="int_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</div>
	<div class="form-group form_input col-md-3">Lawyer<br><select  id="int_lawyer_id" />
	<option value="">No lawyer</option>
	<?php
	$result=mysql_query("select * from lawyer");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['LawyerId']."'>".$row['Name']."(".$row['IdNo'].")</option>";
	}
	
	?>
	</select>
	</div>
	<div class="form-group form_input col-md-3">Phone Number<br><input  type="text" id="phone_number" /></div>
	<div class="form-group form_input col-md-3">Postal Address<br><input  type="text" id="postal_address" /></div>
	<div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label><input  type="button" id="int_parties" value="Click to add" /></div>
	
   </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	<?php endif;?>
	 
	 
	<?php if($_REQUEST['caseType']=="Murder"):?>
	 <div class="col-md-12">
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Doctor  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
	<div class="form-group form_input col-md-3">Name<br><input  type="text" id="doctor_name" /></div>
	<input  type="hidden" value="<?php echo time()?>;" id="doctor_id" />
    
	<div class="form-group form_input col-md-3">Phone Number<br><input  type="text" id="doctor_phone" /></div>
	<div class="form-group form_input col-md-3">Address<br><input  type="text" id="doctor_address" /></div>
	<div class="form-group form_input col-md-3">Email<br><input  type="email" id="doctor_email" /></div>
	<div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label><input  type="button" id="add_doctor" value="Click to add" /></div>	
	   </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	<?php endif;?>
	
	<?php if($_REQUEST['caseType']=="Murder"):?>
	 <div class="col-md-12">
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Investigating Officer  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
	<div class="form-group form_input col-md-3">Name<br><input  type="text" id="investigator_name" /></div>
	<div class="form-group form_input col-md-3"> Forces Number<br><input  type="text" id="investigator_id" /></div>
    <div class="form-group form_input col-md-3">Station<br><input  type="text" id="station" /></div>	
	<div class="form-group form_input col-md-3">Phone Number<br><input  type="text" id="investigator_phone" /></div>
	<div class="form-group form_input col-md-3">Address<br><input  type="text" id="investigator_address" /></div>
	<div class="form-group form_input col-md-3">Email<br><input  type="email" id="investigator_email" /></div>
	<div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label><input  type="button" id="add_investigator" value="Click to add" /></div>	
	   </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	<?php endif;?>
	
<?php if($_REQUEST['caseType']!="Ordinary Cr. Appeals" && $_REQUEST['caseType']!="Capital Appeals"):?>
	 <div class="col-md-12">
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Witness  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
	<div class="form-group form_input col-md-3">Name of witness<br><input  type="text" id="witness_name" /></div>
	<div class="form-group form_input col-md-3">ID Number<br><input  type="text" id="witness_id" /></div>
	<div class="form-group form_input col-md-3">Date of Birth<br><input  type="date" id="witness_dob" /></div>
	<div class="form-group form_input col-md-3">Gender<br><select id="witness_gender" />
	<option value="Male">Male</option>
	<option value="Female">Female</option>
	</select>
	</div>
	
	<div class="form-group form_input col-md-3">Phone Number<br><input  type="text" id="witness_phone" /></div>
	<div class="form-group form_input col-md-3">Postal Address<br><input  type="text" id="witness_address" /></div>
	
	<div class="form-group form_input footer col-md-3">
	<label>Add all before submitting the form</label>
	<input  type="button" id="add_witness" value="Click to add" /></div>	
	   </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	
	<?php endif;?>
	 <div class="col-md-12">
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Offences  </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
	<?php
	$i=0;
	$result=mysql_query("SELECT distinct(Chapter)FROM penalcode");
	while($row=mysql_fetch_array($result)){	   
  	
	?>

			<div class="form-group form_input col-md-3">			
			<select name="penalcode[]" />
			<option value=""><?php echo $row['Chapter'];?></option>
			<?php
			$result1=mysql_query("SELECT Section,Description FROM penalcode where Chapter='".$row['Chapter']."'");
			while($row1=mysql_fetch_array($result1)){
			echo"<option value='".$row1['Section']."'>".$row1['Section']."(".$row1['Description'].")</option>";			
			}  	
			?>
			</select>
			</div>
	<?php } ?>

	   </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	
	
	
	
	
	
	
	

	<?php if($_REQUEST['caseType']=="Criminal revision"):?>
	 <div class="col-md-12">
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Community Service order </h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
	
	<div class="form-group form_input col-md-3">Community Service order<input  type="radio" name="CommunityServinceOrder" value="YES" />Yes
	<input  type="radio" name="CommunityServinceOrder" value="NO" />No</div>
	   </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	<?php endif;?>	
	
	
	 <div class="col-md-12">
              <div class="box box-default collapsed-box">
                <div class="box-header with-border">
                  <h3 class="box-title">Case Details</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
<?php if($_REQUEST['caseType']!="Ordinary Cr. Appeals" && $_REQUEST['caseType']!="Capital Appeals"):?>	
	<div class="form-group form_input col-md-3">Police station<br><select name="station" />
	<?php
	$result=mysql_query("select * from station where CourtName='".$court."'");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['StationName']."'>".$row['StationName']."</option>";
	}
	
	?>
	</select>
	</div>
<?php endif;?>
<?php if($_REQUEST['caseType']=="Ordinary Cr. Appeals" || $_REQUEST['caseType']=="Capital Appeals"):?>	
<div class="form-group form_input col-md-3">Date of Judgement/conviction<br>
<input  type="date" name="judgmentday" /></div>
<div class="form-group form_input col-md-3">Date of Filing<br>
<input  type="date" name="filingDay" /></div>

<div class="form-group form_input col-md-3">Old Case No<br>
<input  type="text" name="oldCaseNo" /></div>

<div class="form-group form_input col-md-3">Extension of time<br>
    <select name="extension" />  	
		<option value='YES'>YES</option>
		<option value='NO'>NO</option>
	</select>
</div>
<?php endif;?>
	
<?php if($_REQUEST['caseType']!="Ordinary Cr. Appeals" && $_REQUEST['caseType']!="Capital Appeals"):?>	
	<div class="form-group form_input col-md-3">No of witnesses heard<br><input  type="number" name="wh" /></div>
	<div class="form-group form_input col-md-3">No of witnesses not heard<br><input  type="number" name="wl" /></div>
	<?php endif;?>
	<div class="form-group form_input col-md-3">Case Code<br>
	<select name="casecode"  id="casecode" />
	<option value=""></option>
	<?php
	$result=mysql_query("select * from casecodes");
	while($row=mysql_fetch_array($result)){
		echo"<option value='".$row['codename']."'>".$row['codename']."</option>";
	}	
	?>
	</select>
	</div>
	<div class="form-group form_input col-md-3">Case is coming for <br>
	<select name="casestate"  id="casestate" />
	<option value=""></option>
	<?php
	$result=mysql_query("select * from case_states");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['state_name']."'>".$row['state_name']."</option>";
	}	
	?>
	</select>
	</div>	
	<div class="form-group form_input col-md-3">Case is coming from <br><select name="formercourt"  id="formercourt" />
	<option value=""></option>
	<?php
	$result=mysql_query("SELECT *
    FROM highcourts");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['courtname']."'>".$row['courtname']."</option>";
	}
    $result=mysql_query("SELECT *
    FROM magistratecourts");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['courtname']."'>".$row['courtname']."</option>";
	}	
	?>
	</select>
	</div>
	<div class="form-group form_input col-md-3"> Former Judicial Officer<br><select name="formerofficer"  id="formerofficer" />
	<option value=""></option>
	<?php
	$result=mysql_query("SELECT * FROM judicial_officers");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['MagistrateName']."'>".$row['MagistrateName']."</option>";
	}  	
	?>
	</select>
	</div>
    <div class="form-group form_input col-md-3">Certificate of urgency<br>
	<select name="certificate_of_urgency"  id="certificate_of_urgency" />
	<option value=""></option>
	<option value="YES">YES</option>
	<option value="NO">NO</option>
	</select>
	</div>	
	 <div class="form-group form_input col-md-3">Case Status<br>
	<select name="status">
	<option value=""></option>
	<option value="Awaiting proceedings">Awaiting proceedings</option>
               <option value="DR to check whether file fowarded">DR to check whether file fowarded</option>
               <option value="Decided File Admitted">Decided File Admitted</option>
               <option value="Hearing date set">Hearing date set</option>
               <option value="Transferred">Transferred</option>
               <option value="TO be placed before the DR for direction">TO be placed before the DR for direction</option>
               <option value="Mention date">Mention date</option>
               <option value="Awaiting Lower Court  File">Awaiting Lower Court  File</option>
               <option value="Mention date set">Mention date set</option>
               <option value="Hearing date set">Hearing date set</option>
	</select>
	</div>
	 <div class="form-group form_input col-md-3">Choose Judge<br>
	<select name="judge">
		<option value=""></option>
	<?php
	$result=mysql_query("SELECT * FROM judicial_officers WHERE CourtName='Kisumu High Court'");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['MagistrateId']."'>".$row['MagistrateName']."</option>";
	}  	
	?>
	</select>
	  
	</div>
<?php if($_REQUEST['caseType']!="Ordinary Cr. Appeals" && $_REQUEST['caseType']!="Capital Appeals"):?>	
	<div class="form-group form_input col-md-3">Office of the DPP<br><select name="dpp"  id="dpp" />
	<option value=""></option>
	<?php
	$result=mysql_query("SELECT * FROM prosecutors");
	while($row=mysql_fetch_array($result)){
	echo"<option value='".$row['ProsecutorName']."'>".$row['ProsecutorName']."</option>";
	}  	
	?>
	</select>
	</div>
	<div class="form-group form_input col-md-3">Office of the Attorney General<br>
	<input type="text" name="ag"  id="ag" />	
	</div>
	<?php endif;?>
	<?php   if($_REQUEST['caseType']=="Murder"):?>
	<div class="form-group form_input col-md-3">Date of Plea<br><input  type="date" name="DateOfPlea" /></div>
	<?php endif;?>
    <div class="form-group form_input col-md-3">Particulars of the case<br><textarea   name="case_particulars" /></textarea></div>	
	<!--<div class="form-group form_input col-md-3">Case Number<br>
	<input  type="text" name="FormerCaseNumber" id="FormerCaseNumber" /></div>-->
	
	
	<?php if($_REQUEST['caseType']=="Ordinary Cr. Appeals" || $_REQUEST['caseType']=="Capital Appeals"):?>
	 <div class="mdbox" style="clear:left;">
	<h3>Letter requesting proceeding</h3>
	 <label>Date of file received from lower courts:</label>
	 <input type="date" id="filedate"  name="filedate"/><br><br>	
	<input type="file" id="attach" OnChange="upload_file()" name="attach"/><br><br>
	 <input type='button' id="uploaded"  value='Select file to attach'/>
	<div id="att">

	</div>
	</div>
	<?php endif;?>
	<div class="form-group form_input footer col-md-3">
	<input  type="submit" name="register_case" value="SUBMIT" /></div>
	   </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
	
	</form>
                <div class="footer">
	
			  
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
