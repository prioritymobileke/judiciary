<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
header("Location:index.php");
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.php"><img src="images/logo1.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>
                                
                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>
<li class=""><a href="dashboard.php"><i class="iconfa-home"></i></span> HOME</a></li>
<li class="dropdown"><a href=""><span class="iconfa-pencil"></span> Today's Cases</a>
                	<ul>
                    	<li class=""><a href="todayNewCases.php"><span class="iconfa-book"></span> Today's New Cases </a></li>
                    	<li class=""><a href="todayTrafficCases.php"><span class="iconfa-book"></span> Today's Traffic Cases </a></li>
			<li class=""><a href="todayOngoingCases.php"><span class="iconfa-book"></span> Today's Ongoing Cases </a></li>
			
                    </ul>
                </li>

<li class=""><a href="newCasesTable.php"><span class="iconfa-laptop"></span> New cases </a></li>
<li class=""><a href="newCasesTrafficTable.php"><span class="iconfa-briefcase"></span> New Traffic cases </a></li>
                
                <li class=""><a href="ongoingCasesTable.php"><span class="iconfa-envelope"></span> Ongoing cases </a></li>
                <li class=""><a href="closedCasesTable.php"><span class="iconfa-signal"></span> Terminated cases </a></li>
                <li class=""><a href="chargedSexOffenders.php"><span class="iconfa-briefcase"></span> Charged sex offenders</a></li>
                <li class="active"><a href="remandees.php"><span class="iconfa-briefcase"></span> Remandees</a></li>


            </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>All Remandees</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">

                  <h4 class="widgettitle">Remandees Table|<a href="reports/scripts/remandees.php" style="color:#fff">  Export <img src="images/images/excel.png" align="center" width="20"></a></h4>
				<div style="background:#fff;padding:10px; width:300px; margin:auto;"><img src="images/images/emblem.png" align="center"></div>
                <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>

                        <th class="head1">Case No</th>
                        <th class="head1">Accused</th>
                        <th class="head1">Remand Facility</th>




                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");


                    $query="select cases.CaseNo,parties.PName,remand.Status,remand.RemandPlace from parties inner join cases on cases.ObNo=parties.ObNo
                     inner join remand on remand.PID=parties.PID where remand.Status is null and cases.CaseStatus !='Sentenced' ";

                    $result=mysql_query($query);
                    if(!(mysql_query($query))){
                        print("could not execute query!");
                        die(mysql_error());

                    }


                    while($row=mysql_fetch_array($result))
                    {

                        $caseno=$row['CaseNo'];
                        $name=$row['PName'];
                        $place=$row['RemandPlace'];


                        ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            

                            <td><?php echo $caseno ?></td>
                            <td><?php echo $name?></td>
                            <td><?php echo $place?></td>


                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
