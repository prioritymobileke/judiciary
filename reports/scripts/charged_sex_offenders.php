<?php

include '../PHPReport.php';
require("../../connect1.php");
$template='invoice.xls';
$config=array(
    'template'=>$template

);
$query="select parties.PName,parties.Dob,parties.Gender,cases.CaseStatus,ob.DateOccurred,ob.LocationOccurred,ob.Station,ob.IncidentType,
 ob.OffenceType,ob.ObNo,parties.ObNo,cases.CaseNo,cases.ObNo from ob inner join parties on parties.ObNo=ob.ObNo inner join cases on
 cases.ObNo=ob.ObNo where ob.IncidentType='Sexual' " ;
$excel=array(array());
$result=mysql_query($query);
$i=0;
$j=0;
while($row=mysql_fetch_array($result))
{
    foreach($row as $item){

        $excel[$j]['CaseNo']= $row['CaseNo'];
        $excel[$j]['ObNo']= $row['ObNo'];
        $excel[$j]['CaseStatus']= $row['CaseStatus'];
        $excel[$j]['PName']= $row['PName'];

        $dob=$row['Dob'];
        $age = date_diff(date_create($dob), date_create('now'))->y;

        $excel[$j]['Age']= $age;
        $excel[$j]['Gender']= $row['Gender'];
        $excel[$j]['OffenceType']= $row['OffenceType'];
        $excel[$j]['Station']= $row['Station'];
        $excel[$j]['DateOccurred']= $row['DateOccurred'];
        $excel[$j]['LocationOccurred']= $row['LocationOccurred'];




    }
    $j++;

}

$R=new PHPReport();


$R->load(array(
        'id'=>'ongoing_cases',
        'header'=>array(
            'CaseNo','ObNo','Case Status','Accused','Age','Gender','Offence','Police Station','Offence Date','Offence Location'
        ),

        'config'=>array(
            'header'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>80,'align'=>'right'),
                2=>array('width'=>300,'align'=>'left'),
                3=>array('width'=>300,'align'=>'left'),
                4=>array('width'=>300,'align'=>'left'),
                5=>array('width'=>300,'align'=>'left'),
                6=>array('width'=>300,'align'=>'left'),
                7=>array('width'=>300,'align'=>'left'),
                8=>array('width'=>300,'align'=>'left'),
                9=>array('width'=>200,'align'=>'left')

            ),
            'data'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>80,'align'=>'right'),
                2=>array('width'=>300,'align'=>'left'),
                3=>array('width'=>300,'align'=>'left'),
                4=>array('width'=>300,'align'=>'left'),
                5=>array('width'=>300,'align'=>'left'),
                6=>array('width'=>300,'align'=>'left'),
                7=>array('width'=>300,'align'=>'left'),
                8=>array('width'=>300,'align'=>'left'),
                9=>array('width'=>200,'align'=>'left')
            ),

        ),
        'data'=>$excel,

    )
);



echo $R->render('excel');
exit();