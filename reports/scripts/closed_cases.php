<?php

include '../PHPReport.php';
require("../../connect1.php");
$template='invoice.xls';
$config=array(
    'template'=>$template

);
$query="select cases.CaseNo,ob.ObNo,cases.Sentence,cases.SentencingDate,GROUP_CONCAT(parties.PName SEPARATOR ' , ' ) as PName from cases inner join
        ob on ob.ObNo=cases.ObNo inner join parties on parties.ObNo=cases.ObNo where CaseStatus='Sentenced' and parties.Role='Accused'  group by cases.CaseNo  " ;
$excel=array(array());
$result=mysql_query($query);
$i=0;
$j=0;
while($row=mysql_fetch_array($result))
{
    foreach($row as $item){

        $excel[$j]['CaseNo']= $row['CaseNo'];
        $excel[$j]['PName']= $row['PName'];
        $excel[$j]['ObNo']= $row['ObNo'];
        $excel[$j]['SentencingDate']= $row['SentencingDate'];
        $excel[$j]['Sentence']= $row['Sentence'];

    }
    $j++;

}

$R=new PHPReport();


$R->load(array(
        'id'=>'ongoing_cases',
        'header'=>array(
            'ObNo','Accused','Offence','Police Station'
        ),

        'config'=>array(
            'header'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>80,'align'=>'right'),
                2=>array('width'=>300,'align'=>'left'),
                3=>array('width'=>300,'align'=>'left'),
                4=>array('width'=>200,'align'=>'left')

            ),
            'data'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>80,'align'=>'right'),
                2=>array('width'=>300,'align'=>'left'),
                3=>array('width'=>300,'align'=>'left'),
                4=>array('width'=>200,'align'=>'left')
            ),

        ),
        'data'=>$excel,

    )
);



echo $R->render('excel');
exit();