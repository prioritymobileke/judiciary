<?php

include '../PHPReport.php';
require("../../connect1.php");

        $query="select ob.OffenceType,ob.CourtDate,ob.EntryDate,parties.ObNo,ob.Station,GROUP_CONCAT(parties.PName SEPARATOR ' , ' ) as PName,parties.Role from parties inner join ob on ob.ObNo=parties.ObNo
                     where parties.ObNo  not in (select ObNo from cases)and parties.Role='Accused' and ob.CourtDate is not null and ob.CourtDate!='' and parties.PName !='' and ob.IncidentType <> 'Traffic' group by ObNo";
					 
        $excel=array(array());
        $result=mysql_query($query);
		$i=0;
		$j=0;
		 while($row=mysql_fetch_array($result))
         {
			 foreach($row as $item){
				 $excel[$j]['ObNo']= $row['ObNo'];
				$excel[$j]['EntryDate']= $row['EntryDate'];
				$excel[$j]['Station']= $row['Station'];
				$excel[$j]['PName']= $row['PName'];				
				$excel[$j]['OffenceType']= $row['OffenceType'];
				
			 }
			 $j++;
			
		 }
		
$R=new PHPReport();
$phpExcel = new PHPExcel();

$R->load(array(
            'id'=>'ongoing_cases',			
			'header'=>array(				
					'OBNo','OB ENTRY DATE','STATION','ACCUSED','OFFENCE'					
				),
			'config'=>array(
					0=>array('width'=>80,'align'=>'left'),
					1=>array('width'=>150,'align'=>'left'),
					2=>array('width'=>200,'align'=>'left'),
					3=>array('width'=>300,'align'=>'left'),
					4=>array('width'=>300,'align'=>'left'),
					
				),
			
             'data'=>$excel,
			
            )
        );
		
		
		
echo $R->render('excel');
exit();