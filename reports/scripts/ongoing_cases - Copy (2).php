<?php

include '../PHPReport.php';
require("../../connect1.php");

        $query="select GROUP_CONCAT(parties.PName SEPARATOR ' , ') AS PName,cases.CaseNo,cases.ObNo,cases.MagistrateName,cases.CaseStatus,ob.CourtName from cases 
	    inner join ob on ob.ObNo=cases.ObNo inner join parties on parties.ObNo=cases.ObNo 
	
	    where cases.CaseStatus!='Sentenced' and parties.Role='Accused' GROUP BY cases.CaseNo" ;
        $excel=array(array());
        $result=mysql_query($query);
		$i=0;
		$j=0;
		 while($row=mysql_fetch_array($result))
         {
			 foreach($row as $item){
				$excel[$j]['CaseNo']= $row['CaseNo'];
				$excel[$j]['ObNo']= $row['ObNo'];
				$excel[$j]['PName']= $row['PName'];
				$excel[$j]['CourtName']= $row['CourtName'];
				$excel[$j]['MagistrateName']= $row['MagistrateName'];
				$excel[$j]['CaseStatus']= $row['CaseStatus'];
			 }
			 $j++;
			
		 }
		
$R=new PHPReport();
$phpExcel = new PHPExcel();

$R->load(array(
            'id'=>'ongoing_cases',			
			'header'=>array(				
					'Case No','ObNo','Party name','Court','Magistrate','Case Status'					
				),
			'config'=>array(
					0=>array('width'=>80,'align'=>'right'),
					1=>array('width'=>80,'align'=>'right'),
					2=>array('width'=>300,'align'=>'left'),
					3=>array('width'=>200,'align'=>'left'),
					4=>array('width'=>300,'align'=>'left'),
					5=>array('width'=>200,'align'=>'left')
				),
			
             'data'=>$excel,
			
            )
        );
		
		
		
echo $R->render('excel');
exit();