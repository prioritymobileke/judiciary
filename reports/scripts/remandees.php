<?php

include '../PHPReport.php';
require("../../connect1.php");
$template='invoice.xls';
$config=array(
    'template'=>$template

);
$query="select cases.CaseNo,parties.PName,remand.Status,remand.RemandPlace from parties inner join cases on cases.ObNo=parties.ObNo
                     inner join remand on remand.PID=parties.PID where remand.Status is null and cases.CaseStatus !='Sentenced' " ;
$excel=array(array());
$result=mysql_query($query);
$i=0;
$j=0;
while($row=mysql_fetch_array($result))
{
    foreach($row as $item){

        $excel[$j]['CaseNo']= $row['CaseNo'];
        $excel[$j]['PName']= $row['PName'];
        $excel[$j]['RemandPlace']= $row['RemandPlace'];

    }
    $j++;

}

$R=new PHPReport();


$R->load(array(
        'id'=>'ongoing_cases',
        'header'=>array(
            'CaseNo','Accused','Remand Facility'
        ),

        'config'=>array(
            'header'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>300,'align'=>'right'),
                2=>array('width'=>300,'align'=>'left')

            ),
            'data'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>300,'align'=>'right'),
                2=>array('width'=>300,'align'=>'left')
            ),

        ),
        'data'=>$excel,

    )
);



echo $R->render('excel');
exit();