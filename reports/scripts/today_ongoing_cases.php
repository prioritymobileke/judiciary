<?php

include '../PHPReport.php';
require("../../connect1.php");
$template='invoice.xls';
$config=array(
    'template'=>$template

);
$query="select cases.CaseNo,cases.MagistrateName,ob.ObNo,
  cases.CaseStatus,ob.CourtName,MIN(casehistory.DateAppeared) as DateAppeared from cases
	inner join ob on ob.ObNo=cases.ObNo  left join casehistory on casehistory.CaseNo=cases.CaseNo
	where cases.CaseStatus!='Sentenced' and casehistory.NextCourtDate=curdate() GROUP BY cases.CaseNo" ;
$excel=array(array());
$result=mysql_query($query);
$i=0;
$j=0;
while($row=mysql_fetch_array($result))
{
    foreach($row as $item){
        $excel[$j]['CaseNo']= $row['CaseNo'];
        $excel[$j]['ObNo']= $row['ObNo'];

        $excel[$j]['CourtName']= $row['CourtName'];
        $excel[$j]['MagistrateName']= $row['MagistrateName'];
        $excel[$j]['CaseStatus']= $row['CaseStatus'];
        $startdate=$row['DateAppeared'];
        $start=date_create($startdate);
        $todaydate=date('Y-m-d');
        $today=date_create($todaydate);

        $caseduration = date_diff($start,$today);
        $duration=$caseduration->format("%a days");

        if($startdate==NULL)
        {
            $excel[$j]['Duration']= 'null';
        }
        else
        {

            $excel[$j]['Duration']= $duration;
        }
    }
    $j++;

}

$R=new PHPReport();


$R->load(array(
        'id'=>'ongoing_cases',
        'header'=>array(
            'Case No','ObNo','Court','Magistrate','Case Status','Case Duration'
        ),

        'config'=>array(
            'header'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>80,'align'=>'right'),
                2=>array('width'=>200,'align'=>'left'),
                3=>array('width'=>300,'align'=>'left'),
                4=>array('width'=>300,'align'=>'left'),
                5=>array('width'=>200,'align'=>'left')
            ),
            'data'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>80,'align'=>'right'),
                2=>array('width'=>200,'align'=>'left'),
                3=>array('width'=>300,'align'=>'left'),
                4=>array('width'=>300,'align'=>'left'),
                5=>array('width'=>200,'align'=>'left')
            ),

        ),
        'data'=>$excel,

    )
);



echo $R->render('excel');
exit();