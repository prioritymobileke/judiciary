<?php

include '../PHPReport.php';
require("../../connect1.php");
$template='invoice.xls';
$config=array(
    'template'=>$template

);
$query="select ob.ObNo,ob.OffenceType,ob.IncidentType,parties.ObNo,parties.PName,ob.CourtDate,ob.IncidentType,ob.Station from ob
                    inner join parties on parties.ObNo=ob.ObNo where parties.ObNo  not in (select ObNo from cases) AND ob.IncidentType='Traffic' and
                    ob.CourtDate=curdate() and parties.Role='Accused' and ob.CourtDate is not null and ob.CourtDate!='' " ;
$excel=array(array());
$result=mysql_query($query);
$i=0;
$j=0;
while($row=mysql_fetch_array($result))
{
    foreach($row as $item){

        $excel[$j]['ObNo']= $row['ObNo'];
        $excel[$j]['PName']= $row['PName'];
        $excel[$j]['OffenceType']= $row['OffenceType'];
        $excel[$j]['Station']= $row['Station'];

    }
    $j++;

}

$R=new PHPReport();


$R->load(array(
        'id'=>'ongoing_cases',
        'header'=>array(
            'ObNo','Party name','Offence','Police Station'
        ),

        'config'=>array(
            'header'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>200,'align'=>'right'),
                2=>array('width'=>300,'align'=>'left'),
                3=>array('width'=>200,'align'=>'left')

            ),
            'data'=>array(
                0=>array('width'=>80,'align'=>'right'),
                1=>array('width'=>200,'align'=>'right'),
                2=>array('width'=>300,'align'=>'left'),
                3=>array('width'=>200,'align'=>'left')
            ),

        ),
        'data'=>$excel,

    )
);



echo $R->render('excel');
exit();