<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
    header("Location:index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
   
   
    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script type="text/javascript" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>
	
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" type="text/css" />
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
				dom: 'Bfrtip',
              buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });
			
		
	
	
	for (i = new Date().getFullYear() +1; i > new Date().getFullYear() -101; i--)
{
    $('#startyear').append($('<option />').val(i).html(i));
	$('#endyear').append($('<option />').val(i).html(i));
}

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
       
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

         <?php include"left_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">

      

        <div class="maincontent">
            <div class="maincontentinner">
			<?php 
		if(isset($_REQUEST["y"])){
		 $startyear=$_REQUEST["y"];
		 $endyear=$_REQUEST["y"];
		 }else{
			 
		$startyear=$_REQUEST["startyear"];
		 $endyear=$_REQUEST["endyear"]; 
		 }
		 ?>

                <h4 class="widgettitle"><?php echo $_REQUEST['caseType'];?> Cases between Jan <?php echo $startyear;?> and Dec <?php echo $endyear;?>				
				<form id="casedateselector" method="get" action="#" style="float:right;" >
				<input type="hidden" name="caseType" value="<?php echo $_REQUEST["caseType"];?>" />
		        <p> 
				<select name="startyear" id="startyear">				
				<option value="">Start year</option>
				<?php for($y=date("Y")+1;$y>1900;$y--){
					echo '<option value="'.$y.'">'.$y.'</option>';
				}
				?>
				</select>				
				<select name="endyear" id="endyear">				
				<option value="">End year</option>
				<?php for($y=date("Y")+1;$y>1900;$y--){
					echo '<option value="'.$y.'">'.$y.'</option>';
				}
				?>
				</select>
				<input type="submit" style="margin-top:-6px;" />
				</p>
		      </form>
				</h4><br>
               
                 <table id="dyntable" class="table table-bordered responsive col-md-6" style="font-size:5px;">


                    <colgroup>
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
						<col class="con0" />                        
                        <col class="con1" />
                        <col class="con0" />
						 <col class="con1" />
                        <col class="con0" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0">S.No</th>
                        <th class="head0">CaseNo</th>
						<th class="head0">Date of<br> Plea</th>
                        <th class="head0">Parties</th>                        
                        <th class="head0">Advocate</th>
                        <th class="head0">No. of<br> witnesses<br> heard</th>
                        <th class="head0">No. of<br> witnesses <br>remaining</th>					
						<th class="head0">Days<br> Outstanding</th>
						<th class="head0">Police <br>Station</th>
						<th class="head0">Custody</th>
                        <th class="head0">Result<br>/Status</th>
                        

                    </tr>
                    </thead>
                    <tbody>
                  <?php
   
		 
                    require("connect1.php");                   
					   
				$query="select * From cases where caseType='".$_REQUEST['caseType']."' and YEAR(DateOfPlea)>='".$startyear."'  and YEAR(DateOfPlea)<='".$endyear."'";
                    $result=mysql_query($query);
					$s=1;
					while($row=mysql_fetch_array($result))
                    { 
				     ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span>
						  
						   <td>                     
                          <?php echo $s;?>
						  </td>                     
 
            <td><?php echo '<a href="assignDate.php?caseNo='.$row['CaseNo'].'">'. $row['CaseNo'].'</a>'?></td> 
            <td><?php echo date("d-M-Y",strtotime($row['DateOfPlea'])) ?></td>			
                            
							
							
								
								<td>
	<?php
	$query2="select * From parties where caseNo='".$row['CaseNo']."'";
	$result2=mysql_query($query2);
	$result3=mysql_query($query2);
	$rep=0;
while($row2=mysql_fetch_array($result2))
{
	if($row2['Role']=="Applicant"||$row2['Role']=="Petitioner"||$row2['Role']=="Complainant")
	{
	echo $row2['PName']."<br> ";
	$rep++;
	}	                                 
}
   if($rep==0) echo "REPUBLIC <br>";                   
	echo "VS <br>";	
	while($row2=mysql_fetch_array($result3))
	{
		
	    if($row2['Role']=="Defendant"||$row2['Role']=="Accused"||$row2['Role']=="Respondent")
	    {
	   echo $row2['PName']."<br>";
	    }	
	}?>
								</td>
                                <td>
	<?php
	$query2="select * From parties join lawyer on parties.LawyerId=lawyer.LawyerId where caseNo='".$row['CaseNo']."'";
	$result2=mysql_query($query2);	
   while($row2=mysql_fetch_array($result2))
   {	
	echo $row2['Name']."<br> ";
		                                 
    }
?>
								</td>	
							
					<td>
<?php
	$query2="select COUNT(*) AS heard From parties where caseNo='".$row['CaseNo']."' and Role ='Witness' and status='HEARD'";
	$result2=mysql_query($query2);	
   while($row2=mysql_fetch_array($result2))
   {

	   echo $row2['heard']."<br> ";	
	   	   
		                                 
    }
?>
</td>
                    <td>
					<?php
	$query2="select COUNT(*) AS notheard From parties where caseNo='".$row['CaseNo']."' and Role ='Witness' and status='NOT HEARD'";
	$result2=mysql_query($query2);	
   while($row2=mysql_fetch_array($result2))
   {

	   echo $row2['notheard']."<br> ";	
	   	   
		                                 
    }
?>
</td>
					
                    <td><?php echo floor((time()-strtotime($row['DateOfPlea']))/(60*60*24)) ?></td>
					<td><?php echo $row['Station'];?></td>
                    <td>	<?php
	$query2="select * From parties where caseNo='".$row['CaseNo']."'";
	$result2=mysql_query($query2);	
   while($row2=mysql_fetch_array($result2))
   {
 if($row2['Role']=="Defendant"||$row2['Role']=="Accused"||$row2['Role']=="Respondent")
	    {
	   echo $row2['IsCustody']."<br> ";	
	    }	   
		                                 
    }
?></td>
                    <td><?php echo $row['CaseState']?></td>
                           
                           
                        </tr>

                    <?php $s++;} ?>
                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
