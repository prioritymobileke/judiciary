<?php include"header.php";?>
<div class="leftpanel">

         <?php include"left_menu.php";?>

    </div><!-- leftpanel -->

    <div class="rightpanel">

      

        <div class="maincontent">
            <div class="maincontentinner">
			<?php 
		if(isset($_REQUEST["y"])){
		 $startyear=$_REQUEST["y"];
		 $endyear=$_REQUEST["y"];
		 }else{
			 
		$startyear=$_REQUEST["startyear"];
		 $endyear=$_REQUEST["endyear"]; 
		 }
		 ?>

                <h4 class="widgettitle"><?php echo $_REQUEST['caseType'];?> Cases between Jan <?php echo $startyear;?> and Dec <?php echo $endyear;?>				
				<form id="casedateselector" method="get" action="#" style="float:right;" >
				<input type="hidden" name="caseType" value="<?php echo $_REQUEST["caseType"];?>" />
		        <p> 
				<select name="startyear" id="startyear">				
				<option value="">Start year</option>
				<?php for($y=date("Y")+1;$y>1900;$y--){
					echo '<option value="'.$y.'">'.$y.'</option>';
				}
				?>
				</select>				
				<select name="endyear" id="endyear">				
				<option value="">End year</option>
				<?php for($y=date("Y")+1;$y>1900;$y--){
					echo '<option value="'.$y.'">'.$y.'</option>';
				}
				?>
				</select>
				<input type="submit" style="margin-top:-6px;" />
				</p>
		      </form>
				</h4><br>
				
               <?php if($_REQUEST['caseType']=="Murder" ):?>
                 <table id="dyntable" class="table table-bordered responsive col-md-6" style="font-size:5px;">


                    <colgroup>
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
						<col class="con0" />                        
                        <col class="con1" />
                        <col class="con0" />
						 <col class="con1" />
                        <col class="con0" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0">S.No</th>
                        <th class="head0">CaseNo</th>
						<th class="head0">Date of<br> Plea</th>
                        <th class="head0">Parties</th>                        
                        <th class="head0">Advocate</th>
                        <th class="head0">No. of<br> witnesses<br> heard</th>
                        <th class="head0">No. of<br> witnesses <br>remaining</th>					
						<th class="head0">Days<br> Outstanding</th>
						<th class="head0">Police <br>Station</th>
						<th class="head0">Custody</th>
                        <th class="head0">Result<br>/Status</th>
                        

                    </tr>
                    </thead>
                    <tbody>
                  <?php
   
		 
                    require("connect1.php");                   
					   
				$query="select * From criminal_murder INNER JOIN parties
          ON criminal_murder.CaseNo=parties.caseNo INNER JOIN lawyer
          ON parties.LawyerId=lawyer.LawyerId where  YEAR(criminal_murder.DateOfPlea)>='".$startyear."'  and YEAR(criminal_murder.DateOfPlea)<='".$endyear."'";
                    $result=mysql_query($query);
					$s=1;
					while($row=mysql_fetch_array($result))
                    { 
				     ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span>
						  
						   <td>                     
                          <?php echo $s;?>
						  </td>                     
 
         
            <td><?php echo '<a href="summaryMurder.php?caseNo='.$row['CaseNo'].'">'. $row['CaseNo'].'</a>'?></td>
            <td><?php echo $row['DateOfPlea'] ?></td>
            <td><?php echo $row['PName'] ?></td>
            <td><?php echo $row['Name'] ?></td>
            <td><?php echo $row['witnessHeard'] ?></td>
            <td><?php echo $row['witnessRemaining'] ?></td>
            
            <td><?php
            

$now = time(); // or your date as well
$your_date = strtotime($row['DateOfPlea']);
$datediff = $now - $your_date;

echo floor($datediff / (60 * 60 * 24));

          ?></td>
          <td><?php echo $row['Station'] ?></td>
            <td><?php echo $row['RemandFacility'] ?></td>
            <td><?php echo $row['CaseState'] ?></td></tr>

                           
                           
                        </tr>

                    <?php $s++;} ?>
                    </tbody>
                </table>
                
              <?php else: ?>
                 <table id="dyntable" class="table table-bordered responsive col-md-6" style="font-size:5px;">


                    <colgroup>
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
						<col class="con0" />                        
                        <col class="con1" />
                        <col class="con0" />
						 <col class="con1" />
                        <col class="con0" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0">S.No</th>
                        <th class="head0">CaseNo</th>
						<th class="head0">Date of<br> Plea</th>
                        <th class="head0">Parties</th>                        
                        <th class="head0">Advocate</th>
                        <th class="head0">No. of<br> witnesses<br> heard</th>
                        <th class="head0">No. of<br> witnesses <br>remaining</th>					
						<th class="head0">Days<br> Outstanding</th>
						<th class="head0">Police <br>Station</th>
						<th class="head0">Custody</th>
                        <th class="head0">Result<br>/Status</th>
                        

                    </tr>
                    </thead>
                    <tbody>
                  <?php
   
		 
                    require("connect1.php");                   
					   
				$query="select * From criminal_ord_appeal INNER JOIN parties
          ON criminal_murder.CaseNo=parties.caseNo INNER JOIN lawyer
          ON parties.LawyerId=lawyer.LawyerId where  YEAR(criminal_murder.DateOfPlea)>='".$startyear."'  and YEAR(criminal_murder.DateOfPlea)<='".$endyear."'";
                    $result=mysql_query($query);
					$s=1;
					while($row=mysql_fetch_array($result))
                    { 
				     ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span>
						  
						   <td>                     
                          <?php echo $s;?>
						  </td>                     
 
         
            <td><?php echo '<a href="summaryMurder.php?caseNo='.$row['CaseNo'].'">'. $row['CaseNo'].'</a>'?></td>
            <td><?php echo $row['DateOfPlea'] ?></td>
            <td><?php echo $row['PName'] ?></td>
            <td><?php echo $row['Name'] ?></td>
            <td><?php echo $row['witnessHeard'] ?></td>
            <td><?php echo $row['witnessRemaining'] ?></td>
            
            <td><?php
            

$now = time(); // or your date as well
$your_date = strtotime($row['DateOfPlea']);
$datediff = $now - $your_date;

echo floor($datediff / (60 * 60 * 24));

          ?></td>
          <td><?php echo $row['Station'] ?></td>
            <td><?php echo $row['RemandFacility'] ?></td>
            <td><?php echo $row['CaseState'] ?></td></tr>

                           
                           
                        </tr>

                    <?php $s++;} ?>
                    </tbody>
                </table>
                <?php endif;?>
<?php include"footer.php";?>