<?php
session_start();
require("connect1.php");
if(!isset($_SESSION['MagistrateId']) || trim ($_SESSION['MagistrateId']==''))
{
    header("Location:index.php");
}
?>

<!DOCTYPE html>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Usalama Dashboard</title>
    <link rel="stylesheet" href="css/style.default.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive-tables.css">

    <link rel="stylesheet" href="css/forms.css" type="text/css">

    <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.1.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>
    <script type="text/javascript" src="js/modernizr.min.js"></script>
    <script type="text/javascript" src="js/responsive-tables.js"></script>
    <script type="text/javascript" src="js/custom.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            // dynamic table
            jQuery('#dyntable').dataTable({
                "sPaginationType": "full_numbers",
                "aaSortingFixed": [[0,'asc']],
                "fnDrawCallback": function(oSettings) {
                    jQuery.uniform.update();
                }
            });

            jQuery('#dyntable2').dataTable( {
                "bScrollInfinite": true,
                "bScrollCollapse": true,
                "sScrollY": "300px"
            });

        });
    </script>
</head>

<body>

<div class="mainwrapper">

    <div class="header">
        <div class="logo">
            <a href="dashboard.php"><img src="images/logo1.png" alt="" /></a>
        </div>
        <div class="headerinner">
            <ul class="headmenu">


                <li class="right">
                    <div class="userloggedinfo">
                        <img src="<?php echo $_SESSION['myphoto'] ?>" alt="" />
                        <div class="userinfo">
                            <h5><?php echo $_SESSION['names'] ?> </h5>
                            <ul>

                                <li><a href="logout.php">Sign Out</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul><!--headmenu-->
        </div>
    </div>

    <div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>

                <li class=""><a href="dashboard.php"><i class="iconfa-home"></i></span> HOME</a></li>
                <li class="dropdown active"><a href=""><span class="iconfa-pencil"></span> Today's Cases</a>
                	<ul style="display: block">
                    	<li class="active"><a href="todayNewCases.php"><span class="iconfa-book"></span> Today's New Cases </a></li>
                    	<li class=""><a href="todayTrafficCases.php"><span class="iconfa-book"></span> Today's Traffic Cases </a></li>
			<li class=""><a href="todayOngoingCases.php"><span class="iconfa-book"></span> Today's Ongoing Cases </a></li>
			
                    </ul>
                </li>
                <li class=""><a href="newCasesTable.php"><span class="iconfa-laptop"></span> New cases </a></li>
                <li class=""><a href="newCasesTrafficTable.php"><span class="iconfa-briefcase"></span> New Traffic cases </a></li>

                <li class=""><a href="ongoingCasesTable.php"><span class="iconfa-envelope"></span> Ongoing cases </a></li>
                <li class=""><a href="closedCasesTable.php"><span class="iconfa-signal"></span> Terminated cases </a></li>
                <li class=""><a href="chargedSexOffenders.php"><span class="iconfa-briefcase"></span> Charged sex offenders</a></li>


            </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>All Cases</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">

                <h4 class="widgettitle">Case Details Table|<a href="reports/scripts/today_new_cases.php" style="color:#fff">  Export <img src="images/images/excel.png" align="center" width="20"></a></h4>
                <div style="background:#fff;padding:10px; width:300px; margin:auto;"><img src="images/images/emblem.png" align="center"></div>
                <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0"></th>
                        <th class="head0">CaseNo</th>
                        <th class="head0">ObNo</th>
                        <!--<th class="head0">Ob entry Date</th>-->
                        <th class="head0">Station</th>
                        <th class="head0">Parties</th>                        
                        <th class="head0">Offence</th>




                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");


                    $query="select cases.CaseNo,ob.OffenceType,ob.CourtDate,parties.ObNo,ob.Station,GROUP_CONCAT(parties.PName SEPARATOR ' , ' ) as PName,cases.CaseNo from ob inner join parties on ob.ObNo=parties.ObNo inner join cases on cases.ObNo=ob.ObNo  
                     where cases.MagistrateName='".$_SESSION['names']."'   and ob.CourtDate=curdate() and ob.CourtDate is not null and ob.CourtDate!='' and parties.PName !='' and ob.IncidentType <> 'Traffic' group by CaseNo";

                    $result=mysql_query($query);


                    while($row=mysql_fetch_array($result))
                    {
                        $caseno=$row['CaseNo'];
                        $obno=$row['ObNo'];
                        //$entrydate=$row['EntryDate'];
                        $station=$row['Station'];
                        
                       $my_accused=mysql_query("select PName from parties where Role='accused' and ObNo='$obno'");
                       
                       while($myrow=mysql_fetch_array($my_accused))
                       {
                       $accused=$myrow['PName'];
                       }
                        
                        $my_complainant=mysql_query("select PName from parties where Role='complainant' and ObNo='$obno'");
                        
                         if(mysql_num_rows($my_complainant)==0)
                       {
                       $complainant='unknown';
                       }
                       
                       while($myrow1=mysql_fetch_array($my_complainant))
                       {
                       $complainant=$myrow1['PName'];                       
                       }
                       
                       $parties=$complainant ." ". "vs"." " .$accused;
                        
                        $offence=$row['OffenceType'];


                        ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            <td><?php
                                echo '<a href="newCases.php?obno='.$obno.'">Proceed with case</a>';

                                ?></td>

                            <td><?php echo $caseno ?></td>
                            <td><?php echo $obno ?></td>
                            <!--<td><?php echo $entrydate?></td>-->
                            <td><?php echo $station?></td>
                            <td><?php echo $parties?></td>
                            
                            <td><?php echo $offence?></td>



                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
