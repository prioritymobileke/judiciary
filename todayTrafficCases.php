<?php include "header.php";?>
    <div class="leftpanel">

        <div class="leftmenu">
            <ul class="nav nav-tabs nav-stacked">
                <li class="nav-header">Navigation</li>


                <li class=""><a href="dashboard.php"><i class="iconfa-home"></i></span> HOME</a></li>
                <li class="dropdown active"><a href=""><span class="iconfa-pencil"></span> Today's Cases</a>
                	<ul style="display: block">
                    	<li class=""><a href="todayNewCases.php"><span class="iconfa-book"></span> Today's New Cases </a></li>
                    	<li class="active"><a href="todayTrafficCases.php"><span class="iconfa-book"></span> Today's Traffic Cases </a></li>
			<li class=""><a href="todayOngoingCases.php"><span class="iconfa-book"></span> Today's Ongoing Cases </a></li>
			
                    </ul>
                </li>
                <li class=""><a href="newCasesTable.php"><span class="iconfa-laptop"></span> New cases </a></li>
                <li class=""><a href="newCasesTrafficTable.php"><span class="iconfa-briefcase"></span> New Traffic cases </a></li>

                <li class=""><a href="ongoingCasesTable.php"><span class="iconfa-envelope"></span> Ongoing cases </a></li>
                <li class=""><a href="closedCasesTable.php"><span class="iconfa-signal"></span> Terminated cases </a></li>
                <li class=""><a href="chargedSexOffenders.php"><span class="iconfa-briefcase"></span> Charged sex offenders</a></li>


            </ul>
        </div><!--leftmenu-->

    </div><!-- leftpanel -->

    <div class="rightpanel">



        <div class="pageheader">

            <div class="pageicon"><span class="iconfa-table"></span></div>
            <div class="pagetitle">
                <h1>All Cases</h1>
            </div>
        </div><!--pageheader-->

        <div class="maincontent">
            <div class="maincontentinner">

                <h4 class="widgettitle">Case Details Table|<a href="reports/scripts/today_traffic_cases.php" style="color:#fff">  Export <img src="images/images/excel.png" align="center" width="20"></a></h4>
                <div style="background:#fff;padding:10px; width:300px; margin:auto;"><img src="images/images/emblem.png" align="center"></div>
                <table id="dyntable" class="table table-bordered responsive">


                    <colgroup>
                        <col class="con0" style="align: center; width: 4%" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                        <col class="con0" />
                        <col class="con1" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th class="head0 nosort"><input type="checkbox" class="checkall" /></th>
                        <th class="head0"></th>
                          <th class="head0">CaseNo</th>
                        <th class="head0">ObNo</th>
                        <th class="head0">Accused</th>
                        <th class="head0">Offence </th>
                        <th class="head0">Station</th>




                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    require("connect1.php");


                    $query="select cases.CaseNo,ob.ObNo,ob.OffenceType,ob.IncidentType,parties.ObNo,parties.PName,ob.CourtDate,ob.IncidentType,ob.Station from ob
                    inner join parties on parties.ObNo=ob.ObNo inner join cases on cases.ObNo=ob.ObNo where parties.ObNo  not in (select ObNo from cases) AND ob.IncidentType='Traffic' and
                    ob.CourtDate=curdate()and cases.MagistrateName='".$_SESSION['names']."' and parties.Role='Accused' and ob.CourtDate is not null and ob.CourtDate!=''   ";

                    $result=mysql_query($query);


                    while($row=mysql_fetch_array($result))
                    {
                        $caseno=$row['CaseNo'];
                        $obno=$row['ObNo'];
                        $name=$row['PName'];
                        $station=$row['Station'];
                        $offence=$row['OffenceType'];


                        ?>

                        <tr class="gradeX">
                            <td class="aligncenter"><span class="center">
                            <input type="checkbox" />

                          </span></td>
                            <td><?php
                                echo '<a href="newCasesTraffic.php?obno='.$obno.'">Proceed with case</a>';

                                ?></td>

                            <td><?php echo $caseno?></td>
                            <td><?php echo $obno ?></td>
                            <td><?php echo $name ?></td>
                            <td><?php echo $offence ?></td>
                            <td><?php echo $station ?></td>



                        </tr>

                    <?php } ?>

                    </tbody>
                </table>

                <div class="footer">
                    <div class="footer-left">
                        <span>&copy; 2013. Priority Mobile Dashboard. All Rights Reserved.</span>
                    </div>
                    <div class="footer-right">
                        <span>Designed by: <a href="http://prioritymobile.co.ke/">Priority Mobile</a></span>
                    </div>
                </div><!--footer-->

            </div><!--maincontentinner-->
        </div><!--maincontent-->

    </div><!--rightpanel-->

</div><!--mainwrapper-->
</body>
</html>
